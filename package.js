Package.describe({
  name: 'znewsham:diagramo-editor',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.1');
    api.use('ecmascript');
    api.use("underscore");
    api.use(['templating'], 'client');

    api.use("znewsham:diagramo-core");
    api.addFiles([
      'client/lib/colorPicker_new.css',
      'client/lib/colorPicker_new.js',
      'client/lib/diagramo-editor.js',
      'client/lib/localProperties.js',
      'client/lib/draggableImage.js',
      'client/lib/key.js',
      'client/lib/handleManager.js',
      'client/lib/handle.js',
      'client/lib/handles/hasHandles.js',
      'client/lib/connectorHandle.js',
      'client/lib/figure.js',
      'client/lib/group.js',
      'client/lib/connectors/defaultOnConnectDelegate.js',
      'client/lib/connectors/connector.js',
      'client/lib/connectors/straightConnector.js',
      'client/lib/connectors/multiPointConnector.js',
      'client/lib/connectors/rightAngleConnector.js',
      'client/lib/connectors/organicConnector.js',
      'client/lib/connectionPoint.js',
      'client/lib/figureConnectionPoint.js',
      'client/lib/connectorConnectionPoint.js',
      'client/lib/glue.js',
      'client/lib/history.js',
      'client/lib/commands/figureMatrixCommand.js',
      'client/lib/commands/figureRotateCommand.js',
      'client/lib/commands/figureFlipCommand.js',
      'client/lib/commands/figureScaleCommand.js',
      'client/lib/commands/figureTranslateCommand.js',
      'client/lib/commands/figureDeleteCommand.js',
      'client/lib/commands/bringToFrontCommand.js',
      'client/lib/commands/sendToBackCommand.js',
      'client/lib/commands/connectorCreateCommand.js',
      'client/lib/commands/connectorDeleteCommand.js',
      'client/lib/commands/propertyChangeCommand.js',
      'client/lib/commands/alignHCommand.js',
      'client/lib/commands/alignVCommand.js',

      'client/lib/propertyEditor.js',
      'client/lib/propertyEditor.css',
      'client/lib/properties/property.js',
      'client/lib/properties/colorProperty.js',
      'client/lib/properties/selectProperty.js',
      'client/lib/properties/checkBoxProperty.js',
      'client/lib/properties/imageSelectProperty.js',
      'client/lib/properties/textProperty.js',
      'client/lib/properties/commandProperty.js',
      'client/lib/properties/actionProperty.js',
      'client/lib/properties/multiProperty.js',

      "client/lib/sets/basic/basic.js",
      "client/lib/sets/basic/square.js",
      "client/lib/sets/basic/rectangle.js",
      "client/lib/sets/basic/circle.js",
      "client/lib/sets/basic/diamond.js",
      "client/lib/sets/basic/ellipse.js",
      "client/lib/sets/basic/hexagon.js",
      "client/lib/sets/basic/octogon.js",
      "client/lib/sets/basic/pentagon.js",
      "client/lib/sets/basic/parallelogram.js",
      "client/lib/sets/basic/rightTriangle.js",
      "client/lib/sets/basic/roundedRectangle.js",
      "client/lib/sets/basic/triangle.js",
      "client/lib/sets/basic/text.js",

      "client/lib/handles/translateHandle.js",
      "client/lib/handles/rotateHandle.js",
      "client/lib/handles/northHandle.js",
      "client/lib/handles/southHandle.js",
      "client/lib/handles/eastHandle.js",
      "client/lib/handles/westHandle.js",
      "client/lib/handles/northEastHandle.js",
      "client/lib/handles/northWestHandle.js",
      "client/lib/handles/southEastHandle.js",
      "client/lib/handles/southWestHandle.js",
      "client/lib/handles/extendSouthHandle.js",
      'client/lib/figureHandle.js',

      "client/lib/sets/connectors/connectors.js",
      "client/lib/sets/connectors/arrow.js",
      "client/lib/sets/connectors/triangle.js",
      /*"client/lib/sets/connectors/blocker.js",
      "client/lib/sets/connectors/gapFill.js",
      "client/lib/sets/connectors/hookLeft.js",
      "client/lib/sets/connectors/hookRight.js", */

      "client/lib/customFigureViewer.js",
      "client/lib/urlCustomFigureProvider.js",
      "client/lib/customFigure.js",
      "client/lib/squigglyLineDelegate.js",
      "client/lib/grid.js"
    ], ["client"])

});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:diagramo-editor');
});
