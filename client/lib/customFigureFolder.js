DIAGRAMO.CustomFigureFolder = function(id, imageSrc, name){
    this.id = id;
    this.name = name;
    this.imageSrc = imageSrc;
    this.figures = [];
    this.is_folder = true;
}
DIAGRAMO.CustomFigureFolder.prototype = {

    getElement: function(diagramo, customFigureProvider){
        var self = this;
        var div = $("<div>");
        div.addClass("custom-figure-folder");
        div.addClass('custom-figure');
        var buttons = $("<div>");
        var plusmin = $("<i>");
        buttons.addClass("buttons");
        plusmin.addClass("fa fa-plus");
        buttons.append(plusmin);

        div.append(this.getImageElement(editor));
        div.append(buttons);

        var label = $("<div>");
        label.addClass("label");
        label.text(self.name + " (" + this.figures.length +")");
        div.append(label);

        var figuresElement = $("<div>");
        figuresElement.addClass("figures");
        figuresElement.css("display","none");
        var self = this;
        this.figures.forEach(function(figure){
            var div = figure.getElement(diagramo, customFigureProvider);
            figuresElement.append(div);
        });
        div.append(figuresElement);
        plusmin.click(function(){
            if(plusmin.hasClass("fa-plus")){
                figuresElement.css("display","block");
                plusmin.removeClass("fa-plus");
                plusmin.addClass("fa-minus");
            }
            else{
                figuresElement.css("display","none");
                plusmin.addClass("fa-plus");
                plusmin.removeClass("fa-minus");
            }
        });

        var editDeleteDiv = $("<div>");
        editDeleteDiv.addClass("edit-delete");
        var edit = $("<span>");

        edit.html("Edit Folder");
        edit.addClass("edit");
        edit[0].onclick = function(evt){
            customFigureProvider.editFigure(self);
        }

        var del = $("<span>");
        del.html("Delete Folder");
        del.addClass("delete");
        del.click(function(evt){
            customFigureProvider.deleteFigure(self);
        });

        var sep = $("<span>");
        sep.html("|");
        editDeleteDiv.append(del);
		editDeleteDiv.append(edit);
        /*editDeleteDiv.append(sep);*/
        figuresElement.append(editDeleteDiv);
        return div;
    },
    getImageElement: function(diagramo){
        var self = this;
        var img = document.createElement("img");
        img.src = this.imageSrc;
        img.width = "32";
        return img;
    }
}
