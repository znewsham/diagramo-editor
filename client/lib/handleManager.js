
/**HandleManager will act like a Singleton (even not defined as one)
 * You will attach a Figure to it and he will be in charge with the figure manipulation
 * @constructor
 * @this {HandleManager}
 **/
function HandleManager(editor){
    /**The shape (figure or connector) that the HandleManager will manage*/
    this.shape = null;
    this.dragging = null;
    /**An {Array} with current handles*/
    this.handles = {
        nw: DIAGRAMO.FigureHandle.new("nw", null),
        n: DIAGRAMO.FigureHandle.new("n", null),
        ne: DIAGRAMO.FigureHandle.new("ne", null),
        sw: DIAGRAMO.FigureHandle.new("sw", null),
        s: DIAGRAMO.FigureHandle.new("s", null),
        se: DIAGRAMO.FigureHandle.new("se", null),
        e: DIAGRAMO.FigureHandle.new("e", null),
        w: DIAGRAMO.FigureHandle.new("w", null),
        r: DIAGRAMO.FigureHandle.new("r", null),
    };
    var self = this;
    this.editor = editor;
    _.keys(this.handles).forEach(function(key){
        self.handles[key].zIndex = 0;
        self.editor.addPaintable(self.handles[key]);
    });
    /**An {Array} with connector handles*/
    this.connectorHandles = [];
    /**Selection rectangle (the rectangle upon the Handles will stay in case of a Figure/Group)*/
    this.selectRect = new DIAGRAMO.Polygon();
    this.selectRect.isHidden = true;
    this.selectRect.visible = false;
    this.selectRect.style.strokeStyle = "#cccccc";
    this.selectRect.style.lineWidth = "15px";
    this.selectRect.id = DIAGRAMO.generateId();
    this.editor.addPaintable(this.selectRect);
    /**Currently selected handle*/
    this.handleSelectedIndex = -1;
    /**Distance from shape where to draw the handles*/
    this.handleOffset = 0;

    HandleManager.afterTransformShape = function(matrix, transformConnector){
        //this is not HandleManager, this is the figure being transformed.
        self.setShape(this);
    };
}
DIAGRAMO.HandleManager = HandleManager;

HandleManager.prototype = {
    toJSONObject: function(){
        var ret = {
            selectRectId: this.selectRect.id
        };

        return ret;
    },
    mouseUpHandler: function(event){
        this.editor.redraw = this.dragging != null;
        this.dragging = null;
    },
    mouseMoveHandler: function(event){
        if(this.dragging != null){
            //this.editor.canvas.style.cursor = this.dragging.getCursor();
            if(event.lastMove != null){
                this.dragging.action(event.lastMove,event.x,event.y);
                this.setShape(this.shape);
            }
        }
    },
    mouseDownHandler: function(event){

    },
    /**Get selected handle or null if no handler selected*/
    getSelectedHandle: function(){
        if(this.dragging != null){
            return this.dragging;
        }
        return null;
    },

    /**Use this method to set a new shape (Figure or Connetor)  to this manager.
     * Every time a new figure is set, old handles will dissapear (got erased by new figure's handles)
     **/
    setShape: function(shape){
        if(this.shape != shape){
            this.editor.redraw = true;
        }
        if(this.shape != null){
            this.shape.afterTransformHandlers = _.without(this.shape.afterTransformHandlers, HandleManager.afterTransformShape);
        }
        if(shape != null){
            shape.activeHandle = null;
            shape.afterTransform(HandleManager.afterTransformShape);
        }

        var self = this;

        if(this.shape){
            this.shape.selected = false;
            this.shape.handleManager = null;
        }
        this.selectRect.visible = false;
        _.keys(this.handles).forEach(function(key){
           self.handles[key].visible = false;
        });
        this.shape = shape;
        if(shape == null){

        }
        else if(shape.isConnector || shape.getHandles){
          shape.handleManager = this;
            this.selectRect.visible = false;
            var handles = shape.getHandles();
            this.shape.selected = true;
            _.keys(this.handles).forEach(function(key){
               self.handles[key].visible = false;
            });
        }
        else if(shape.isFigure){
            shape.handleManager = this;
            this.selectRect.visible = true;
            if(this.shape.isText){
                this.handles.r.visible = true;
                this.handles.r.parent = shape;
            }
            else{
                _.keys(this.handles).forEach(function(key){
                    self.handles[key].visible = true;
                    self.handles[key].parent = shape;
                });
            }
            //find Figure's angle
            var angle = DIAGRAMO.Util.getAngle(this.shape.rotationCoords[0], this.shape.rotationCoords[1]);
            //rotate it back to "normal" space (from current space)
            this.shape.disable("afterTransform");
            var center = this.shape.rotationCoords[0].clone();
            this.shape.transform(DIAGRAMO.Matrix.mergeTransformations(
                DIAGRAMO.Matrix.translationMatrix(-center.x, -center.y),
                DIAGRAMO.Matrix.rotationMatrix(-angle),
                DIAGRAMO.Matrix.translationMatrix(center.x, center.y)
            ), false);


            //construct bounds of the Figure in "normal" space
            var bounds = this.shape.getBounds();
            this.selectRect.points = [];
            this.selectRect.addPoint(new DIAGRAMO.Point(bounds[0] - this.handleOffset, bounds[1] - this.handleOffset)); //top left
            this.selectRect.addPoint(new DIAGRAMO.Point(bounds[2] + this.handleOffset, bounds[1] - this.handleOffset)); //top right
            this.selectRect.addPoint(new DIAGRAMO.Point(bounds[2] + this.handleOffset, bounds[3] + this.handleOffset)); //bottom right
            this.selectRect.addPoint(new DIAGRAMO.Point(bounds[0] - this.handleOffset, bounds[3] + this.handleOffset)); //bottom left

            bounds = this.selectRect.getBounds();

            //update current handles
            var handle = this.handles["nw"];
            handle.point.x = bounds[0];
            handle.point.y = bounds[1];

            handle = this.handles["n"];
            handle.point.x = bounds[0]+(bounds[2]-bounds[0])/2;
            handle.point.y = bounds[1];

            handle = this.handles["ne"];
            handle.point.x = bounds[2];
            handle.point.y = bounds[1];

            handle = this.handles["e"];
            handle.point.x = bounds[2];
            handle.point.y = bounds[1]+(bounds[3]-bounds[1])/2;

            handle = this.handles["se"];
            handle.point.x = bounds[2];
            handle.point.y = bounds[3];

            handle = this.handles["s"];
            handle.point.x = bounds[0]+(bounds[2]-bounds[0])/2;
            handle.point.y = bounds[3];

            handle = this.handles["sw"];
            handle.point.x = bounds[0];
            handle.point.y = bounds[3];

            handle = this.handles["w"];
            handle.point.x = bounds[0];
            handle.point.y = bounds[1]+(bounds[3]-bounds[1])/2;


            handle = this.handles["r"];
            handle.point.x = bounds[0]+(bounds[2]-bounds[0])/2;
            //JS: because handleOffset is 0, we still need to see rotation handle
            if (this.handleOffset!=0){
                handle.point.y = bounds[1] - this.handleOffset * 1.5;
            }
            else{
                handle.point.y = bounds[1] - 15;
            }
            handle.visible = true;


            //rotate figure from "normal" space to current space
            this.shape.transform(DIAGRAMO.Matrix.mergeTransformations(
                DIAGRAMO.Matrix.translationMatrix(-center.x, -center.y),
                DIAGRAMO.Matrix.rotationMatrix(angle),
                DIAGRAMO.Matrix.translationMatrix(center.x, center.y)
            ), false);

            this.shape.enable("afterTransform");

            this.selectRect.transform(DIAGRAMO.Matrix.mergeTransformations(
                    DIAGRAMO.Matrix.translationMatrix(-center.x, -center.y),
                    DIAGRAMO.Matrix.rotationMatrix(angle),
                    DIAGRAMO.Matrix.translationMatrix(center.x, center.y)
                ), false);
            //now transform the handles from "normal" space too
            _.keys(this.handles).forEach(function(key){
                self.handles[key].transform(DIAGRAMO.Matrix.mergeTransformations(
                    DIAGRAMO.Matrix.translationMatrix(-center.x, -center.y),
                    DIAGRAMO.Matrix.rotationMatrix(angle),
                    DIAGRAMO.Matrix.translationMatrix(center.x, center.y)
                ), false);
            });
        }
    },

    /**Returns all handles for a shape (figure or connector).
     *It does not mean that the HandleManager keeps records of all Handles for a
     *Figure but more likely they are computed on-the-fly
     *@return an {Array} of {Handle} that you can further use to manage the figure
     **/
    handleGetAll: function(){
        return this.handles;
    },

    /**Returns the handle from a certain coordinates
     *@param {Number} x - the value on Ox
     *@param {Number} y - the value on Oy
     ***/
    handleGet: function(x,y){
        for(var i=0; i<this.handles.length; i++){
            if(this.handles[i].contains(x,y)){
                return this.handles[i];
            }
        }
        return null;
    },

    /**
     *Select the handle from a certain coordinates
     *@param {Number} x - the value on Ox
     *@param {Number} y - the value on Oy
     **/
    handleSelectXY: function(x,y){
        this.handleSelectedIndex=-1;
        for (var i=0; i<this.handles.length; i++){
            if(this.handles[i].contains(x,y)){
                this.handleSelectedIndex = i;
            }
        }
    },

    /**
     *Clear this.
     **/
    clear: function(){
        this.handleSelectedIndex = -1;
        this.shape = null;
        this.handles = [];
    },
};
