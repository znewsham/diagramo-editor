function NorthWestTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "nw", handleManager);
}

DIAGRAMO.NorthWestTranslateHandle = NorthWestTranslateHandle;

DIAGRAMO.NorthWestTranslateHandle.init = function(){
  DIAGRAMO.NorthWestTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {
    getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      /*You can think as a combined North and West action*/
      transX = figBounds[2];
      transY = figBounds[3];
      if(newX<figBounds[2]-5 && newY<figBounds[3]-5){
          scaleY = (figBounds[3]-newY) /(figBounds[3]-handlerPoint.y);
          scaleX = (figBounds[2]-newX) / (figBounds[2]-handlerPoint.x);
      }
      return [transX, transY, scaleX, scaleY]
    }
  });
}
