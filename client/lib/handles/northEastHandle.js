function NorthEastTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "ne", handleManager);
}

DIAGRAMO.NorthEastTranslateHandle = NorthEastTranslateHandle;

DIAGRAMO.NorthEastTranslateHandle.init = function(){
  DIAGRAMO.NorthEastTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {
    getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      transX = figBounds[0]
      transY = figBounds[3];
      if(newX>figBounds[0]+5 && newY<figBounds[3]-5){
          scaleX = (newX-figBounds[0])/(handlerPoint.x-figBounds[0]);
          scaleY = (figBounds[3]-newY)/(figBounds[3]-handlerPoint.y);
      }
      return [transX, transY, scaleX, scaleY];
    }
  });
}
