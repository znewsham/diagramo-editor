function SouthTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "s", handleManager);
}

DIAGRAMO.SouthTranslateHandle = SouthTranslateHandle;

DIAGRAMO.SouthTranslateHandle.init = function(){
  DIAGRAMO.SouthTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {
    getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      /*move the xOy coodinates at the top of the figure (superfluous as we are there already) and then scale*/
      transY = figBounds[1];
      if(newY > figBounds[1]+5){ //South must not get too close to North
          scaleY = (newY-figBounds[1])/(handlerPoint.y-figBounds[1]);
      }
      return [0, transY, 1, scaleY]
    }
  });
}
