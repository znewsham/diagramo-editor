function RotateHandle(handleManager){
    DIAGRAMO.FigureHandle.call(this, "r", handleManager);
    var self = this;
    this.mouseDown(function(event){
        self.parent.handleManager.dragging = self;
        self.parent.activeHandle = self;
        return false;//we want this to bubble
    });
}

DIAGRAMO.RotateHandle = RotateHandle;

DIAGRAMO.RotateHandle.init = function(){
  DIAGRAMO.RotateHandle.prototype = _.extend({}, DIAGRAMO.FigureHandle.prototype, {
    action: function(lastMove, newX, newY, transX, transY, scaleX, scaleY){
      //rotationCoords[0] is always the center of the shape, we clone it as when we do -rotationCoords[0].x, it is set to 0.
      var center = this.parent.rotationCoords[0].clone();
      var endAngle = DIAGRAMO.Util.getAngle(this.parent.rotationCoords[0],new DIAGRAMO.Point(newX,newY));
      var startAngle = DIAGRAMO.Util.getAngle(this.parent.rotationCoords[0],this.parent.rotationCoords[1]);//new DIAGRAMO.Point(lastMove[0],lastMove[1])

      if(DIAGRAMO.isShiftPressed()){
        endAngle = endAngle * 180 /  Math.PI;
        endAngle = Math.round(endAngle / 5) * 5;
        endAngle = endAngle * Math.PI / 180;
      }

      var rotAngle = endAngle - startAngle;

      var equivTransfMatrix = DIAGRAMO.Matrix.mergeTransformations(
          DIAGRAMO.Matrix.translationMatrix(-center.x, -center.y),
          DIAGRAMO.Matrix.rotationMatrix(rotAngle),
          DIAGRAMO.Matrix.translationMatrix(center.x,center.y)
      );
      //TODO: make somehow to compute the inverse of it.
      //@see http://en.wikipedia.org/wiki/Transformation_matrix#Rotation to find inverses

      var inverseTransfMatrix = DIAGRAMO.Matrix.mergeTransformations(
          DIAGRAMO.Matrix.translationMatrix(-center.x, -center.y),
          DIAGRAMO.Matrix.rotationMatrix(-rotAngle),
          DIAGRAMO.Matrix.translationMatrix(center.x,center.y)
      );


      var cmdRotate = new DIAGRAMO.COMMANDS.FigureRotateCommand(this.parent, equivTransfMatrix, inverseTransfMatrix);
      cmdRotate.execute();
    }
  });
}
