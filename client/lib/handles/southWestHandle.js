function SouthWestTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "sw", handleManager);
}

DIAGRAMO.SouthWestTranslateHandle = SouthWestTranslateHandle;

DIAGRAMO.SouthWestTranslateHandle.init = function(){
  DIAGRAMO.SouthWestTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {
    getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      transX = figBounds[2]
      transY = figBounds[1];
      if(newX<figBounds[2]-5 && newY>figBounds[1]+5){
          scaleX = (figBounds[2]-newX)/((figBounds[2]-handlerPoint.x));
          scaleY = (newY-figBounds[1])/(handlerPoint.y-figBounds[1]);
      }
      return [transX, transY, scaleX, scaleY]
    }
  });
}
