function TranslateHandle(handleType, handleManager){
    DIAGRAMO.FigureHandle.call(this, handleType, handleManager);
    var self = this;
    this.mouseDown(function(event){
        self.parent.handleManager.dragging = self;
        self.parent.activeHandle = self;
        return false;//we want this to bubble
    });
}

DIAGRAMO.TranslateHandle = TranslateHandle;

DIAGRAMO.TranslateHandle.init = function(){
  DIAGRAMO.TranslateHandle.prototype = _.extend({}, DIAGRAMO.FigureHandle.prototype, {
    action: function(lastMove, newX, newY){

      //find the angle by which the figure was rotated (for any figure this is initally 0 so if != than 0 we have a rotation)
      var angle = DIAGRAMO.Util.getAngle(this.parent.rotationCoords[0], this.parent.rotationCoords[1]);

      //save initial figure's center
      var oldCenter = this.parent.rotationCoords[0].clone();
      //move the new [x,y] to the "un-rotated" and "un-translated" space
      var p = new DIAGRAMO.Point(newX,newY);
      p.transform(DIAGRAMO.Matrix.translationMatrix(-oldCenter.x,-oldCenter.y));
      p.transform(DIAGRAMO.Matrix.rotationMatrix(-angle));
      p.transform(DIAGRAMO.Matrix.translationMatrix(oldCenter.x,oldCenter.y));

      this.parent.transform(DIAGRAMO.Matrix.translationMatrix(-oldCenter.x,-oldCenter.y));
      this.parent.transform(DIAGRAMO.Matrix.rotationMatrix(-angle));
      this.parent.transform(DIAGRAMO.Matrix.translationMatrix(oldCenter.x,oldCenter.y));
      var figBounds = this.parent.getBounds();

      this.parent.transform(DIAGRAMO.Matrix.translationMatrix(-oldCenter.x,-oldCenter.y));
      this.parent.transform(DIAGRAMO.Matrix.rotationMatrix(angle));
      this.parent.transform(DIAGRAMO.Matrix.translationMatrix(oldCenter.x,oldCenter.y));
      newX = p.x;
      newY = p.y;

      var handlerPoint=new DIAGRAMO.Point(this.point.x,this.point.y) //Handler's center point (used to draw it's circle)
      //rotate that as well.
      handlerPoint.transform(DIAGRAMO.Matrix.translationMatrix(-oldCenter.x,-oldCenter.y));
      handlerPoint.transform(DIAGRAMO.Matrix.rotationMatrix(-angle));
      handlerPoint.transform(DIAGRAMO.Matrix.translationMatrix(oldCenter.x,oldCenter.y));

      var props = this.getTransScale(lastMove, newX, newY, figBounds, angle, handlerPoint);
      var transX = props[0];
      var transY = props[1];
      var scaleX = props[2];
      var scaleY = props[3];
      /*By default the NW, NE, SW and SE are scalling keeping the ratio
       *but you can use SHIFT to cause a free (no keep ratio) change
       *So, if no SHIFT pressed we force a "keep ration" resize
       **/
      if(!DIAGRAMO.isShiftPressed() && transX != 0 && transY != 0){//keep ratios, only affects ne/nw resize

          //if we are scaling along the x axis (West or East resize), with an arc(behaves like corner) then scale relative to x movement
          //TODO: what's the reason for this?
          if(this.getCursor()=="w-resize" || this.getCursor()=="e-resize"){
              scaleY = scaleX;
          }
          else { //for everything else, scale based on y
              scaleX = scaleY;
          }
      }


      //move the figure to origine and "unrotate" it
      var matrixToOrigin = DIAGRAMO.Matrix.mergeTransformations(
          DIAGRAMO.Matrix.translationMatrix(-oldCenter.x,-oldCenter.y),
          DIAGRAMO.Matrix.rotationMatrix(-angle),
          DIAGRAMO.Matrix.translationMatrix(oldCenter.x,oldCenter.y)
          );

      //scale matrix
      var scaleMatrix = DIAGRAMO.Matrix.mergeTransformations(
          DIAGRAMO.Matrix.translationMatrix(-transX, -transY),
          DIAGRAMO.Matrix.scaleMatrix(scaleX, scaleY),
          DIAGRAMO.Matrix.translationMatrix(transX, transY)
      );

      var unscaleMatrix = DIAGRAMO.Matrix.mergeTransformations(
          DIAGRAMO.Matrix.translationMatrix(-transX, -transY),
          DIAGRAMO.Matrix.scaleMatrix(1/scaleX, 1/scaleY),
          DIAGRAMO.Matrix.translationMatrix(transX, transY)
      );

      //move and rotate the figure back to its original coordinates
      var matrixBackFromOrigin = DIAGRAMO.Matrix.mergeTransformations(
          DIAGRAMO.Matrix.translationMatrix(-oldCenter.x,-oldCenter.y),
          DIAGRAMO.Matrix.rotationMatrix(angle),
          DIAGRAMO.Matrix.translationMatrix(oldCenter.x,oldCenter.y)
      );

      var directMatrix = DIAGRAMO.Matrix.mergeTransformations(matrixToOrigin, scaleMatrix, matrixBackFromOrigin);
      var reverseMatrix = DIAGRAMO.Matrix.mergeTransformations(matrixToOrigin, unscaleMatrix, matrixBackFromOrigin);


      var cmdScale = new DIAGRAMO.COMMANDS.FigureScaleCommand(this.parent, directMatrix, reverseMatrix);
      cmdScale.execute();
    }
  });
}
