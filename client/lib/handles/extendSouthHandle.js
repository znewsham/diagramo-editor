function ExtendSouthHandle(handleManager, extendFunction){
    DIAGRAMO.Handle.call(this, "ES", handleManager);
    this.extendFunction = extendFunction;
    var self = this;
    this.mouseDown(function(event){
        self.parent.handleManager.dragging = self;
        self.parent.activeHandle = self;
        return false;//we want this to bubble
    });
}

DIAGRAMO.ExtendSouthHandle = ExtendSouthHandle;

DIAGRAMO.ExtendSouthHandle.init = function(){
  DIAGRAMO.ExtendSouthHandle.prototype = _.extend({}, DIAGRAMO.Handle.prototype, {
    action: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      var tmp = new DIAGRAMO.SouthTranslateHandle(this.parent);
      tmp.transform(DIAGRAMO.Matrix.translationMatrix(this.point.x, this.point.y));
      tmp.action(lastMove, newX, newY);
      this.extendFunction();
    }
  });
}
