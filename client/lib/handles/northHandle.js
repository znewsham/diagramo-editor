function NorthTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "n", handleManager);
}

DIAGRAMO.NorthTranslateHandle = NorthTranslateHandle;

DIAGRAMO.NorthTranslateHandle.init = function(){
  DIAGRAMO.NorthTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {

      getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
        /*move the xOy coodinates at the bottom of the figure and then scale*/
        transY = figBounds[3];
        if(newY < figBounds[3]-5){ //North must not get too close to South
            scaleY = (figBounds[3]-newY)/(figBounds[3] - handlerPoint.y);
        }
        return [0, transY, 1, scaleY];
    }
  });
}
