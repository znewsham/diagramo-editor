function SouthEastTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "se", handleManager);
}

DIAGRAMO.SouthEastTranslateHandle = SouthEastTranslateHandle;

DIAGRAMO.SouthEastTranslateHandle.init = function(){
  DIAGRAMO.SouthEastTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {
    getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      transX = figBounds[0];
      transY = figBounds[1];
      if(newX>figBounds[0]+5 && newY>figBounds[1]+5){
          scaleY= (newY-figBounds[1]) / (handlerPoint.y-figBounds[1]);
          scaleX= (newX-figBounds[0]) / (handlerPoint.x-figBounds[0]);
      }
      return [transX, transY, scaleX, scaleY]
    }
  });
}
