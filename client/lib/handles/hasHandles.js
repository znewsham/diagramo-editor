DIAGRAMO.HasHandles = function(){

}
DIAGRAMO.HasHandles.prototype = {
  transform: function(matrix, transformConnector, transformText){
    var bounds = this.getBounds();
    if(!this.topLeft){
      this.topLeft = new DIAGRAMO.Point(bounds[0], bounds[1]);
      this.topRight = new DIAGRAMO.Point(bounds[2], bounds[1]);
    }
    if(this.isGroup){
      DIAGRAMO.Group.prototype.transform.call(this, matrix, transformConnector, transformText);
    }
    else{
      DIAGRAMO.Figure.prototype.transform.call(this, matrix, transformConnector, transformText);
    }
    this.handles.forEach(function(handle){
      handle.transform(matrix);
    });
    this.topLeft.transform(matrix);
    this.topRight.transform(matrix);
    var center = DIAGRAMO.Util.getCenter(bounds);
    var rPoint = _.findWhere(this.handles, {handleType: "r"});
    if(rPoint){
      var midTop = DIAGRAMO.Util.getMiddle(this.topLeft, this.topRight);
      var angle = DIAGRAMO.Util.getAngle(center, midTop);
      var newPoint = DIAGRAMO.Util.getEndPoint(
        center,
        DIAGRAMO.Util.getLength(center, midTop) + 10,
        angle
      );
      rPoint.point.x = newPoint.x;
      rPoint.point.y = newPoint.y;
    }
  },
  clone: function(){
    var ret = DIAGRAMO.Figure.prototype.clone.call(this);
    ret.topLeft = this.topLeft.clone();
    ret.topRight = this.topRight.clone();
    ret.setupHandles();
    return ret;
  },
  paint: function(context){
    DIAGRAMO.Figure.prototype.paint.call(this, context);
    if(this.selected){
      this.handles.forEach(function(handle){
        handle.paint(context);
      })
    }
  },
  contains: function(x, y){
    var ret = DIAGRAMO.Figure.prototype.contains.call(this, x, y);
    this.handles.forEach(function(handle){
        if(handle.contains(x, y)){
            ret = true;
        }
    });
    return ret;
  },
  near: function(x, y, radius){
    var ret = DIAGRAMO.Figure.prototype.near.call(this, x, y, radius);;
    this.handles.forEach(function(handle){
        if(handle.near(x, y, DIAGRAMO.Handle.RADIUS + radius)){
            ret = true;
        }
    });
    return ret;
  },
  getCursor: function(event){
    if(!this.isVisible()){
        return "";
    }
    else{
        var cursor = "";
        this.handles.some(function(handle){
           if(handle.near(event.x, event.y, DIAGRAMO.Handle.RADIUS)){
               cursor = handle.getCursor(event);
               return true;
           }
       });
        return cursor;
    }
  },
  hasHandlesMouseOverHandler: function(event){
      /*var ret = false;
      this.handles.some(function(handle){
         if(handle.near(event.x, event.y, DIAGRAMO.Handle.RADIUS)){
             ret = handle.trigger("mouseOver", [event]);
             return ret == undefined ? false : true;
         }
      });
      return ret;*/
  },
  hasHandlesMouseMoveHandler: function(event){
    if(!this.selected){
      return false;
    }
    var ret = false;
    this.getHandles().some(function(handle){
       if(handle.near(event.x, event.y, DIAGRAMO.Handle.RADIUS)){
           ret = handle.trigger("mouseMove", [event]);
           return ret == undefined ? false : true;
       }
    });
    return ret;
  },
  hasHandlesMouseDownHandler: function(event){
    if(!this.selected){
      return false;
    }
    var ret = false;
    this.getHandles().some(function(handle){
       if(handle.near(event.x, event.y, DIAGRAMO.Handle.RADIUS)){
           ret = handle.trigger("mouseDown", [event]);
           return ret == undefined ? false : true;
       }
    });
    return ret;
  },
}
