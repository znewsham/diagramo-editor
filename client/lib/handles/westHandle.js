function WestTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "w", handleManager);
}

DIAGRAMO.WestTranslateHandle = WestTranslateHandle;

DIAGRAMO.WestTranslateHandle.init = function(){
  DIAGRAMO.WestTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {
    getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      /*move the xOy coordinates at the right of the figure and then scale*/
      transX = figBounds[2];
      if(newX < figBounds[2]-5){ //West(newX) must not get too close to East(figBounds[2])
          scaleX = (figBounds[2]-newX)/(figBounds[2]-handlerPoint.x);
      }
      return [transX, 0, scaleX, 1];
    }
  });
}
