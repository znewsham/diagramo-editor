function EastTranslateHandle(handleManager){
    DIAGRAMO.TranslateHandle.call(this, "e", handleManager);
}

DIAGRAMO.EastTranslateHandle = EastTranslateHandle;

DIAGRAMO.EastTranslateHandle.init = function(){
  DIAGRAMO.EastTranslateHandle.prototype = _.extend({}, DIAGRAMO.TranslateHandle.prototype, {
    getTransScale: function(lastMove, newX, newY, figBounds, angle, handlerPoint){
      /*move the xOy coodinates at the left of the figure (superfluous as we are there already) and then scale*/
      transX = figBounds[0];
      if(newX > figBounds[0]+5){
          scaleX = (newX-figBounds[0])/(handlerPoint.x-figBounds[0]);
      }
      return [transX, 0, scaleX, 1];
    }
  });
}
