function FigureHandle(handleType, handleManager){
    DIAGRAMO.Handle.call(this, handleType, handleManager);
    this.isFigureHandle = true;
}
DIAGRAMO.FigureHandle = FigureHandle;
FigureHandle.new = function(type, handleManager){
  switch(type){
    case "r":
      return new DIAGRAMO.RotateHandle(handleManager);
    case "n":
      return new DIAGRAMO.NorthTranslateHandle(handleManager);
    case "e":
      return new DIAGRAMO.EastTranslateHandle(handleManager);
    case "s":
      return new DIAGRAMO.SouthTranslateHandle(handleManager);
    case "w":
      return new DIAGRAMO.WestTranslateHandle(handleManager);
    case "ne":
      return new DIAGRAMO.NorthEastTranslateHandle(handleManager);
    case "nw":
      return new DIAGRAMO.NorthWestTranslateHandle(handleManager);
    case "se":
      return new DIAGRAMO.SouthEastTranslateHandle(handleManager);
    case "sw":
      return new DIAGRAMO.SouthWestTranslateHandle(handleManager);
  }
}
FigureHandle.init = function(){
    _.extend(FigureHandle.prototype, DIAGRAMO.Handle.prototype, {
    });
    DIAGRAMO.RotateHandle.init();
    DIAGRAMO.TranslateHandle.init();
    DIAGRAMO.NorthTranslateHandle.init();
    DIAGRAMO.EastTranslateHandle.init();
    DIAGRAMO.SouthTranslateHandle.init();
    DIAGRAMO.WestTranslateHandle.init();
    DIAGRAMO.NorthEastTranslateHandle.init();
    DIAGRAMO.NorthWestTranslateHandle.init();
    DIAGRAMO.SouthEastTranslateHandle.init();
    DIAGRAMO.SouthWestTranslateHandle.init();
    DIAGRAMO.ExtendSouthHandle.init();
};
