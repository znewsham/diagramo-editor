
/**
 *A connection point that is attached to a figure and can accept connectors
 *
 *@constructor
 *@this {ConnectionPoint}
 *@param {Connector | Figure} parent - the parent to which this ConnectionPoint is attached. It can be either a {Figure} or a {Connector}
 *@param {Point} point - coordinate of this connection point, better than using x and y, because when we move "snap to" this
 * connectionPoint the line will follow
 *@param {Number} id - unique id to the parent figure
 *@param {String} connectionPointType - the connectionPointType of the parent. It can be either 'figure' or 'connector'
 *
 *@author Zack Newsham <zack_newsham@yahoo.co.uk>
 *@author Alex Gheorghiu <alex@scriptoid.com>
 */
function ConnectorConnectionPoint(type, point,modifiablePoints, connector){
    DIAGRAMO.ConnectorHandle.call(this, type, point, modifiablePoints, connector);
    DIAGRAMO.ConnectionPoint.call(this);

    /**Current connection point color*/
    this.color = DIAGRAMO.ConnectionPoint.NORMAL_COLOR;
    this.connectionPointType = DIAGRAMO.ConnectionPoint.TYPE_CONNECTOR;
    /**Radius of the connection point*/
    this.radius = 3;

    /**Serialization connectionPointType*/
    this.oType = 'ConnectionPoint'; //object connectionPointType used for JSON deserialization
    var self = this;
    this.isConnectionPoint = true;
    this.beforeConnect(function(other){
      return self.parent == other.parent;
    })
    this.beforeConnect(function(other){
      this.parent.zIndex = other.parent.zIndex - 1;
    })

    //its a handle, if we interact with it, transform the other end point
    this.beforeAction(this.beforeTransformSnap);
    this.beforeTransform(this.beforeTransformSnap);

}
DIAGRAMO.ConnectorConnectionPoint = ConnectorConnectionPoint;


/**Creates a {ConnectionPoint} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {ConnectionPoint} a newly constructed ConnectionPoint
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
ConnectorConnectionPoint.load = function(o, newConnectionPoint){
    if(newConnectionPoint == undefined){
        newConnectionPoint = new DIAGRAMO.ConnectorConnectionPoint(o.handleType, DIAGRAMO.Point.load(o.point), null); //fake constructor
    }
    newConnectionPoint = DIAGRAMO.ConnectorHandle.load(o, newConnectionPoint);
    return newConnectionPoint;
}

/**Creates a an {Array} of {ConnectionPoint} out of JSON parsed array
 *@param {JSONObject} v - the JSON parsed {Array}
 *@return {Array} of newly loaded {ConnectionPoint}s
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
ConnectorConnectionPoint.loadArray = function(v){
    var newConnectionPoints = [];

    for(var i=0; i<v.length; i++){
        newConnectionPoints.push(ConnectionPoint.load(v[i]));
    }

    return newConnectionPoints;
}

/**Clones an array of {ConnectionPoint}s
 *@param {Array} v - the array of {ConnectionPoint}s
 *@return an {Array} of {ConnectionPoint}s
 **/
ConnectorConnectionPoint.cloneArray = function(v){
    var newConnectionPoints = [];
    for(var i=0; i< v.length; i++){
        newConnectionPoints.push(v[i].clone());
    }
    return newConnectionPoints;
}

ConnectorConnectionPoint.init = function(){
    _.extend(ConnectorConnectionPoint.prototype, DIAGRAMO.ConnectorHandle.prototype,DIAGRAMO.ConnectionPoint.prototype,{

        isEnd: function(){
          return this.parent && this.parent.getConnectionPoints()[1] == this;
        },
        isStart: function(){
          return this.parent && this.parent.getConnectionPoints()[0] == this;
        },
        beforeTransformSnap: function(matrix){
          var other = this.parent.getOtherConnectionPoint(this);
          if(other.glue && other.glue.figureConPoint instanceof DIAGRAMO.MagicFigureConnectionPoint){
            other.glue.figureConPoint.snapConnectionPoint(other);
          }
          return true;
        },
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.ConnectorHandle.prototype.toJSONObject.call(this), {
                oType: "DIAGRAMO.ConnectorConnectionPoint"
            });

            return ret;
        },

        clone: function(){
            var ret = new ConnectorConnectionPoint(this.handleType, this.point.clone(), this.modifiablePoints.map(function(obj){return {type: obj.type, point: obj.point.clone()};}), this.parent);

            return ret;
        },

        //should we block the connection
        beforeConnectHandler: function(conPoint){
            return conPoint.connectionPointType != DIAGRAMO.ConnectionPoint.TYPE_FIGURE
        },

        /**Compares to another ConnectionPoint
         *@param {ConnectionPoint} anotherConnectionPoint - the other connection point
         *@return {Boolean} - true if equals, false otherwise
         **/
        equals:function(anotherConnectionPoint){

            return this.id == anotherConnectionPoint.id
            && this.point.equals(anotherConnectionPoint.point)
            && this.parentId == anotherConnectionPoint.parentId
            && this.connectionPointType == anotherConnectionPoint.connectionPointType
            && this.color == anotherConnectionPoint.color
            && this.radius == anotherConnectionPoint.radius;
        },

        /**
         *Paints the ConnectionPoint into a Context
         *@param {Context} context - the 2D context
         **/
        paint:function(context){
            context.save();
            context.fillStyle = this.color;
            context.strokeStyle = '#000000';
            context.lineWidth = 1;
            context.beginPath();
            context.arc(this.point.x, this.point.y, DIAGRAMO.ConnectionPoint.RADIUS, 0, (Math.PI/180)*360, false);
            context.stroke();
            context.fill();
            context.restore();
        },


        /**Highlight the connection point*/
        highlight:function(){
            this.color = DIAGRAMO.ConnectionPoint.OVER_COLOR;
        },

        /**Un-highlight the connection point*/
        unhighlight:function(){
            this.color = DIAGRAMO.ConnectionPoint.NORMAL_COLOR;
        },


        /**Tests to see if a point (x, y) is within a range of current ConnectionPoint
         *@param {Numeric} x - the x coordinate of tested point
         *@param {Numeric} y - the x coordinate of tested point
         *@return {Boolean} - true if inside, false otherwise
         *@author Alex Gheorghiu <alex@scriptoid.com>
         **/
        contains:function(x, y){
            return this.near(x, y, DIAGRAMO.ConnectionPoint.RADIUS);
        },

        /**Tests to see if a point (x, y) is within a specified range of current ConnectionPoint
         *@param {Numeric} x - the x coordinate of tested point
         *@param {Numeric} y - the x coordinate of tested point
         *@param {Numeric} radius - the radius around this point
         *@return {Boolean} - true if inside, false otherwise
         *@author Alex Gheorghiu <alex@scriptoid.com>
         **/
        near:function(x, y, radius){
            return this.point.near(x,y,radius);
        },


        /**A String representation of the point*/
        toString:function(){
            return "ConnectionPoint id = " + this.id  + ' point = ['+ this.point + '] ,connectionPointType = ' + this.connectionPointType + ", parentId = " + this.parentId + ")";
        },
        transform: function(matrix){
          this.trigger("beforeTransform", [matrix]);
            var self = this;
            this.point.transform(matrix);
            var first = true;
            this.modifiablePoints.forEach(function(obj){
                if(obj.point != self.point){
                    if(first){
                        first = false;
                        obj.point.transform(matrix);
                        return;
                    }
                    if(obj.type == "h"){
                        obj.point.x = self.point.x
                    }
                    else if(obj.type == 'v'){
                        obj.point.y = self.point.y;
                    }
                    else{
                        obj.point.transform(matrix);
                    }
                }
            });
        },
    });
}
