DIAGRAMO.DefaultOnConnectDelegate = function(){

}

DIAGRAMO.DefaultOnConnectDelegate.jaggedSolutions = function(connector){
    var solutions = [];
    var startExitPoint = null;
    var endExitPoint = null;

    //find start exit point
    var conPoints = connector.getConnectionPoints();
    var startFigure = conPoints[0].glue == null ? null : conPoints[0].glue.figureConPoint.parent;
    var sBounds = startFigure == null ? null : startFigure.getBounds();
    if(sBounds != null){
        var potentialExits = [];

        potentialExits.push(new DIAGRAMO.Point(conPoints[0].point.x, sBounds[1] - DIAGRAMO.FIGURE_ESCAPE_DISTANCE)); //north
        potentialExits.push(new DIAGRAMO.Point(sBounds[2] + DIAGRAMO.FIGURE_ESCAPE_DISTANCE, conPoints[0].point.y)); //east
        potentialExits.push(new DIAGRAMO.Point(conPoints[0].point.x, sBounds[3] + DIAGRAMO.FIGURE_ESCAPE_DISTANCE)); //south
        potentialExits.push(new DIAGRAMO.Point(sBounds[0] - DIAGRAMO.FIGURE_ESCAPE_DISTANCE, conPoints[0].point.y)); //west

        //pick closest exit point
        startExitPoint = potentialExits[0];
        for(var i=1; i < potentialExits.length; i++){
            if(DIAGRAMO.Util.distance(conPoints[0].point, potentialExits[i]) < DIAGRAMO.Util.distance(conPoints[0].point, startExitPoint)){
                startExitPoint = potentialExits[i];
            }
        }

        if(startExitPoint == null){
            alert("No way");
        }
    }


    var endFigure = conPoints[conPoints.length - 1].glue == null ? null : conPoints[conPoints.length - 1].glue.figureConPoint.parent;
    var eBounds = endFigure == null ? null : endFigure.getBounds();
    //find end exit point
    if(eBounds != null){
        var potentialExits = [];

        potentialExits.push(new DIAGRAMO.Point(conPoints[conPoints.length - 1].point.x, eBounds[1] - DIAGRAMO.FIGURE_ESCAPE_DISTANCE)); //north
        potentialExits.push(new DIAGRAMO.Point(eBounds[2] + DIAGRAMO.FIGURE_ESCAPE_DISTANCE, conPoints[conPoints.length - 1].point.y)); //east
        potentialExits.push(new DIAGRAMO.Point(conPoints[conPoints.length - 1].point.x, eBounds[3] + DIAGRAMO.FIGURE_ESCAPE_DISTANCE)); //south
        potentialExits.push(new DIAGRAMO.Point(eBounds[0] - DIAGRAMO.FIGURE_ESCAPE_DISTANCE, conPoints[conPoints.length - 1].point.y)); //west

        //pick closest exit point
        endExitPoint = potentialExits[0];
        for(var i=1; i < potentialExits.length; i++){
            if(DIAGRAMO.Util.distance(conPoints[conPoints.length - 1].point, potentialExits[i]) < DIAGRAMO.Util.distance(conPoints[conPoints.length - 1].point, endExitPoint)){
                endExitPoint = potentialExits[i];
            }
        }

        if(endExitPoint == null){
            alert("No way");
        }
    }

    //Basic solution (basic kit :p)
    var s = [conPoints[0].point];
    var gapIndex = 0; //the index of the gap (where do we need to insert new points) DO NOT CHANGE IT
    if(startExitPoint){
        s.push(startExitPoint);
        gapIndex = 1;
    }
    if(endExitPoint){
        s.push(endExitPoint);
    }
    s.push(conPoints[conPoints.length - 1].point);



    //SO - no additional points
    var s0 = DIAGRAMO.Point.cloneArray(s);
    solutions.push(['s0', 's0', s0]);



    //S1
    var s1 = DIAGRAMO.Point.cloneArray(s);

    //first variant
    var s1_1 = DIAGRAMO.Point.cloneArray(s1);
    s1_1.splice(gapIndex + 1, 0, new DIAGRAMO.Point(s1_1[gapIndex].x , s1_1[gapIndex+1].y) );
    solutions.push(['s1', 's1_1', s1_1]);

    //second variant
    var s1_2 = DIAGRAMO.Point.cloneArray(s1);
    s1_2.splice(gapIndex + 1, 0, new DIAGRAMO.Point(s1_2[gapIndex+1].x , s1_2[gapIndex].y) );
    solutions.push(['s1', 's1_2', s1_2]);


    //S2

    //Variant I
    var s2_1 = DIAGRAMO.Point.cloneArray(s);
    var s2_1_1 = new DIAGRAMO.Point( (s2_1[gapIndex].x + s2_1[gapIndex+1].x) / 2,  s2_1[gapIndex].y);
    var s2_1_2 = new DIAGRAMO.Point( (s2_1[gapIndex].x + s2_1[gapIndex+1].x) / 2,  s2_1[gapIndex+1].y);
    s2_1.splice(gapIndex + 1, 0, s2_1_1, s2_1_2);
    solutions.push(['s2', 's2_1', s2_1]);


    //Variant II
    var s2_2 = DIAGRAMO.Point.cloneArray(s);
    var s2_2_1 = new DIAGRAMO.Point( s2_2[gapIndex].x, (s2_2[gapIndex].y + s2_2[gapIndex+1].y)/2 );
    var s2_2_2 = new DIAGRAMO.Point( s2_2[gapIndex+1].x, (s2_2[gapIndex].y + s2_2[gapIndex+1].y)/2);
    s2_2.splice(gapIndex + 1, 0, s2_2_1, s2_2_2);
    solutions.push(['s2', 's2_2', s2_2]);


    //Variant III
    var s2_3 = DIAGRAMO.Point.cloneArray(s);
    //find the amount (stored in delta) of pixels we need to move right so no intersection with a figure will be present
    //!See:  /documents/specs/connected_figures_deltas.jpg file

    var eastExits = [s2_3[gapIndex].x + 20, s2_3[gapIndex+1].x + 20]; //add points X coordinates to be able to generate Variant III even in the absence of figures :p

    if(sBounds){
        eastExits.push(sBounds[2] + 20);
    }

    if(eBounds){
        eastExits.push(eBounds[2] + 20);
    }

    var eastExit = DIAGRAMO.Util.max(eastExits);
    var s2_3_1 = new DIAGRAMO.Point( eastExit, s2_3[gapIndex].y );
    var s2_3_2 = new DIAGRAMO.Point( eastExit, s2_3[gapIndex+1].y );
    s2_3.splice(gapIndex + 1, 0, s2_3_1, s2_3_2);
    solutions.push(['s2', 's2_3', s2_3]);


    //Variant IV
    var s2_4 = DIAGRAMO.Point.cloneArray(s);
    //find the amount (stored in delta) of pixels we need to move up so no intersection with a figure will be present
    //!See:  /documents/specs/connected_figures_deltas.jpg file

    var northExits = [s2_4[gapIndex].y - 20, s2_4[gapIndex+1].y - 20]; //add points y coordinates to be able to generate Variant III even in the absence of figures :p

    if(sBounds){
        northExits.push(sBounds[1] - 20);
    }

    if(eBounds){
        northExits.push(eBounds[1] - 20);
    }

    var northExit = DIAGRAMO.Util.min(northExits);
    var s2_4_1 = new DIAGRAMO.Point( s2_4[gapIndex].x, northExit);
    var s2_4_2 = new DIAGRAMO.Point( s2_4[gapIndex+1].x, northExit);
    s2_4.splice(gapIndex + 1, 0, s2_4_1, s2_4_2);
    solutions.push(['s2', 's2_4', s2_4]);


    //Variant V
    var s2_5 = DIAGRAMO.Point.cloneArray(s);
    //find the amount (stored in delta) of pixels we need to move left so no intersection with a figure will be present
    //!See:  /documents/specs/connected_figures_deltas.jpg file

    var westExits = [s2_5[gapIndex].x - 20, s2_5[gapIndex+1].x - 20]; //add points x coordinates to be able to generate Variant III even in the absence of figures :p

    if(sBounds){
        westExits.push(sBounds[0] - 20);
    }

    if(eBounds){
        westExits.push(eBounds[0] - 20);
    }

    var westExit = DIAGRAMO.Util.min(westExits);
    var s2_5_1 = new DIAGRAMO.Point( westExit, s2_5[gapIndex].y);
    var s2_5_2 = new DIAGRAMO.Point( westExit, s2_5[gapIndex+1].y);
    s2_5.splice(gapIndex + 1, 0, s2_5_1, s2_5_2);
    solutions.push(['s2', 's2_5', s2_5]);


    //Variant VI
    var s2_6 = DIAGRAMO.Point.cloneArray(s);
    //find the amount (stored in delta) of pixels we need to move down so no intersection with a figure will be present
    //!See:  /documents/specs/connected_figures_deltas.jpg file

    var southExits = [s2_6[gapIndex].y + 20, s2_6[gapIndex+1].y + 20]; //add points y coordinates to be able to generate Variant III even in the absence of figures :p

    if(sBounds){
        southExits.push(sBounds[3] + 20);
    }

    if(eBounds){
        southExits.push(eBounds[3] + 20);
    }

    var southExit = DIAGRAMO.Util.max(southExits);
    var s2_6_1 = new DIAGRAMO.Point( s2_6[gapIndex].x, southExit);
    var s2_6_2 = new DIAGRAMO.Point( s2_6[gapIndex+1].x, southExit);
    s2_6.splice(gapIndex + 1, 0, s2_6_1, s2_6_2);
    solutions.push(['s2', 's2_6', s2_6]);



    //FILTER solutions

    /*Algorithm
     * 0. solutions are ordered from minimmun nr of points to maximum >:)
     * 1. remove all solutions that are not orthogonal (mainly s0 solution)
     * 2. remove all solutions that go backward (we will not need them ever)
     * 3. remove all solutions with intersections
     * 4. pick first class of solutions with same nr of points (ex: 2)
     * 5. pick the first solution with 90 degree angles (less turnarounds)
     * (not interesteted) sort by length :p
     */

    //1. filter non ortogonal solutions
    var orthogonalSolution = [];
    for(var l=0; l<solutions.length; l++){
        var solution = solutions[l][2];
        if(DIAGRAMO.Util.orthogonalPath(solution)){
            orthogonalSolution.push(solutions[l]);
        }
    }
    solutions = orthogonalSolution;

    //do not allow start and end points to coincide - ignore them
    if(!conPoints[0].point.equals(conPoints[conPoints.length - 1].point)){
        var forwardSolutions = [];
        var temp = '';
        for(var l=0; l<solutions.length; l++){
            var solution = solutions[l][2];
            if(DIAGRAMO.Util.forwardPath(solution)){
                forwardSolutions.push(solutions[l]);
            }
            else{
                temp = temp +  "\n\t" + solution;
            }
        }
        solutions = forwardSolutions;
    }

    var nonIntersectionSolutions = []
    for(var l=0; l<solutions.length; l++){
        var solution = solutions[l][2];
        //Log.info("Solution id= " + solutions[l][1] + ' nr points = ' + solution.length + ", points = " + solution);
        var intersect = false;

        var innerLines = solution.slice(); //just a shallow copy

        /*If any bounds just trim the solution. So we avoid the strange case when a connection
         *startes from a point on a figure and ends inside of the same figure, but not on a connection point*/
        if(eBounds || sBounds){
            //i0nnerLines = innerLines.slice(0, innerLines.length - 1);
            innerLines = innerLines.slice(1, innerLines.length - 1);
            //Log.info("\t eBounds present,innerLines nr. points = " + innerLines.length + ", points = " + innerLines);
        }



        //now test for intersection
        if(sBounds){
            intersect = intersect || DIAGRAMO.Util.polylineIntersectsRectangle(innerLines, sBounds);
        }
        if(eBounds){
            intersect = intersect || DIAGRAMO.Util.polylineIntersectsRectangle(innerLines, eBounds);
        }

        if(!intersect){
            nonIntersectionSolutions.push(solutions[l]);
        }
    }
    //If all solutions intersect than connector is destiny  :) and just ignore the intersection filter
    if(nonIntersectionSolutions.length != 0){
        //reasign to solutions
        solutions = nonIntersectionSolutions;
    }

    //4. get first class of solutions with same nr of points
    if(solutions.length == 0){
        alert("connector is not possible");
    }

    var firstSolution = solutions[0][2]; //pick first solution
    var nrOfPoints = firstSolution.length;
    var sameNrPointsSolution = [];

    for(var l=0; l<solutions.length; l++){
        var solution = solutions[l][2];
        if(solution.length == nrOfPoints){
            sameNrPointsSolution.push(solutions[l]);
        }
    }

    solutions = sameNrPointsSolution;




    /*5. Pick the first solution with 90 degree angles (less turnarounds)
    *in case we have more than one solution in our class
    */
    var solIndex = 0;
    for(var l=0; l<solutions.length; l++){
        var solution = solutions[l][2];
        if(DIAGRAMO.Util.scorePath( solutions[solIndex][2] ) < DIAGRAMO.Util.scorePath( solutions[l][2] ) ){
            solIndex = l;
        }
    }
    solutions = [solutions[solIndex]];
    if(solutions[0][2].length > 3){
        var start = solutions[0][2][0];
        var mid = solutions[0][2][1];
        var end = solutions[0][2][2];
        if(DIAGRAMO.Util.pointOnLine(start, end, mid)){
            solutions[0][2].splice(1, 1);
        }
    }
    if(solutions[0][2].length > 3){
        var start = solutions[0][2][solutions[0][2].length - 1];
        var mid = solutions[0][2][solutions[0][2].length - 2];
        var end = solutions[0][2][solutions[0][2].length - 3];
        if(DIAGRAMO.Util.pointOnLine(start, end, mid)){
            solutions[0][2].splice(solutions[0][2].length - 2, 1);
        }
    }
    return solutions;
}

DIAGRAMO.DefaultOnConnectDelegate.smoothSolutions = function(connector){
    var solutions = DIAGRAMO.DefaultOnConnectDelegate.jaggedSolutions(connector);
    var option = 3;

    switch(option){
        case 0:
            //do nothing
            break;

        case 1: //add intermediate points
            //Add the middle point for start and end segment so that we "force" the
            //curve to both come "perpendicular" on bounds and also make the curve
            //"flee" more from bounds (on exit)
            for(var s=0; s<solutions.length; s++){
                var solTurningPoints = solutions[s][2];

                //first segment
                var a1 = solTurningPoints[0];
                var a2 = solTurningPoints[1];
                var startMiddlePoint = DIAGRAMO.Util.getMiddle(a1, a2);
                solTurningPoints.splice(1,0, startMiddlePoint);

                //last segment
                var a3 = solTurningPoints[solTurningPoints.length - 2];
                var a4 = solTurningPoints[solTurningPoints.length - 1];
                var endMiddlePoint = DIAGRAMO.Util.getMiddle(a3, a4);
                solTurningPoints.splice(solTurningPoints.length - 1, 0, endMiddlePoint);
            }
            break;

        case 2: //remove points
            for(var s=0; s<solutions.length; s++){
                var solType= solutions[s][0];
                if(solType == 's1' || solType == 's2'){
                    var solTurningPoints = solutions[s][2];
                    solTurningPoints.splice(1,1);
                    solTurningPoints.splice(solTurningPoints.length - 2, 1);
                }
            }
            break;

        case 3:
            /*remove colinear point for s1 as it seems that more colinear points do not look good
             * on organic solutions >:D*/
            for(var s=0; s<solutions.length; s++){
                var solType= solutions[s][0];
                if(solType == 's1'){
                    var solTurningPoints = solutions[s][2];
                    var reducedSolution = DIAGRAMO.Util.collinearReduction(solTurningPoints);
                    solutions[s][2] = reducedSolution;
                }
            }
            break;
    }//end switch
    return solutions;
}
