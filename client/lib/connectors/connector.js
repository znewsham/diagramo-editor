function Connector(paintable){
    DIAGRAMO.PointBasedPrimitive.call(this);
    this.mainItem = paintable;
    this.paintables.push(paintable);
    this.style.lineWidth = DIAGRAMO.PROPERTIES.CLOUD_LINEWIDTH;
    this.style.strokeStyle = "#000000";
    this.handles = [];
    this.activeHandle = null;
    this.isConnector = true;
    this.selected = false;
    this.afterConnectHandlers = null;
    this.beforeTransform(this.beforeTransformHandler);
    this.onConnectDelegate = DIAGRAMO.DefaultOnConnectDelegate;
    this.endItem = null;
    this.startItem = null;
    var points = this.mainItem.getPoints();
    this.endText = new DIAGRAMO.Text("[]", points[points.length - 1].x + 10, points[points.length  - 1].y + 10, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize);
    this.midText = new DIAGRAMO.Text(DIAGRAMO.FigureDefaults.textStr, points[Math.floor(points.length / 2)].x, points[Math.floor(points.length / 2)].y, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize)
    this.properties = [];
    this.properties.push(new DIAGRAMO.PROPERTIES.TextProperty(this, "midText.str", "Mid Text", {multiline: true}));
    this.properties.push(new DIAGRAMO.PROPERTIES.MultiProperty(this, "Text Style", [
        new DIAGRAMO.PROPERTIES.SelectProperty(this, "midText.size", "", DIAGRAMO.PROPERTIES.TEXT_SIZES, {width: "75px", marginRight: "5px"}),
        new DIAGRAMO.PROPERTIES.ColorProperty(this, "midText.style.fillStyle", "")
    ]));
    this.properties.push(new DIAGRAMO.PROPERTIES.MultiProperty(this, "Line Style", [
        new DIAGRAMO.PROPERTIES.SelectProperty(this, "style.lineWidth", "Line Width", DIAGRAMO.PROPERTIES.LINE_WIDTHS, {width: "75px", marginRight: "5px"}),
        new DIAGRAMO.PROPERTIES.ColorProperty(this, "strokeStyle", "Line Color")
    ]));
    this.properties.push(new DIAGRAMO.PROPERTIES.SelectProperty(this, "lineStyle", "", DIAGRAMO.PROPERTIES.CONNECTOR_LINE_STYLES, {width: "75px", marginRight: "25px"}));
    this.properties.push(new DIAGRAMO.PROPERTIES.SelectProperty(this, "startItem", "Start Style", DIAGRAMO.PROPERTIES.CONNECTOR_STYLES, {width: "100px"}));
    this.properties.push(new DIAGRAMO.PROPERTIES.SelectProperty(this, "endItem", "End Style", DIAGRAMO.PROPERTIES.CONNECTOR_STYLES, {width: "100px"}));

    this.mouseOver(this.hasHandlesMouseOverHandler);
    this.mouseMove(this.hasHandlesMouseMoveHandler);
    this.mouseDown(this.hasHandlesMouseDownHandler);
    this.keyDown(function(event){
      if(this.isConnected()){
        return false;
      }
      if(event.keyCode == DIAGRAMO.KEYS.UP){
        this.transform(DIAGRAMO.Matrix.translationMatrix(0, -1));
      }
      else if(event.keyCode == DIAGRAMO.KEYS.DOWN){
        this.transform(DIAGRAMO.Matrix.translationMatrix(0, 1));
      }
      else if(event.keyCode == DIAGRAMO.KEYS.LEFT){
        this.transform(DIAGRAMO.Matrix.translationMatrix(-1, 0));
      }
      else if(event.keyCode == DIAGRAMO.KEYS.RIGHT){
        this.transform(DIAGRAMO.Matrix.translationMatrix(1, 0));
      }
      else{
        return false;
      }
      return true;
    })
    this.afterTransform(function(matrix){
        var points = this.mainItem.getPoints();
        if(points.length == 0){
          return;
        }
        this.endText.vector[0].x = points[points.length - 1].x + 10;
        this.endText.vector[0].y = points[points.length - 1].y + 10;
        this.endText.vector[1].x = points[points.length - 1].x + 10;
        this.endText.vector[1].y = points[points.length - 1].y - 30;

        var low = points[Math.floor(points.length / 2)];
        var x,y;
        if(points.length % 2 == 0){
            var high = low;
            low = points[Math.floor(points.length / 2) - 1];
            x =  Math.min(low.x, high.x) + (Math.max(low.x, high.x) - Math.min(low.x, high.x)) / 2;
            y =  Math.min(low.y, high.y) + (Math.max(low.y, high.y) - Math.min(low.y, high.y)) / 2;

        }
        else{
            x = low.x;
            y = low.y;
        }
        this.midText.vector[0].x = x;
        this.midText.vector[0].y = y;
        this.midText.vector[1].x = x;
        this.midText.vector[1].y = y - 20;
    });
}
DIAGRAMO.Connector = Connector;

Connector.load = function(jsonObject, connector){
    if(connector == undefined){
        connector = new DIAGRAMO.CONNECTORS.Connector();
    }
    connector.paintables = [];
    connector.mainItem = DIAGRAMO.load(jsonObject.mainItem);
    if(jsonObject.paintDelegate){
      if(jsonObject.paintDelegate.paintables){
        jsonObject.paintDelegate.paintables[0].item = connector.mainItem;
      }
      else{
        jsonObject.paintDelegate.item = connector.mainItem;
      }
      connector.paintDelegate = DIAGRAMO.load(jsonObject.paintDelegate);
    }
    connector.paintables.push(connector.mainItem);
    connector.id = jsonObject.id;
    connector.style = DIAGRAMO.Style.load(jsonObject.style)
    connector.handles = [];
    connector.endItem = jsonObject.endItem == null ? null : DIAGRAMO.load(jsonObject.endItem);
    connector.startItem = jsonObject.startItem == null ? null : DIAGRAMO.load(jsonObject.startItem);
    if(connector.endItem){
        connector.endItem.style = connector.style;
    }
    if(connector.startItem){
        connector.startItem.style = connector.style;
    }
    if(connector.style.lineStyle){
      connector.setLineStyle(connector.style.lineStyle);
    }
    connector.midText = DIAGRAMO.Text.load(jsonObject.midText);
    connector.endText = DIAGRAMO.Text.load(jsonObject.endText);
    jsonObject.handles.forEach(function(handle){
        var han = DIAGRAMO.load(handle);
        connector.handles.push(han);
        han.parent = connector;
    });
    connector.isNotFinished = false;
    connector.buildFromPoints(connector.mainItem.points || connector.mainItem.getPoints(), connector.getConnectionPoints());
    connector.trigger("afterTransform", DIAGRAMO.Matrix.IDENTITY);
    return connector;
}
Connector.init = function(){
    _.extend(Connector.prototype, DIAGRAMO.PointBasedPrimitive.prototype, DIAGRAMO.HasHandles.prototype, {

        clone:function(){
            var obj = window;
            var parts = this.oType.split(".");
            for(var i = 0; i < parts.length - 1; i++){
              obj = obj[parts[i]];
            }
            var points = this.getPoints();
            var ret = new obj[parts[parts.length - 1]](points[0].clone(), points[points.length - 1].clone());

            //ret.mainItem = this.mainItem.clone();
            if(this.endItem){
              ret.setEndItem(this.endItem.name);
            }
            if(this.startItem){
              ret.setStartItem(ret.startItem.name);
            }
            if(this.paintDelegate){
              ret.paintDelegate = new DIAGRAMO.Primitive();
              ret.paintDelegate.paintables.push(new this.paintDelegate.paintables[0].constructor(ret.mainItem, this.paintDelegate.paintables[0].startIndex, this.paintDelegate.paintables[0].pointIndexes));
            }
            ret.style = this.style.clone();
            return ret;
        },
        getGlues: function(){
            var ret = [];
            this.getConnectionPoints().forEach(function(conPoint){
                if(conPoint.glue){
                    ret.push(conPoint.glue);
                }
            });
            return ret;
        },
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.PointBasedPrimitive.prototype.toJSONObject.call(this), {
                oType: "DIAGRAMO.Connector",
                mainItem: this.mainItem.toJSONObject(),
                endText: this.endText.toJSONObject(),
                midText: this.midText.toJSONObject(),
                endItem: !this.endItem  ? null : this.endItem.toJSONObject(),
                startItem: !this.startItem  ? null : this.startItem.toJSONObject(),
                handles: []
            });

            this.handles.forEach(function(handle){
                ret.handles.push(handle.toJSONObject());
            });

            return ret;
        },
	
        setStrokeStyle: function(strokeStyle){
          this.style.strokeStyle = strokeStyle;
          if(this.mainItem instanceof DIAGRAMO.Path){
            this.mainItem.paintables.forEach(function(p){
              p.style.strokeStyle = undefined;
            })
          }
        },
        getStrokeStyle: function(){
          return this.style.strokeStyle;
        },
        setLineStyle: function(lineStyle){
            if(this.style.lineStyle){
                var oldDelegate = _.findWhere(DIAGRAMO.PROPERTIES.CONNECTOR_LINE_STYLES, {Value: this.style.lineStyle}).delegate;
                if(oldDelegate){
                    this.paintDelegate = null
                }
            }
          this.mainItem.paintables.forEach(function(paintable){
            if(paintable.paintDelegate){
              paintable.paintDelegate = null;
            }
          });
          var newDelegate = _.findWhere(DIAGRAMO.PROPERTIES.CONNECTOR_LINE_STYLES, {Value: lineStyle}).delegate;
          if(newDelegate){
            this.paintDelegate = new DIAGRAMO.Primitive();
            this.paintDelegate.paintables.push(new DIAGRAMO[newDelegate](this.mainItem, 0, null));
          }
          this.style.lineStyle = lineStyle;
          if(this.mainItem instanceof DIAGRAMO.Path){
            this.mainItem.paintables.forEach(function(p){
              p.style.lineStyle = undefined;
            })
          }
        },
        getLineStyle: function(){
            return this.style.lineStyle;
        },
        getStartItem: function(){
            return this.startItem ? this.startItem.name : "none";
        },
        getEndItem: function(){
            return this.endItem ? this.endItem.name : "none";
        },
        setStartItem: function(itemName){
          if(!itemName){
            return;
          }
          var figureFunctionNameParts = _.findWhere(DIAGRAMO.PROPERTIES.CONNECTOR_STYLES,{Value: itemName}).FigureFunction.split(".");
          var obj = window;
          for(var i = 0; i < figureFunctionNameParts.length; i++){
              obj = obj[figureFunctionNameParts[i]];
          }

          this.startItem = new obj(this.getPoints()[0].x,this.getPoints()[0].y, this.style, DIAGRAMO.Util.getAngle(this.getPoints()[0], this.getPoints()[1]));
          this.updateEndPoints();
        },
        setEndItem: function(itemName){
          if(!itemName){
            return;
          }
          if(!_.findWhere(DIAGRAMO.PROPERTIES.CONNECTOR_STYLES,{Value: itemName}).FigureFunction){
            this.endItem = null;
          }
          else{
            var figureFunctionNameParts = _.findWhere(DIAGRAMO.PROPERTIES.CONNECTOR_STYLES,{Value: itemName}).FigureFunction.split(".");
            var obj = window;
            for(var i = 0; i < figureFunctionNameParts.length; i++){
                obj = obj[figureFunctionNameParts[i]];
            }

            var points = this.getPoints();
            this.endItem = new obj(points[points.length - 1].x,points[points.length - 1].y, this.style, DIAGRAMO.Util.getAngle(points[points.length - 1], points[points.length - 2]));
          }
          this.updateEndPoints();
        },
        beforeTransformHandler: function(){
            return !this.isGlued();
        },
        isConnected: function(){
          var ret = false;
          this.getConnectionPoints().forEach(function(cp){
            if(cp.glue){
              ret = true;
            }
          });
          return ret;
        },
        afterConnect: function(eventHandler){
            if(_.isUndefined(this.afterConnectHandlers) || _.isNull(this.afterConnectHandlers)){
                this.afterConnectHandlers = [];
            }
            this.afterConnectHandlers.push(eventHandler);
        },
        contains: function(x, y){
            return this.near(x, y, 2);
        },
        near: function(x, y, radius){
            if(this.mainItem.near(x, y, radius)){
                return true;
            }
            var ret = false;
            this.handles.forEach(function(handle){
                if(handle.near(x, y, DIAGRAMO.Handle.RADIUS + radius)){
                    ret = true;
                }
            });
            return ret;
        },
        getHandles: function(){
            return this.handles;
        },
        getConnectionPoints: function(){
            return _.where(this.handles, {isConnectionPoint: true});
        },
        getOtherConnectionPoint: function(conPoint){
          var conPoints = this.getConnectionPoints();
          return conPoint == conPoints[0] ? conPoints[1] : conPoints[0];
        },
        subPaint: function(context){
            context.save();
            DIAGRAMO.PointBasedPrimitive.prototype.paint.call(this,context);
            var oldStyle = this.style;
            if(this.startItem){
                this.startItem.style = this.startItem.style.clone();
                this.startItem.style.lineStyle = "continuous";
                this.startItem.paint(context);
                this.startItem.style = oldStyle;
            }
            if(this.endItem){
                this.endItem.style = this.endItem.style.clone();
                this.endItem.style.lineStyle = "continuous";
                this.endItem.paint(context);
                this.endItem.style = oldStyle;
            }
            if(this.selected){
                this.handles.forEach(function(handle){
                    handle.paint(context);
                });
            }
            this.endText.paint(context);
            this.midText.paint(context);
            context.restore();
        },
        paint: function(context){
          if(this.selected){
            var oldStyle = this.style.strokeStyle;
            var oldWidth = this.style.lineWidth;
            var oldStyles = [];
            var self = this;
            this.style.strokeStyle = "lime";
            this.style.lineWidth = oldWidth * 2;
            var segment = false;
            if(this.mainItem instanceof DIAGRAMO.Path){
              this.mainItem.paintables.forEach(function(p){
                oldStyles.push(p.style.clone());
                if(!self.getSelectedComponent()){
                  p.style = self.style.clone();
                }
                if(p == self.getSelectedComponent() && p.selected){
                  p.style = self.style.clone();
                  //p.style.strokeStyle = "lightblue";
                  segment = true;
                }
              });
            }
            if(segment){
              self.style.lineWidth = oldWidth;
            }
            //why is save/restore required here, it is called directly within subPaint?
            context.save();
            this.subPaint(context);
            context.restore();
            this.style.strokeStyle = oldStyle;
            this.style.lineWidth = oldWidth;
            if(this.mainItem instanceof DIAGRAMO.Path){
              this.mainItem.paintables.forEach(function(p){
                p.style = oldStyles.splice(0,1)[0];
              })
            }
          }
          //why is save/restore required here, it is called directly within subPaint?
          context.save();
          this.subPaint(context);
          context.restore();
        },
        getPoints: function(step){
            return this.mainItem.getPoints(step);
        },
        getMultiPoints: function(step){
          var points = this.getPoints(step);
          if(points.length >= 1 / step){
            return points;
          }
          else{
            var totalDistance = 0;
            var _points = [];
            for(var i = 0; i < points.length - 1; i++){
              totalDistance += DIAGRAMO.Util.distance(points[i], points[i + 1]);
            }

            for(var i = 0; i < points.length - 1; i++){
              var distance = DIAGRAMO.Util.distance(points[i], points[i + 1]);
              var create = Math.round((1 / step) / totalDistance * distance);
              for(var a = 0; a < create; a++){
                var p = DIAGRAMO.Util.point_on_segment(points[i], points[i + 1], distance / create * a);
                _points.push(p);
              }
            }

            //ensure we cover the end point;
            _points.push(points[points.length - 1]);
            return _points;
          }

        },
        transformEndPoint: function(item, point, secondPoint){
            //item.transform(matrix);
            //rotate correctly
            if(!secondPoint){
              return;
            }
            var dx = item.rotationCoords[0].x;
            var dy = item.rotationCoords[0].y;
            item.transform(DIAGRAMO.Matrix.translationMatrix(-dx, -dy));
            item.transform(DIAGRAMO.Matrix.rotationMatrix(-item.getAngle()));
            item.transform(DIAGRAMO.Matrix.rotationMatrix(DIAGRAMO.Util.getAngle(point, secondPoint)));
            item.transform(DIAGRAMO.Matrix.translationMatrix(point.x, point.y));
        },
        transform: function(matrix){
            this.mainItem.transform(matrix);
            //this.transformHandles(matrix);
            this.trigger("afterTransform", [matrix]);
        },
        transformHandles: function(matrix){
            this.handles.forEach(function(handle){
                handle.transform(matrix);
            });
        },
        isGlued: function(){
            return this.handles.some(function(handle){
                if(handle.glue != null){
                    return true;
                }
            })
        },
        repositionHandles: function(){
            throw "Must be overwritten"
        },
        updateEndPoints:function(matrix){
            throw "Must be overwritten"
        }
    });
}
