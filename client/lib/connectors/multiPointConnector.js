function MultiPointConnector(startPoint, endPoint){
  var line = null;
  if(startPoint instanceof DIAGRAMO.Path){
    line = startPoint;
  }
  else{
    var line = new DIAGRAMO.Polyline();
    this.oType = "DIAGRAMO.CONNECTORS.MultiPointConnector";
    startPoint.x += 1;
    line.addPoint(startPoint);
    line.addPoint(endPoint);
  }

    DIAGRAMO.Connector.call(this, line);
    this.buildFromPoints(line.points, this.getConnectionPoints());

    //same for straight
    this.afterTransform(this.updateEndPoints)

    //we really want to put this at the beginning
    this.afterTransformHandlers.splice(0,0,this.afterTransformHandler);

    this.isNotFinished = true;

}
DIAGRAMO.CONNECTORS.MultiPointConnector = MultiPointConnector;
DIAGRAMO.CONNECTORS.MultiPointConnector.img = "/assets/icon_connector_custom.png";

MultiPointConnector.load = function(jsonObject, connector){
    if(connector == undefined){
        connector = new DIAGRAMO.CONNECTORS.MultiPointConnector(new DIAGRAMO.Point(0,0), new DIAGRAMO.Point(0,0));
    }
    connector = DIAGRAMO.Connector.load(jsonObject, connector);
    connector.isNotFinished = false;
    return connector;
};

MultiPointConnector.init = function(){
    _.extend(MultiPointConnector.prototype, DIAGRAMO.Connector.prototype, {
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Connector.prototype.toJSONObject.call(this), {
                oType: this.oType,
            });
            return ret;
        },
        updateEndPoints: function(matrix){
            var points = this.getPoints();
            if(this.startItem != null){
                this.transformEndPoint(this.startItem, points[0], points[1]);
            }
            if(this.endItem != null){
                this.transformEndPoint(this.endItem, points[points.length - 1], points[points.length - 2]);
            }
        },
        clone: function(){
          var con = new DIAGRAMO.CONNECTORS.MultiPointConnector(this.mainItem.clone());
          con.isNotFinished = false;
          if(this.endItem){
            con.endItem = this.endItem.clone();
          }
          if(this.startItem){
            con.startItem = this.startItem.clone();
          }

          return con;
        },
        finish: function(){
            //specific to multi point connector
            var self = this;
            this.isNotFinished = false;
            var conPoints = [];
            this.handles.forEach(function(handle){
                if(handle.isConnectionPoint && conPoints.length == 0){
                    conPoints.push(handle);
                }
                else if(handle.isConnectionPoint){
                    self.handles = _.without(self.handles, handle);
                }
            });
            this.mainItem.points.splice(this.mainItem.points.length - 1, 1);

            this.buildFromPoints(this.mainItem.points, conPoints);
            this.trigger("afterTransform", DIAGRAMO.Matrix.Identity);
        },
        buildFromPoints: function(points, conPoints){
            //if(this.mainItem.points.length != points.length || conPoints.length == 0){
                var editor = DIAGRAMO.editorFromPaintable(this);
                //clear old handles
                var currentHandle = editor ? editor.handleManager.dragging : null;

                if(!currentHandle || currentHandle.isFigureHandle || currentHandle.isConnectionPoint){
                    var handles = [];
                    this.handles.forEach(function(handle){
                        if(handle.isConnectionPoint){
                            handles.push(handle);
                        }
                    });
                    this.handles = handles;
                    for(var i = 1; i < points.length - 1; i++){

                        var handle = new DIAGRAMO.ConnectorHandle(
                            "f",
                            points[i],
                            [{type: "f", point: points[i]}],
                            this
                        );
                        if(this.handleMouseDown){
                            handle.mouseDown(this.handleMouseDown);
                        }
                        this.handles.push(handle);
                        this.handles[this.handles.length - 1].visible = true;
                    }
                    this.mainItem.points = points;
                }
                if(conPoints == null || conPoints.length == 0){
                    var conPoint = new DIAGRAMO.ConnectorConnectionPoint(
                        "f",
                        points[0],
                        [{type: "f", point: points[0]}],
                        this
                    );
                    if(this.connectionPointMouseDownHandler){
                        conPoint.mouseDown(this.connectionPointMouseDownHandler);
                    }
                    this.handles.push(conPoint);
                }
                if(conPoints == null || conPoints.length < 2){
                    var conPoint = new DIAGRAMO.ConnectorConnectionPoint(
                        "f",
                        points[points.length - 1],
                        [{type: "f", point: points[points.length - 1]}],
                        this
                    );
                    if(this.connectionPointMouseDownHandler){
                        conPoint.mouseDown(this.connectionPointMouseDownHandler);
                    }
                    this.handles.push(conPoint);
                }
                else if(!currentHandle || currentHandle.isConnectionPoint){
                    conPoints[0].point = points[0];
                    conPoints[0].modifiablePoints = [{type: "f", point: points[0]}];

                    conPoints[1].point = points[points.length - 1];
                    conPoints[1].modifiablePoints = [{type: "f", point: points[points.length - 1]}];
                }
            //}
        },
        afterTransformHandler: function(matrix){
        },
        transform: function(matrix){
            var conPoints = this.getConnectionPoints();
            var self = this;
            var lastPoint = this.mainItem.points[0];
            DIAGRAMO.Connector.prototype.transform.call(this, matrix);
            for(var i = 1; i < this.mainItem.points.length; i++){
                var point = this.mainItem.points[i];
                if(i == this.mainItem.points.length - 1){
                    var tmp = point;
                    point = lastPoint;
                    lastPoint = tmp;
                }
                var handle = _.filter(self.handles, function(handle){
                    return _.findWhere(handle.modifiablePoints,{point:point}) && _.findWhere(handle.modifiablePoints,{point:lastPoint}) && handle.handleType != "f";
                }) ;
                if(handle.length > 0){
                    handle = handle[0];
                    var mPoint = _.findWhere(handle.modifiablePoints,{point:point});
                    if(mPoint.type == "h"){
                        point.x = lastPoint.x;
                    }
                    else if(mPoint.type == "v"){
                        point.y = lastPoint.y;
                    }
                    lastPoint = point;
                }
            }
            this.repositionHandles();
        },
        repositionHandles: function(){
        }
    });

}
