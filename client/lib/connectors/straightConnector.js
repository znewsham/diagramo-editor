function StraightConnector(startPoint, endPoint){
  var pl = new DIAGRAMO.Polyline();
  pl.addPoint(startPoint);
  pl.addPoint(endPoint);
    DIAGRAMO.Connector.call(this, pl);
    this.oType = "DIAGRAMO.CONNECTORS.StraightConnector";
    this.buildFromPoints([startPoint, endPoint], []);
    //same for right angle
    this.afterTransform(this.updateEndPoints)
    this.afterTransform(this.updateEndPoints);

}
DIAGRAMO.CONNECTORS.StraightConnector = StraightConnector;
DIAGRAMO.CONNECTORS.StraightConnector.img = "/assets/icon_connector_straight.png";

StraightConnector.load = function(jsonObject, connector){
    if(connector == undefined){
        connector = new DIAGRAMO.CONNECTORS.StraightConnector(new DIAGRAMO.Point(0,0), new DIAGRAMO.Point(0,0));
    }
    connector = DIAGRAMO.Connector.load(jsonObject, connector);

    return connector;
};

DIAGRAMO.CONNECTORS.StraightConnector.init = function(){
    _.extend(StraightConnector.prototype, DIAGRAMO.Connector.prototype, {
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Connector.prototype.toJSONObject.call(this), {
                oType: this.oType,
            });

            return ret;
        },
        updateEndPoints:function(matrix){
            var points = this.getPoints();
            if(this.startItem != null){
                this.transformEndPoint(this.startItem, points[0], points[1]);
            }
            if(this.endItem != null){
                this.transformEndPoint(this.endItem, points[points.length - 1], points[points.length - 2]);
            }
        },
        buildFromPoints: function(points, conPoints){
            if(conPoints.length == 0){
                this.handles.push(new DIAGRAMO.ConnectorConnectionPoint("f", points[0], [{type: "f", point: points[0]}], this));
                this.handles.push(new DIAGRAMO.ConnectorConnectionPoint("f", points[1], [{type: "f", point: points[1]}], this));
            }
            else{
                conPoints[0].point = points[0];
                conPoints[1].point = points[1];
            }
        },
        repositionHandles: function(){
            this.handles.forEach(function(handle){
                handle.point.x = handle.modifiablePoints[0].point.x;
                handle.point.y = handle.modifiablePoints[0].point.y;
            });
        }
    });
}
