function RightAngleConnector(startPoint, endPoint){
    var line = new DIAGRAMO.Polyline();
    startPoint.x += 1;
    var midPoint = new DIAGRAMO.Point(startPoint.x, endPoint.y);
    line.addPoint(startPoint);
    line.addPoint(midPoint);
    line.addPoint(endPoint);

    DIAGRAMO.Connector.call(this, line);
    this.oType = "DIAGRAMO.CONNECTORS.RightAngleConnector";
    this.buildFromPoints([startPoint, midPoint, endPoint], this.getConnectionPoints());

    //same for straight
    this.afterTransform(this.updateEndPoints)
    this.isSmart = true;
    //we really want to put this at the beginning
    this.afterTransformHandlers.splice(0,0,this.afterTransformHandler);

}
DIAGRAMO.CONNECTORS.RightAngleConnector = RightAngleConnector;
DIAGRAMO.CONNECTORS.RightAngleConnector.img = "/assets/icon_connector_jagged.png";

RightAngleConnector.load = function(jsonObject, connector){
    if(connector == undefined){
        connector = new DIAGRAMO.CONNECTORS.RightAngleConnector(new DIAGRAMO.Point(0,0), new DIAGRAMO.Point(0,0));
    }
    connector = DIAGRAMO.Connector.load(jsonObject, connector);

    return connector;
};

RightAngleConnector.init = function(){
    _.extend(RightAngleConnector.prototype, DIAGRAMO.Connector.prototype, {
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Connector.prototype.toJSONObject.call(this), {
                oType: this.oType,
            });
            return ret;
        },
        updateEndPoints: function(matrix){
            var points = this.getPoints();
            if(this.startItem != null){
                this.transformEndPoint(this.startItem, points[0], points[1]);
            }
            if(this.endItem != null){
                this.transformEndPoint(this.endItem, points[points.length - 1], points[points.length - 2]);
            }
        },
        buildFromPoints: function(points, conPoints){
            //if(this.mainItem.points.length != points.length || conPoints.length == 0){
                var editor = DIAGRAMO.editorFromPaintable(this);
                //clear old handles
                var currentHandle = editor ? editor.handleManager.dragging : null;

                if(!currentHandle || currentHandle.isFigureHandle || currentHandle.isConnectionPoint){
                    var handles = [];
                    this.handles.forEach(function(handle){
                        if(handle.isConnectionPoint){
                            handles.push(handle);
                        }
                    });
                    this.handles = handles;
                    for(var i = 0; i < points.length - 1; i++){

                        var handle = new DIAGRAMO.ConnectorHandle(
                            points[i].x == points[i + 1].x ? "h" : "v",
                            new DIAGRAMO.Point(
                                points[i].x != points[i + 1].x ? (points[i].x + (points[i + 1].x - points[i].x) / 2) : points[i].x,
                                points[i].x != points[i + 1].x ? points[i].y : (points[i].y + (points[i + 1].y - points[i].y) / 2)),
                            [{type: points[i].x == points[i + 1].x ? "h" : "v", point: points[i]}, {type: points[i].x == points[i + 1].x ? "h" : "v", point: points[i + 1]}],
                            this
                        );
                        if(this.handleMouseDown){
                            handle.mouseDown(this.handleMouseDown);
                        }
                        this.handles.push(handle);
                        this.handles[this.handles.length - 1].visible = true;
                        this.handles[this.handles.length - 1].zIndex = this.zIndex - 0.1;
                    }
                    this.mainItem.points = points;
                }
                if(conPoints == null || conPoints.length == 0){
                    var conPoint1 = new DIAGRAMO.ConnectorConnectionPoint(
                        "f",
                        points[0],
                        [{type: "f", point: points[0]}, {type: points[0].x == points[1].x ? "h" : "v", point: points[1]}],
                        this
                    );

                    if(this.connectionPointMouseDownHandler){
                        conPoint1.mouseDown(this.connectionPointMouseDownHandler);
                    }
                    this.handles.push(conPoint1);

                    var conPoint2 = new DIAGRAMO.ConnectorConnectionPoint(
                        "f",
                        points[points.length - 1],
                        [{type: "f", point: points[points.length - 1]}, {type: points[points.length - 1].x == points[points.length - 2].x ? "h" : "v", point: points[points.length - 2]}],
                        this
                    );

                    if(this.connectionPointMouseDownHandler){
                        conPoint2.mouseDown(this.connectionPointMouseDownHandler);
                    }
                    this.handles.push(conPoint2);
                }
                else if(!currentHandle || currentHandle.isConnectionPoint){
                    conPoints[0].point = points[0];
                    conPoints[0].modifiablePoints = [{type: "f", point: points[0]}, {type: points[0].x == points[1].x ? "h" : "v", point: points[1]}];

                    conPoints[1].point = points[points.length - 1];
                    conPoints[1].modifiablePoints = [{type: "f", point: points[points.length - 1]}, {type: points[points.length - 1].x == points[points.length - 2].x ? "h" : "v", point: points[points.length - 2]}];
                }
            //}
        },
        afterTransformHandler: function(matrix){
            //forces update of the end points
            var solution = this.onConnectDelegate.jaggedSolutions(this)[0];
            if(solution[2].length > 2 && this.activeHandle != null){
                this.buildFromPoints(solution[2], this.getConnectionPoints());
            }
            this.repositionHandles();
        },
        transform: function(matrix){
            var conPoints = this.getConnectionPoints();
            var self = this;
            var lastPoint = this.mainItem.points[0];
            DIAGRAMO.Connector.prototype.transform.call(this, matrix);
            for(var i = 1; i < this.mainItem.points.length; i++){
                var point = this.mainItem.points[i];
                if(i == this.mainItem.points.length - 1){
                    var tmp = point;
                    point = lastPoint;
                    lastPoint = tmp;
                }
                var handle = _.filter(self.handles, function(handle){
                    return _.findWhere(handle.modifiablePoints,{point:point}) && _.findWhere(handle.modifiablePoints,{point:lastPoint}) && handle.handleType != "f";
                }) ;
                if(handle.length > 0){
                    handle = handle[0];
                    var mPoint = _.findWhere(handle.modifiablePoints,{point:point});
                    if(mPoint.type == "h"){
                        point.x = lastPoint.x;
                    }
                    else if(mPoint.type == "v"){
                        point.y = lastPoint.y;
                    }
                    lastPoint = point;
                }
            }
            this.repositionHandles();
        },
        repositionHandles: function(){
            this.handles.forEach(function(handle){
                if(handle.isConnectionPoint){
                   handle.point.x = handle.modifiablePoints[0].point.x;
                   handle.point.y = handle.modifiablePoints[0].point.y;
                }
                else{
                    handle.point.x = Math.min(handle.modifiablePoints[0].point.x, handle.modifiablePoints[1].point.x) + (Math.max(handle.modifiablePoints[0].point.x, handle.modifiablePoints[1].point.x) - Math.min(handle.modifiablePoints[0].point.x, handle.modifiablePoints[1].point.x)) / 2;
                    handle.point.y = Math.min(handle.modifiablePoints[0].point.y, handle.modifiablePoints[1].point.y) + (Math.max(handle.modifiablePoints[0].point.y, handle.modifiablePoints[1].point.y) - Math.min(handle.modifiablePoints[0].point.y, handle.modifiablePoints[1].point.y)) / 2;
                }
            });
        }
    });

}
