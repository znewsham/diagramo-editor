function OrganicConnector(startPoint, endPoint, midPoint){
      this.isSmart = true;
    if(startPoint instanceof DIAGRAMO.NURBS){
      DIAGRAMO.Connector.call(this,startPoint);
      //slightly different from straight and right angle
      this.afterTransform(this.updateEndPoints)

      //we really want to put this at the beginning
      this.afterTransformHandlers.splice(0,0,this.afterTransformHandler);

      this.buildFromPoints(startPoint.points,[]);
    }
    else{
      if(_.isUndefined(midPoint)){
          midPoint = new DIAGRAMO.Point(startPoint.x, endPoint.y);
      }
      DIAGRAMO.Connector.call(this, new DIAGRAMO.NURBS([startPoint, midPoint, endPoint]));
      this.handles.push(new DIAGRAMO.ConnectorHandle("f", midPoint, [{type: "f", point: midPoint}], this));



      //slightly different from straight and right angle
      this.afterTransform(this.updateEndPoints)

      //we really want to put this at the beginning
      this.afterTransformHandlers.splice(0,0,this.afterTransformHandler);

      this.buildFromPoints([startPoint, endPoint, midPoint],[]);
    }
    this.oType = "DIAGRAMO.CONNECTORS.OrganicConnector";
}
DIAGRAMO.CONNECTORS.OrganicConnector = OrganicConnector;
DIAGRAMO.CONNECTORS.OrganicConnector.img = "/assets/icon_connector_organic.png";

OrganicConnector.load = function(jsonObject, connector){
    if(connector == undefined){
        connector = new DIAGRAMO.CONNECTORS.OrganicConnector(new DIAGRAMO.Point(0,0), new DIAGRAMO.Point(0,0), new DIAGRAMO.Point(0,0));
    }
    connector = DIAGRAMO.Connector.load(jsonObject, connector);

    return connector;
};

OrganicConnector.init = function(){
    _.extend(OrganicConnector.prototype, DIAGRAMO.Connector.prototype, {
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Connector.prototype.toJSONObject.call(this), {
                oType: this.oType,
            });

            return ret;
        },
        updateEndPoints:function(matrix){
            var points = this.getPoints();
            if(this.startItem != null){
                this.transformEndPoint(this.startItem, points[0], points[20]);
            }
            if(this.endItem != null){
                this.transformEndPoint(this.endItem, points[points.length - 1], points[points.length - 2]);
            }
        },
        clone: function(){
            var mainItem = this.mainItem.clone();

            var con =  new DIAGRAMO.CONNECTORS.OrganicConnector(mainItem);
            if(this.endItem){
              con.endItem = this.endItem.clone();
            }
            if(this.startItem){
              con.startItem = this.startItem.clone();
            }
            return con;
        },
        buildFromPoints: function(points,conPoints){
            var editor = DIAGRAMO.editorFromPaintable(this);
            //clear old handles
            var self = this;
            var currentHandle = editor ? editor.handleManager.dragging : null;
            if(!currentHandle || currentHandle.isFigureHandle || currentHandle.isConnectionPoint){
                this.mainItem.points = points;
                var handles = [];
                this.handles.forEach(function(handle){
                    if(handle.isConnectionPoint){
                        handles.push(handle);
                    }
                });
                this.handles = handles;

                for(var i = 1; i < points.length - 1; i ++){
                    var handle = new DIAGRAMO.ConnectorHandle("f", points[i], [{type: "f", point: points[i]}], this);
                    if(this.handleMouseDown){
                        handle.mouseDown(this.handleMouseDown);
                    }
                    this.handles.push(handle);
                }
            }
            if(conPoints.length == 0){
                var startConPoint = new DIAGRAMO.ConnectorConnectionPoint("f", points[0], [{type: "f", point: points[0]}], this);
                conPoints.push(startConPoint);
                this.handles.push(startConPoint);
            }
            if(conPoints.length < 2){
                var endConPoint = new DIAGRAMO.ConnectorConnectionPoint("f", points[points.length - 1], [{type: "f", point: points[points.length - 1]}], this);
                conPoints.push(endConPoint);
                this.handles.push(endConPoint);
            }
            this.mainItem.fragments = this.mainItem.nurbsPoints(DIAGRAMO.Point.cloneArray(this.mainItem.points));
            conPoints[0].point = this.mainItem.points[0];
            conPoints[0].modifiablePoints = [{type: "f", point: this.mainItem.points[0]}];

            conPoints[1].point = this.mainItem.points[this.mainItem.points.length - 1];
            conPoints[1].modifiablePoints = [{type: "f", point: this.mainItem.points[this.mainItem.points.length - 1]}];
        },
        afterTransformHandler: function(matrix){
            //forces update of the end points
            if(this.activeHandle != null){
                var solution = this.onConnectDelegate.smoothSolutions(this)[0];
                this.buildFromPoints(solution[2], this.getConnectionPoints());
            }
        },
        getPoints: function(){
            return this.mainItem.getPoints();
        }
    });

}
