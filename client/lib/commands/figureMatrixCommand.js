"use strict";

/*
 * This is triggered when a figure was scaled/expanded
 * @this {FigureMatrixCommand}
 * @constructor
 * @author Zack
 */
function FigureMatrixCommand(figure, matrix, reverseMatrix){

    /**Any sequence of many mergeable actions can be packed by the history*/
    this.mergeable = true;
    try{
    this.figureId = figure.id;
  }
  catch(e){
    console.log(e);
  }
    this.figure = figure;

    this.matrix = matrix;
    this.reverseMatrix = reverseMatrix;
}
DIAGRAMO.COMMANDS.FigureMatrixCommand = FigureMatrixCommand;

FigureMatrixCommand.prototype = {

    /**This method got called every time the Command must execute*/
    execute : function(){
        this.figure.transform(this.matrix);
    },


    /**This method should be called every time the Command should be undone*/
    undo : function(){
        this.figure.transform(this.reverseMatrix);
    }
};
