"use strict";
function SendToBackCommand(editor, figure){
    this.figure = figure;
    this.editor = editor;
}
DIAGRAMO.COMMANDS.SendToBackCommand = SendToBackCommand;
_.extend(SendToBackCommand.prototype, {
    execute: function(){
      var sorted = _.sortBy(this.editor.paintables, function(p){return p.zIndex});
      var lastZIndex = false;
      for(var i = 0; i < sorted.length; i++){
        if(lastZIndex && sorted[i] != this.editor.background && !sorted[i].isHandle && sorted[i] != this.editor.selectionArea){
          var tmp = sorted[i].zIndex;
          sorted[i].zIndex = lastZIndex;
          lastZIndex = tmp;
        }
        if(sorted[i] == this.figure){
          lastZIndex = this.figure.zIndex;
        }
      }
      if(lastZIndex !== false){
        this.figure.zIndex = lastZIndex;
        this.editor.repaint();
      }
    }
});
