"use strict";

/* 
 * This is triggered when a figure was scaled/expanded
 * @this {FigureScaleCommand} 
 * @constructor
 * @author Alex Gheorghiu <alex@scriptoid.com>
 */
function FigureScaleCommand(figure, matrix, reverseMatrix){
    this.oType = 'FigureScaleCommand';
    DIAGRAMO.COMMANDS.FigureMatrixCommand.call(this, figure, matrix, reverseMatrix);
        
}
DIAGRAMO.COMMANDS.FigureScaleCommand = FigureScaleCommand;
_.extend(FigureScaleCommand.prototype, DIAGRAMO.COMMANDS.FigureMatrixCommand.prototype, {});


