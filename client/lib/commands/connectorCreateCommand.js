"use strict";

/*
 * This is triggered when a figure was rotated
 * @this {FigureRotateCommand}
 * @constructor
 * @author Alex Gheorghiu <alex@scriptoid.com>
 */
function ConnectorCreateCommand(editor, connector, startPoint, endPoint){
    this.oType = 'ConnectorCreateCommand';
    this.editor = editor;
    this.connector = connector;
    this.startPoint = startPoint;
    this.endPoint = endPoint;

}
DIAGRAMO.COMMANDS.ConnectorCreateCommand = ConnectorCreateCommand;
_.extend(ConnectorCreateCommand.prototype, {
    execute: function(){
        var connector = this.connector;
        var points = connector.mainItem.points || connector.mainItem.getPoints(null);
        var startPoint = points[0];
        var endPoint = points[points.length - 1];
        startPoint.transform(DIAGRAMO.Matrix.translationMatrix(this.startPoint.x - startPoint.x + 1, this.startPoint.y - startPoint.y + 1));
        endPoint.transform(DIAGRAMO.Matrix.translationMatrix(this.endPoint.x - endPoint.x, this.endPoint.y - endPoint.y));

        var conPoints = connector.getConnectionPoints();
        connector.activeHandle = conPoints[conPoints.length - 1];
        connector.trigger("afterTransform", DIAGRAMO.Matrix.IDENTITY);
        this.editor.addConnector(this.connector);
        this.editor.selectConnector(this.connector);
        this.editor.handleManager.dragging = this.connector.getConnectionPoints()[this.connector.getConnectionPoints().length - 1];

        connector.zIndex = this.editor.paintables.length;
        var sub = new DIAGRAMO.COMMANDS.BringToFrontCommand(this.editor, connector);
        sub.execute();
        if(DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler){
          this.editor.canvas.onmousedown = DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler;
          DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler = null;
        }
        this.editor.repaint();
    }
});
