PropertyChangeCommand = function(object, accessors, newValue, oldValue){
    this.object = object;
    this.accessors = accessors;
    this.oldValue = oldValue;
    this.newValue = newValue;
}


DIAGRAMO.COMMANDS.PropertyChangeCommand = PropertyChangeCommand;
_.extend(PropertyChangeCommand.prototype, {
    setValue: function(value){
        var obj = this.object;
        var accessors = this.accessors.split(".");
        for(var i = 0; i < accessors.length - 1; i++){
            var getterName = "get" + accessors[i].charAt(0).toUpperCase() + accessors[i].slice(1);
            if(obj[getterName]){
                obj = obj[getterName]();
            }
            else{
                obj = obj[accessors[i]];
            }
        }
        var setterName = "set" + accessors[accessors.length - 1].charAt(0).toUpperCase() + accessors[accessors.length - 1].slice(1);
        if(obj[setterName]){
            obj[setterName](value);
        }
        else{
            obj[accessors[accessors.length - 1]] = value;
        }
    },
    execute: function(){
        this.setValue(this.newValue);
    }
});
