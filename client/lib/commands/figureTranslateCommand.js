/* 
 * This is triggered when a figure was translated
 * @this {FigureTranslateCommand} 
 * @constructor
 * @param {Integer} figureId - the id of the figure translated
 * @param {Array} matrix - the transformation matrix of translation
 * @author Alex Gheorghiu <alex@scriptoid.com>
 */
function FigureTranslateCommand(figure, matrix){
    this.oType = 'FigureTranslateCommand';
    DIAGRAMO.COMMANDS.FigureMatrixCommand.call(this, figure, matrix,  [
    [1, 0, -matrix[0][2]],
    [0, 1, -matrix[1][2]],
    [0, 0, 1]
    ]);
}
DIAGRAMO.COMMANDS.FigureTranslateCommand = FigureTranslateCommand;
_.extend(FigureTranslateCommand.prototype, DIAGRAMO.COMMANDS.FigureMatrixCommand.prototype, {});