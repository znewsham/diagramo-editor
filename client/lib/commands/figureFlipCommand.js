"use strict";

/*
 * This is triggered when a figure was flipped
 * @this {FigureFlipCommand}
 * @constructor
 * @author Zack
 */
function FigureFlipCommand(figure, matrix, reverseMatrix){
    this.oType = 'DIAGRAMO.COMMANDS.FigureFlipCommand';
    DIAGRAMO.COMMANDS.FigureMatrixCommand.call(this, figure, matrix, reverseMatrix);
    var bounds = this.figure.getBounds();
    var width = bounds[2]-bounds[0];
    var height = bounds[3]-bounds[1];
    this.dx = bounds[0] + width / 2;
    this.dy = bounds[1] + height / 2;
}

DIAGRAMO.COMMANDS.FigureFlipCommand = FigureFlipCommand;
_.extend(FigureFlipCommand.prototype, DIAGRAMO.COMMANDS.FigureMatrixCommand.prototype, {
    execute: function(ignoreConnectors){
        var figure = this.figure;
        var self = this;
        figure.transform(DIAGRAMO.Matrix.translationMatrix(0-this.dx, 0-this.dy), true);
        figure.transform(this.matrix, true);
        figure.transform(DIAGRAMO.Matrix.translationMatrix(this.dx,this.dy), true);
        figure.paintables.forEach(function(p){
            if(p.isFigure || p instanceof DIAGRAMO.Text){//if we're transforming a group
                var fBounds = p.getBounds();
                var tDx = fBounds[0] + (fBounds[2] - fBounds[0]) / 2;
                var tDy = fBounds[1] + (fBounds[3] - fBounds[1]) / 2;
                p.transform(DIAGRAMO.Matrix.translationMatrix(0-tDx, 0-tDy), false, 1);
                p.transform(self.matrix, false, 1);
                p.transform(DIAGRAMO.Matrix.translationMatrix(tDx,tDy), false, 1);
            }
        });
    }
});
