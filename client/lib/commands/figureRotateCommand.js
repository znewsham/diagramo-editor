"use strict";

/* 
 * This is triggered when a figure was rotated
 * @this {FigureRotateCommand} 
 * @constructor
 * @author Alex Gheorghiu <alex@scriptoid.com>
 */
function FigureRotateCommand(figure, matrix, reverseMatrix){
    this.oType = 'FigureRotateCommand';
    DIAGRAMO.COMMANDS.FigureMatrixCommand.call(this, figure, matrix, reverseMatrix);
        
}
DIAGRAMO.COMMANDS.FigureRotateCommand = FigureRotateCommand;
_.extend(FigureRotateCommand.prototype, DIAGRAMO.COMMANDS.FigureMatrixCommand.prototype, {});