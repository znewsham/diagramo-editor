"use strict";

/*
 * Align all figures in a group vertically
 * @this {AlignVCommand}
 * @constructor
 * @author Zack
 */
function AlignVCommand(figure){
    this.oType = 'DIAGRAMO.COMMANDS.AlignVCommand';
    this.figure = figure;
}

DIAGRAMO.COMMANDS.AlignVCommand = AlignVCommand;
_.extend(AlignVCommand.prototype, {
    execute: function(){
      var figure = this.figure;
      var self = this;
      var first = true;
      var ypos = 0;
      figure.paintables.forEach(function(p){
        if(first){
          ypos = p.getBounds()[1];
          first = false;
        }
        else{
          var bounds = p.getBounds();
          p.transform(DIAGRAMO.Matrix.translationMatrix(0, ypos - bounds[1]));
        }
      });
    }
});
