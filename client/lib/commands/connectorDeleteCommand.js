"use strict";
function ConnectorDeleteCommand(editor, connector){
    this.connector = connector;
    this.editor = editor;
}
DIAGRAMO.COMMANDS.ConnectorDeleteCommand = ConnectorDeleteCommand;

_.extend(ConnectorDeleteCommand.prototype, {
    execute: function(){
        var self = this;
        this.editor.paintables = _.without(this.editor.paintables, this.connector);
        this.editor.connectors = _.without(this.editor.connectors, this.connector);
        this.connector.handles.forEach(function(conPoint){
            if(conPoint.glue){
                conPoint.glue.figureConPoint.glues = [];
                self.editor.glues = _.without(self.editor.glues, conPoint.glue);
            };
            self.editor.connectionPoints = _.without(self.editor.connectionPoints, conPoint);
        });
    },
    undo: function(){

    }
});
