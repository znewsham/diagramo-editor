"use strict";
function FigureDeleteCommand(editor, figure){
    this.figure = figure;
    this.editor = editor;
}
DIAGRAMO.COMMANDS.FigureDeleteCommand = FigureDeleteCommand;

_.extend(FigureDeleteCommand.prototype, {
    execute: function(){
        var self = this;
        if(this.figure.isGroup){
          this.figure.paintables.forEach(function(p){
            if(p.isConnector){
              var cmd = new DIAGRAMO.COMMANDS.ConnectorDeleteCommand(self.editor, p);
              cmd.execute();
            }
          })
        }
        this.editor.paintables = _.without(this.editor.paintables, this.figure);
        this.figure.connectionPoints.forEach(function(conPoint){
            conPoint.glues.forEach(function(glue){
                glue.connectorConPoint.glue = null;
                self.editor.glues = _.without(self.editor.glues, glue);
            });
            self.editor.connectionPoints = _.without(self.editor.connectionPoints, conPoint);
        });
    },
    undo: function(){

    }
});
