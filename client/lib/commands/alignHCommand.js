"use strict";

/*
 * Align all figures in a group horizontally
 * @this {AlignHCommand}
 * @constructor
 * @author Zack
 */
function AlignHCommand(figure){
    this.oType = 'DIAGRAMO.COMMANDS.AlignHCommand';
    this.figure = figure;
}

DIAGRAMO.COMMANDS.AlignHCommand = AlignHCommand;
_.extend(AlignHCommand.prototype, {
    execute: function(ignoreConnectors){
      var figure = this.figure;
      var self = this;
      var first = true;
      var xpos = 0;
      figure.paintables.forEach(function(p){
        if(first){
          xpos = p.getBounds()[0];
          first = false;
        }
        else{
          var bounds = p.getBounds();
          p.transform(DIAGRAMO.Matrix.translationMatrix(xpos - bounds[0], 0));
        }
      });
    }
});
