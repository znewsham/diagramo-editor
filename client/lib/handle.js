"use strict";

/**
 *  Copyright 2010 Scriptoid s.r.l
 **/

/**
 *Handles are created on-the-fly for a figure. They are completelly managed by the HandleManager
 *Each handle is responsable for an action. The {Handle} does not need to keep a reference to the parent shape
 *as the HandleManager will do that.
 *@constructor
 *@this {Handle}
 *@param {String} handleType - the handleType of handle
 **/
function Handle(handleType, parent){
    this.id = DIAGRAMO.generateId();
    DIAGRAMO.Primitive.call(this);
    /**Type of Handle*/
    this.handleType = handleType;
    this.isHandle = true;

    //may be the handle manager, or a connector.
    this.parent = parent;

    /*These are stupidly initialized to 0 but they should not be present at all...
     *anyway they got set to the proper values in HandleManager::handleGetAll() function*/
    this.point = new DIAGRAMO.Point(0, 0);

    /**Used by Connector handles, to not display redundant handles (i.e. when they are on the same line)*/
    this.visible = false;
    this.beforeActionHandlers = [];

    this.beforeAction(this.beforeActionHandler);
}
DIAGRAMO.Handle = Handle;

/**It's a (static) vector of handle handleTypes
 * Note: R - stand for rotation
 * Note: f - stands for free (any direction)
 * Note: More handles might be added in the future : like handles to increase the number of edges for a hexagone
 * Those handles will be specific for a figure
 **/
Handle.handleTypes = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw', 'r', 'f' ]; //DO NOT CHANGE THE ORDER OF THESE VALUES

/**It's a (static) vector of connector handleTypes*/
Handle.connectorTypes = ['ns', 'ew'];

/**Creates a {Handle} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Handle} a newly constructed Handle
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Handle.load = function(o, handle){
    if(handle == undefined){
         handle = new DIAGRAMO.Handle(o.handleType);
    }
    handle.visible = o.visible;
    handle.id = o.id;
    return handle;
}

/**Creates an array of handles from an array of {JSONObject}s
 *@param {Array} v - the array of JSONObjects
 *@return an {Array} of {Handle}s
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Handle.loadArray = function(v){
    var newHandles = [];
    for(var i=0; i< v.length; i++){
        newHandles.push(Handle.load(v[i]));
    }
    return newHandles;
}

/**Default handle radius*/
Handle.RADIUS = 3;
Handle.init = function(){
_.extend(Handle.prototype,DIAGRAMO.Primitive.prototype,{
    toJSONObject: function(){
        var ret = _.extend({}, DIAGRAMO.Primitive.prototype.toJSONObject.call(this), {
            oType: "DIAGRAMO.Handle",
            handleType: this.handleType,
            point: this.point.toJSONObject(),
            visible: this.visible
        });

        return ret;
    },

    constructor : Handle,
    beforeActionHandler: function(){},
    beforeAction: function(eventHandler){
        if(_.isUndefined(this.beforeActionHandlers) || _.isNull(this.beforeActionHandlers)){
            this.beforeActionHandlers = [];
        }
        this.beforeActionHandlers.push(eventHandler);
    },
    afterAction: function(eventHandler){
        if(_.isUndefined(this.afterActionHandlers) || _.isNull(this.afterActionHandlers)){
            this.afterActionHandlers = [];
        }
        this.afterActionHandlers.push(eventHandler);
    },
    /**Compares to another Handle
     *@param {Handle} group -  - the other glue
     *@return {Boolean} - true if equals, false otherwise
     **/
    equals : function(anotherHandle){
        if(!anotherHandle instanceof Handle){
            return false;
        }

        return this.handleType == anotherHandle.handleType
        && this.point.x == anotherHandle.point.x
        && this.point.y == anotherHandle.point.y
        && this.visible == anotherHandle.visible;
    },


    /**This is the method you have to call to paint a handler
     * All handles will be circles...so we avoid to much of the computing for rectangle handles
     * They will have a filling color (green) and a stoke (black)
     * @param {Context} context - the 2D context
     **/
    paint : function(context){
        context.save();

        //fill the handler
        context.beginPath();
        context.arc(this.point.x, this.point.y, Handle.RADIUS, 0, Math.PI*2, false);
        context.fillStyle = "rgb(0,255,0)";
        context.closePath();
        context.fill();

        //stroke the handler
        context.beginPath();
        context.arc(this.point.x, this.point.y, Handle.RADIUS, 0, Math.PI*2, false);
        context.strokeStyle = "rgb(0,0,0)";
        context.lineWidth = 1;
        context.closePath();
        context.stroke();


        /*if(this.handleType == 'r'){
            var line = new DIAGRAMO.Line(new DIAGRAMO.Point(this.point.x,this.point.y), new DIAGRAMO.Point(this.handleManager.handles["n"].x,this.handleManager.handles["n"].y));
            line.style.dashLength = 3;
            line.style.strokeStyle = "grey";
            line.style.lineWidth = 1;
            line.paint(context);
        }*/

        context.restore();
    },


    /**See if the handle contains a point
     *@param {Number} x - the x coordinate of the point
     *@param {Number} y - the y coordinate of the point
     **/
    contains:function(x,y){
        return this.point.near(x,y, Handle.RADIUS);
    },


    /**See if the handle contains a point
     *@param {Number} x - the x coordinate of the point
     *@param {Number} y - the y coordinate of the point
     **/
    near:function(x,y, radius){
        return this.point.near(x,y, radius);
    },


    /**
     *Get a handle bounds
     **/
    getBounds : function(){
        return [this.point.x - Handle.RADIUS, this.point.y - Handle.RADIUS, this.point.x + Handle.RADIUS,this.point.y + Handle.RADIUS];
    },


    /**
     *Transform the Handle through a matrix
     *@param {Matrix} matrix - the matrix that will perform the transformation
     **/
    transform: function(matrix){
        this.point.transform(matrix);
    },


    /**Get the specific cursor for this handle. Cursor is ONLY a visual clue for
     *  the user to know how to move his mouse.
     *
     *Behaviour:
     * If North handle is in the North we have 'Up/Down arrow" cursor
     * If North handle is in the West (so it has "Left/Right arrow") (or anything different that North)
     *  we have 'Left/Right arrow' cursor but the figure will expand as follows:
     *  - rotate back to initial position
     *  - expand North
     *  - rotate back to current position
     *  - repaint
     * @see <a href="http://www.w3schools.com/css/pr_class_cursor.asp">http://www.w3schools.com/css/pr_class_cursor.asp</a> for cusor values
     * @author Zack Newsham <zack_newsham@yahoo.co.uk>
     * @author Alex Gheorghiu <alex@scriptoid.com>
     **/

    getCursor:function(){
        if(this.visible == false && (!this.parent || !this.parent.selected)){
            return "";
        }
        if(this.handleType == 'r'){
            return 'move';
        }

        var figureBounds = this.parent.getBounds(); //get figure's bounds
        var figureCenter = new DIAGRAMO.Point(figureBounds[0] + ((figureBounds[2]-figureBounds[0])/2),
            (figureBounds[1] + ((figureBounds[3] - figureBounds[1])/2)) ); //get figure's center

        //find north
        var closestToNorthIndex = -1; //keeps the index of closest handle to North
        var minAngleToNorth = 2 * Math.PI; //keeps the smallest (angular) distante to North
        var myIndex = -1;

        var self = this;
        var handleCenter = this.point;
        var angle = DIAGRAMO.Util.getAngle(figureCenter,handleCenter) * (180 / Math.PI); //get the angle between those 2 points 0=n

        var keys = ["n","ne","e","se","s","sw","w","nw"];
        angle = Math.round(angle / 45) * 45;
        var index = angle / 45;
        return keys[index] + "-resize";
    }
});
}
