DIAGRAMO.PROPERTIES.MultiProperty = function(figure, label, properties, style){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, null, label);
    this.properties = properties;
    this.style = style;
};


_.extend(DIAGRAMO.PROPERTIES.MultiProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    generateCode: function(editor, DOMObject){
        var self = this;

        var div = document.createElement("div");
        div.className = "line";
        div.style.width = "100%";
        if(this.label){
            var labelDiv = document.createElement("div");
            labelDiv.className = "label";
            labelDiv.textContent = this.label;

            div.appendChild(labelDiv);
        }

        //must be done before, to allow colorPicker
        DOMObject.appendChild(div);
        var outerDiv = document.createElement("div");
        outerDiv.style.float = "right";
        outerDiv.style.width = "100px";
        outerDiv.className = "input";
        div.appendChild(outerDiv);
        if(this.style){
            _.keys(this.style).forEach(function(s){
                outerDiv.style[s] = self.style[s];
            })
        }
        this.properties.forEach(function(property){
            var innerDiv = document.createElement("div");
            innerDiv.style.height = "auto";
            outerDiv.appendChild(innerDiv);
            property.generateCode(editor, innerDiv);
            $(".label", $(innerDiv)).remove();
            var html = $(".line", $(innerDiv)).html();
            innerDiv = $(innerDiv);
            $(".line", innerDiv).addClass("noclass");
            $(".line", innerDiv).removeClass("line");
        });

        return DOMObject;

    }
});
