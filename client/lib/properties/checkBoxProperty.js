DIAGRAMO.PROPERTIES.CheckboxProperty = function(figure, accessors, label, style){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, accessors, label);
    this.style = style;
};


_.extend(DIAGRAMO.PROPERTIES.CheckboxProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    generateCode: function(editor, DOMObject){
        var self = this;
        value = this.getValue();
        var div = document.createElement("div");
        div.className = "line";
        div.style.width = "100%";
        var labelDiv = document.createElement("div");
        labelDiv.className = "label";
        labelDiv.textContent = this.label;

        div.appendChild(labelDiv);

        var selectDiv = document.createElement("div");
        selectDiv.style.cssText ="float: right;";

        selectDiv.id = this.property; // for DOM manipulation
        div.appendChild(selectDiv);
        if(this.style){
            _.keys(this.style).forEach(function(s){
                selectDiv.style[s] = self.style[s];
            })
        }
        var selProperty = this.property; //save it in a separate variable as if refered by (this) it will refert to the 'select' DOM Object

        var cb = document.createElement("input");
        cb.type = "checkbox";
        cb.checked = this.getValue();
        cb.onclick = function(){
            var command = new DIAGRAMO.COMMANDS.PropertyChangeCommand(self.figure, self.accessors, cb.checked, value);
            command.execute();
            editor.repaint();
        }
        selectDiv.appendChild(cb);

        DOMObject.appendChild(div);

        return DOMObject;
    }
});
