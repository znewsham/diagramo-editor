DIAGRAMO.PROPERTIES.TextProperty = function(figure, accessors, label, options){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, accessors, label);
    this.options = options;
};


_.extend(DIAGRAMO.PROPERTIES.TextProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    generateCode: function(editor, DOMObject){
        var self = this;
        var value = this.getValue();

        var div = document.createElement("div");
        div.className = "line";
        var labelDiv = document.createElement("div");
        labelDiv.className = "label";
        labelDiv.textContent = this.label;
        var text;
        if(this.options && this.options.multiline){
            text = document.createElement("textarea");
            div.style.height = "40px";
            text.style.width = "98%";
        }
        else{
            text = document.createElement("input");
            div.appendChild(labelDiv);
        }
        text.placeholder = "Text...";
        text.style.float = "right";
        text.id = this.accessors; // for DOM manipulation
        div.appendChild(text);
        text.value = value;
        text.onkeyup = function(){
            var command = new DIAGRAMO.COMMANDS.PropertyChangeCommand(self.figure, self.accessors, this.value, value);
            command.execute();
            editor.repaint();
        };
        if(this.options.type){
          text.type = this.options.type;
        }

        DOMObject.appendChild(div);

        return DOMObject;
    }
});
