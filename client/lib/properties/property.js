DIAGRAMO.PROPERTIES.Property = function(figure, accessors, label, keyMap){
    this.figure = figure;
    this.accessors = accessors;
    this.label = label;
    this.keyMap = keyMap
};


_.extend(DIAGRAMO.PROPERTIES.Property.prototype, {
    keyDownHandler: function(event){
      
    },
    getValue: function(){
        var obj = this.figure;
        var accessors = this.accessors.split(".");
        for(var i = 0; i < accessors.length; i++){
            var getterName = "get" + accessors[i].charAt(0).toUpperCase() + accessors[i].slice(1);
            if(obj[getterName]){
                obj = obj[getterName]();
            }
            else{
                obj = obj[accessors[i]];
            }
        }
        return obj;
    },
});
