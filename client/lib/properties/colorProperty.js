DIAGRAMO.PROPERTIES.ColorProperty = function(figure, accessors, label, style){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, accessors, label);
    this.style = style;
};


_.extend(DIAGRAMO.PROPERTIES.ColorProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    generateCode: function(editor, DOMObject){
        var self = this;
        var value = this.getValue();

        var uniqueId = new Date().getTime();
        var div = document.createElement("div");
        div.className = "line";
        var labelDiv = document.createElement("div");
        labelDiv.className = "label";
        labelDiv.textContent = this.label;
        div.appendChild(labelDiv);

        var colorSelectorDiv = document.createElement("div");
        if(this.style){
            _.keys(this.style).forEach(function(s){
                colorSelectorDiv.style[s] = self.style[s];
            })
        }
        colorSelectorDiv.id = 'colorSelector' + uniqueId;
        colorSelectorDiv.className = 'color-selector';

        var colorInput = document.createElement("input");
        colorInput.type = "text";
        colorInput.id = 'colorpickerHolder' + uniqueId;
        colorInput.value = value;
        colorSelectorDiv.appendChild(colorInput);

        div.appendChild(colorSelectorDiv);

        DOMObject.appendChild(div);

        var colorPicker = document.getElementById('colorpickerHolder'+uniqueId);

        //let plugin do the job
        $(colorPicker).colorPicker();

        //on change update the figure
        var propExposedToAnonymous = this.property;
        colorPicker.onchange = function() {
            var command = new DIAGRAMO.COMMANDS.PropertyChangeCommand(self.figure, self.accessors, colorPicker.value, value);
            command.execute();
            editor.repaint();
        };
        return DOMObject;
    }
});
