DIAGRAMO.PROPERTIES.ImageUploadProperty = function(figure, accessors, label, style){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, accessors, label);
    this.style = style;
};


_.extend(DIAGRAMO.PROPERTIES.ImageUploadProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
  generateCode: function(editor, DOMObject){
      var self = this;

      var div = document.createElement("div");
      div.className = "line";
      div.style.width = "100%";
      var labelDiv = document.createElement("div");
      labelDiv.className = "label";
      labelDiv.textContent = this.label;

      div.appendChild(labelDiv);

      var selectDiv = document.createElement("div");
      selectDiv.style.cssText ="float: right;";

      selectDiv.id = this.property; // for DOM manipulation
      div.appendChild(selectDiv);
      if(this.style){
          _.keys(this.style).forEach(function(s){
              selectDiv.style[s] = self.style[s];
          })
      }
      var selProperty = this.property; //save it in a separate variable as if refered by (this) it will refert to the 'select' DOM Object

      var btn = document.createElement("input");
      btn.type = "button";
      btn.value = "Upload";
      var dd = document.createElement("div");
      selectDiv.appendChild(dd);
      selectDiv.appendChild(btn);
      var dialog;
      self.button = btn;
      btn.onclick = function(e){
        dialog = $("#file-upload").dialog({autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: [
            {
                text: "Cancel",
                click: function(){
                    if(self.getValue() == window.location){
                      editor.removePaintable(self.figure);
                      editor.selectFigure(null);
                    }
                    dialog.dialog( "close" );
                }
            }
        ],})
        dialog.dialog("open");
      }
      dd.id="file-upload";
      dd.outerHTML = '<div id="file-upload" title="File Upload" style="display: none"><form action="/file-upload" class="dropzone" id="dropzone"></form></div>';
      DOMObject.appendChild(div);
      var dz = new Dropzone("form#dropzone", {
        url: "./common/controller.php?action=upload",

      })

      dz.on("success", function(something, response){
        response = response.replace("%20", "").replace(' ', '');
        var cmd = new DIAGRAMO.COMMANDS.PropertyChangeCommand(self.figure, self.accessors, response, "");
        cmd.execute();
        editor.repaint();
        dialog.dialog("close");
      });
      return DOMObject;
  }
});
