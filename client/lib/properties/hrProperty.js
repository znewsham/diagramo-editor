DIAGRAMO.PROPERTIES.HrProperty = function(){
    DIAGRAMO.PROPERTIES.Property.call(this, null, null, null);
};


_.extend(DIAGRAMO.PROPERTIES.HrProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    generateCode: function(editor, DOMObject){
        var hr = document.createElement("hr");
        hr.size = 2;
        hr.color = "#515151";
        DOMObject.appendChild(hr);
        return DOMObject;
    }
});
