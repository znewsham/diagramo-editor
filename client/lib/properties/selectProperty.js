DIAGRAMO.PROPERTIES.SelectProperty = function(figure, accessors, label, options, style){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, accessors, label);
    this.options = options;
    this.style = style;
};


_.extend(DIAGRAMO.PROPERTIES.SelectProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    keyDownHandler: function(event){
      var ret = true;
      var self = this;
      this.options.forEach(function(opt){
        if(opt.KeyCodes && opt.KeyCodes.indexOf(event.keyCode) != -1 && event.shiftPressed == true){
          ret = false;
          var command = new DIAGRAMO.COMMANDS.PropertyChangeCommand(self.figure, self.accessors, opt.Value, "");
          command.execute();
        }
      });
      return ret;
    },
    generateCode: function(editor, DOMObject){
        var self = this;
        var value = this.getValue();
        if(value instanceof Object){
          value = value.Value;
        }

        var div = document.createElement("div");
        div.className = "line";
        var labelDiv = document.createElement("div");
        labelDiv.className = "label";
        labelDiv.textContent = this.label;
        div.appendChild(labelDiv);
        var select = document.createElement("select");
        select.id = this.property; // for DOM manipulation
        div.appendChild(select);
        if(this.style){
            _.keys(this.style).forEach(function(s){
                select.style[s] = self.style[s];
            })
        }

        for(var i=0; i< this.options.length; i++){
            var option = document.createElement("option");
            option.value = this.options[i].Value;
            option.selected = value == option.value ? "selected" : "";
//            Log.info("\t Text : " + v[i].Text + " Value : " + v[i].Value);
            option.text = this.options[i].Text; //see: http://www.w3schools.com/jsref/coll_select_options.asp
            select.options.add(option); //push does not exist in the options array
            if(option.value == value){
                option.selected = true;
            }
        }

        var selProperty = this.property; //save it in a separate variable as if refered by (this) it will refert to the 'select' DOM Object
        select.onchange = function(){
            var command = new DIAGRAMO.COMMANDS.PropertyChangeCommand(self.figure, self.accessors, this.options[this.selectedIndex].value, value);
            command.execute();
            editor.repaint();
        };

        DOMObject.appendChild(div);

        return DOMObject;
    }
});
