DIAGRAMO.PROPERTIES.CommandProperty = function(figure, accessors, label, imageSrc, command, style){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, accessors, label);
    this.imageSrc = imageSrc;
    this.command = command;
    this.label = label;
    this.style = style;
};


_.extend(DIAGRAMO.PROPERTIES.CommandProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    generateCode: function(editor, DOMObject){
        var self = this;

        var div = document.createElement("div");
        div.className = "line";
        div.style.width = "100%";
        var labelDiv = document.createElement("div");
        labelDiv.className = "label";
        labelDiv.textContent = this.label;

        div.appendChild(labelDiv);

        var selectDiv = document.createElement("div");
        selectDiv.style.cssText ="float: right;";

        selectDiv.id = this.property; // for DOM manipulation
        div.appendChild(selectDiv);
        if(this.style){
            _.keys(this.style).forEach(function(s){
                selectDiv.style[s] = self.style[s];
            })
        }
        var selProperty = this.property; //save it in a separate variable as if refered by (this) it will refert to the 'select' DOM Object

        var img = document.createElement("img");
        img.src = this.imageSrc;
        img.alt = this.label;
        (function(img){
            img.onclick = function(e){
                self.command.execute();
                editor.repaint();
            }
        }(img));
        selectDiv.appendChild(img);

        DOMObject.appendChild(div);

        return DOMObject;
    }
});
