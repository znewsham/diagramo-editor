DIAGRAMO.PROPERTIES.ImageSelectProperty = function(figure, accessors, label, options){
    DIAGRAMO.PROPERTIES.Property.call(this, figure, accessors, label);
    this.options = options;
};


_.extend(DIAGRAMO.PROPERTIES.ImageSelectProperty.prototype, DIAGRAMO.PROPERTIES.Property.prototype, {
    generateCode: function(editor, DOMObject){
        var self = this;
        var value = this.getValue();

        var div = document.createElement("div");
        div.className = "line";
        div.style.width = "100%";
        var labelDiv = document.createElement("div");
        labelDiv.className = "label";
        labelDiv.textContent = this.label;

        div.appendChild(labelDiv);

        var selectDiv = document.createElement("div");
        selectDiv.style.cssText ="float: right;";

        selectDiv.id = this.property; // for DOM manipulation
        div.appendChild(selectDiv);
        var selProperty = this.property; //save it in a separate variable as if refered by (this) it will refert to the 'select' DOM Object

        for(var i=0; i< this.options.length; i++){
            var img = document.createElement("img");
            img.src = this.options[i].Image;
            img.value = this.options[i].Value;
            img.title = this.options[i].Text;
            img.height = 40;
            img.rootSrc = img.src;
            if(this.options[i].Value == value){
                img.src = img.src.replace(".png","") + "-selected.png";
            }
            img.style.height = "40px";
            img.style.width = "40px";
            (function(img){
                img.onmouseover = function(e){
                    for(var i = 0; i < this.parentElement.children.length; i++){
                        this.parentElement.children[i].onmouseout(e);
                    }
                    this.src = this.rootSrc.replace(".png","") + "-selected.png";
                }
                img.onmouseout = function(e){
                    if(value == this.value){
                        this.src = this.rootSrc.replace(".png","") + "-selected.png";;
                    }
                    else{
                        this.src = this.rootSrc;
                    }
                }
                img.onclick = function(e){
                    var command = new DIAGRAMO.COMMANDS.PropertyChangeCommand(self.figure, self.accessors, this.value, value);
                    command.execute();
                    editor.repaint();
                    value = this.value;
                    for(var i = 0; i < this.parentElement.children.length; i++){
                        this.parentElement.children[i].onmouseout(e);
                    }
                }
            }(img));
            selectDiv.appendChild(img);
        }

        div.style.height = "40px";
        DOMObject.appendChild(div);

        return DOMObject;
    }
});
