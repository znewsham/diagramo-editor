function ConnectionPoint(){
    this.beforeConnectHandlers = null;
    this.afterConnectHandlers = null;
    this.beforeConnect(this.beforeConnectHandler);
}
DIAGRAMO.ConnectionPoint = ConnectionPoint;

/**Color used by default to draw the connection point*/
ConnectionPoint.NORMAL_COLOR = "#FFFF33"; //yellow.

ConnectionPoint.FIGURE_COLOR = "#ADD8E6";

/*Color used to signal that the 2 connection points are about to glue*/
ConnectionPoint.OVER_COLOR = "#FF9900"; //orange

/*Color used to draw connected (glued) connection points*/
ConnectionPoint.CONNECTED_COLOR = "#ff0000"; //red

/**Connection point default radius*/
ConnectionPoint.RADIUS = 4;

/**Connection point (liked to)/ connectionPointType figure*/
ConnectionPoint.TYPE_FIGURE = 'figure';

/**Connection point (liked to)/ connectionPointType connector*/
ConnectionPoint.TYPE_CONNECTOR = 'connector';

DIAGRAMO.ConnectionPoint.prototype = {
        afterConnect: function(eventHandler){
            if(_.isUndefined(this.afterConnectHandlers) || _.isNull(this.afterConnectHandlers)){
                this.afterConnectHandlers = [];
            }
            this.afterConnectHandlers.push(eventHandler);
        },
        beforeConnect: function(eventHandler){
            if(_.isUndefined(this.beforeConnectHandlers) || _.isNull(this.beforeConnectHandlers)){
                this.beforeConnectHandlers = [];
            }
            this.beforeConnectHandlers.push(eventHandler);
        },
}
