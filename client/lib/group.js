function Group(){
    this.id = DIAGRAMO.generateId();
    DIAGRAMO.Figure.call(this, this.id);
    this.isGroup = true;
    this.isTemp = true;
    this.properties = [];
    this.connectionPoints = [];
    this.groups = [];
    this.style.fillStyle = null;
}

DIAGRAMO.Group = Group;
Group.load = function(jsonObject){
    if(!jsonObject.version && jsonObject.paintables){
      jsonObject.version = "1.1";
    }
    if(jsonObject.version != DIAGRAMO.VERSION){
      DiagramoEditor.convertors[!jsonObject.version ? null : jsonObject.version][DIAGRAMO.VERSION].convertFigure(jsonObject);
    }
    //hack fix
    if(_.findWhere(jsonObject.paintables, {oType: "DIAGRAMO.SETS.basic.Text"})){
      DiagramoEditor.convertors["1.1"][DIAGRAMO.VERSION].convertFigure(jsonObject);
    }
    var group = new DIAGRAMO.Group();
    group = DIAGRAMO.Figure.load(jsonObject, group);
    group.glues = jsonObject.glues;//required for favs
    group.isTemp = jsonObject.isTemp;
    if(!group.glues){
      jsonObject.glues = [];
      var fConPoints = [];
      var cConPoints = [];
      group.paintables.forEach(function(f){
        if(!f.isFigure){
          return;
        }
        fConPoints = fConPoints.concat(f.connectionPoints);
      });
      group.paintables.forEach(function(c){
        if(!c.isConnector){
          return;
        }
        cConPoints = cConPoints.concat(c.getConnectionPoints());
      })
      fConPoints.forEach(function(fc){
        var cc = _.find(cConPoints, function(c){
          return Math.abs(c.point.x - fc.point.x) < 0.05 && Math.abs(c.point.y - fc.point.y) < 0.05;
        });
        if(cc){
          cConPoints = _.without(cConPoints, cc);
          jsonObject.glues.push({
            automatic: false,
            figureConPointId: fc.id,
            connectorConPointId: cc.id,
            oType: "DIAGRAMO.Glue"
          });
        }
      });
    }
    return group;
}

Group.init = function(){
    _.extend(Group.prototype, DIAGRAMO.Figure.prototype, {
      getConnectionPoints: function(){
        var conPoints = [];
        this.paintables.forEach(function(p){
          if(p.connectionPoints){
            conPoints = conPoints.concat(p.connectionPoints);
          }
          else if(p.getConnectionPoints){
            conPoints = conPoints.concat(p.getConnectionPoints());
          }
        });
        return conPoints;
      },
        contains: function(x, y){
          var bounds = this.getBounds();
          return bounds[0] < x && bounds[2] > x && bounds[1] < y && bounds[3] > y;
        },
        getCursor: function(){
          return "crosshair";
        },
        clone: function(){
          var group = new DIAGRAMO.Group();
          this.paintables.forEach(function(p){
            var p1 = p.clone();
            p1.id = DIAGRAMO.generateId();
            group.paintables.push(p1);
          });
          group.id = DIAGRAMO.generateId();
          group.isTemp = this.isTemp;
          return group;
        },
        getGlues: function(){
          var glues = [];
          this.paintables.forEach(function(p){
            if(p.isGroup){
              glues = glues.concat(g.getGlues());
            }
            else if(p.isFigure){
              p.connectionPoints.forEach(function(c){
                if(c.glues){
                  glues = glues.concat(c.glues);
                }
              });
            }
          });
          return glues;
        },
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Figure.prototype.toJSONObject.call(this), {
                oType: "DIAGRAMO.Group",
                isTemp: this.isTemp,
                glues: []
            });
            this.getGlues().forEach(function(g){
              ret.glues.push(g.toJSONObject());
            });
            return ret;
        },
        transform: function(matrix){
            var self = this;
            this.paintables.forEach(function(paintable){
                if(!paintable.isConnector){
                    var tmpGlues = [];
                    if(paintable.getGlues){
                        paintable.getGlues().forEach(function(glue){
                            if(self.paintables.indexOf(glue.connectorConPoint.parent) >= 0){
                                tmpGlues.push(glue);
                                glue.figureConPoint.glues = _.without(glue.figureConPoint.glues, glue);
                                glue.connectorConPoint.glue = null;
                            }
                        });
                    }
                    paintable.transform(matrix, true);
                    tmpGlues.forEach(function(glue){
                        glue.connectorConPoint.glue = glue;
                        glue.figureConPoint.glues = glue.figureConPoint.glues.concat(glue);
                    });
                }
            });
            this.paintables.forEach(function(paintable){
                if(paintable.isConnector){

                    var exclude = false;
                    var include = null;
                    paintable.getConnectionPoints().forEach(function(conPoint){
                        //if we are moving a connector, whose connected figure(s) are not in the group, do not transform at all.
                        if(conPoint.glue && _.indexOf(self.paintables, conPoint.glue.figureConPoint.parent) == -1){
                            exclude = true;
                        }
                        //if we are moving a connector whose connected figure(s) are in the same group, we need to transform
                        if(conPoint.glue && _.indexOf(self.paintables, conPoint.glue.figureConPoint.parent) != -1){
                          include = conPoint;
                        }
                    });
                    if(!exclude){
                        paintable.transform(matrix);
                    }

                    //it is connected to something inside and outside
                    if(exclude && include != null){
                      include.transform(matrix);
                      paintable.activeHandle = include;
                      paintable.trigger("afterTransform", [matrix]);
                      paintable.activeHandle = null;
                    }
                }
            });
            if(this.rotationCoords.length!=0){
                this.rotationCoords[0].transform(matrix);
                this.rotationCoords[1].transform(matrix);
            }

            this.trigger("afterTransform", [matrix, true, undefined]);
        }
    });
};
