/**A Glue just glues together 2 ConnectionPoints.
 *Glued ConnectionPoints usually belongs to a Connector and a Figure.
 *
 *@constructor
 *@this {Glue}
 *@param {Number} cp1Id - the id of the first {ConnectionPoint} (usually from a {Figure})
 *@param {Number} cp2Id - the id of the second {ConnectionPoint} (usualy from a {Connector})
 *@param {Boolean} automatic - type of connection connector to a figure:
 * if true - {Connector} connects a {Figure} itself
 * else - {Connector} connects specific {ConnectionPoint} of {Figure}
 **/
function Glue(figureConPoint,connectorConPoint,automatic){
    /**First shape's id (usually from a {Figure})*/
    this.figureConPoint = figureConPoint;

    /**Second shape's id (usualy from a {Connector})*/
    this.connectorConPoint = connectorConPoint;

    /*By default all the Glues are created with the first number as Figure's id and second number as
     *Connector's id. In the future glues can be used to glue other types as well*/

    /**First id type (usually 'figure')*/
    this.type1 = 'figure';

    /**First id type (usually 'connector')*/
    this.type2 = 'connector';

    /**object type used for JSON deserialization*/
    this.oType = 'Glue';

    /**Type of connector's behaviour:
     * if it's true - connector connects the whole figure and touches it's optimum connection point
     * if it's false - connector connects one fixed connection point of the figure
     * */
    this.automatic = automatic;
}
DIAGRAMO.Glue = Glue;
/**Creates a {Glue} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Glue} a newly constructed Glue
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Glue.load = function(o){
    var newGlue = new Glue(o.figureConPointId, o.connectorConPointId, o.automatic); //fake constructor

    return newGlue;
}


/**Creates a an {Array} of {Glue} out of JSON parsed array
 *@param {JSONObject} v - the JSON parsed {Array}
 *@return {Array} of newly loaded {Glue}s
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Glue.loadArray = function(v){
    var newGlues = [];

    for(var i=0; i<v.length; i++){
        newGlues.push(Glue.load(v[i]));
    }

    return newGlues;
}

/**Clones an array of points
 *@param {Array} v - the array of {Glue}s
 *@return an {Array} of {Glue}s
 **/
Glue.cloneArray = function(v){
    var newGlues = [];
    for(var i=0; i< v.length; i++){
        newGlues.push(v[i].clone());
    }
    return newGlues;
}

Glue.prototype = {
    toJSONObject: function(){
        return {
            oType: "DIAGRAMO.Glue",
            figureConPointId: this.figureConPoint.id,
            connectorConPointId: this.connectorConPoint.id,
            automatic: this.automatic
        };
    },


    /**Clone current {Glue}
     **/
    clone: function(){
        return new Glue(this.id1, this.id2, this.automatic);
    },

    /**Compares to another Glue
     *@param {Glue} anotherGlue -  - the other glue
     *@return {Boolean} - true if equals, false otherwise
     **/
    equals:function(anotherGlue){
        if(!anotherGlue instanceof Glue){
            return false;
        }

        return this.connectorConPoint == anotherGlue.connectorConPoint
        && this.figureConPoint == anotherGlue.figureConPoint
        && this.automatic == anotherGlue.automatic
        && this.type1 == anotherGlue.type1
        && this.type2 == anotherGlue.type2;
    },

    /**String representation of the Glue
     *@return {String} - the representation
     **/
    toString:function(){
        return 'Glue : (id1 = ' + this.id1
            + ', id2 = ' + this.id2
            + ', type1 = ' + this.type1
            + ', type2 = ' + this.type2
            + ', automatic = ' + this.automatic
            + ')';
    },
    //only transform the connector
    transform:function(matrix){
        var self = this;
        this.connectorConPoint.transform(matrix);
    }
}
