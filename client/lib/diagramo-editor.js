
DIAGRAMO.PROPERTIES = {};
DIAGRAMO.SETS = {};

function DiagramoEditor(canvas){
    DIAGRAMO.Diagramo.call(this,canvas);
    this.redraw = false;
    this.selectionArea = new DIAGRAMO.Polygon();
    this.selectionArea.points.push(new DIAGRAMO.Point(0,0));
    this.selectionArea.points.push(new DIAGRAMO.Point(0,0));
    this.selectionArea.points.push(new DIAGRAMO.Point(0,0));
    this.selectionArea.points.push(new DIAGRAMO.Point(0,0));
    this.selectionArea.style.strokeStyle = 'grey';
    this.selectionArea.style.gradientBounds = [];
    this.selectionArea.style.lineWidth = '1';
    this.selectionArea.visible = false;
    this.selectionArea.isHidden = true;
    this.selectionArea.zIndex = 1;
    this.addPaintable(this.selectionArea);
    this.wasFigureSelected = false;
    this.isMouseDown = false;
    this.snapToGrid = false;
    /**
     * @property {HandleManager} manages handles
     */
    this.handleManager = new DIAGRAMO.HandleManager(this);

    /**An {Array} of {Connector}s. Keeps all Connectors from canvas*/
    this.connectors = [];

    /**An {Array} of {ConnectionPoint}s. Keeps all ConnectionPoints from canvas*/
    this.connectionPoints = [];

    /**An {Array} of {Glue}s. Keeps all Glues from canvas*/
    this.glues = [];
    /**
     * @property {History} history of actions
     */
    this.history = new DIAGRAMO.History(this);
    this.lastMove = null;
    this.selectedObject = null;
    this.state = {
        name: DIAGRAMO.STATES.STATE_NONE
    }
    document.onclick = function(event){
        _.keys(DIAGRAMO.editors).forEach(function(name){
            if(DIAGRAMO.editors[name].selectedObject && DIAGRAMO.editors[name].selectedObject.isNotFinished){
                DIAGRAMO.editors[name].keyDownHandler({
                    keyCode: DIAGRAMO.KEYS.ESCAPE
                });
            }
        });
    };
    document.onmousemove = function(event){
        event.stopPropagation();
        event.preventDefault();
        if(DIAGRAMO.dragging){
            DIAGRAMO.dragging.mouseMoveHandler.call(DIAGRAMO.dragging, event);
        }
    }
}
DIAGRAMO.editors = {};
DIAGRAMO.GetDiagramoEditor = function(canvas){
    if(DIAGRAMO.editors[canvas.id] == undefined){
        DIAGRAMO.editors[canvas.id] = new DIAGRAMO.DiagramoEditor(canvas);
    }
    return DIAGRAMO.editors[canvas.id];
}

DIAGRAMO.DiagramoEditor = DiagramoEditor;
DIAGRAMO.SETS.init = function(){
    _.keys(DIAGRAMO.SETS).forEach(function(set){
       if(set != "init"){
           DIAGRAMO.SETS[set].init();
       }
    });
}
DIAGRAMO.CONNECTORS = {};
DIAGRAMO.CONNECTORS.init = function(){
    _.keys(DIAGRAMO.CONNECTORS).forEach(function(con){
       if(con != "init"){
           DIAGRAMO.CONNECTORS[con].init();
       }
    });
}
DIAGRAMO.STATES = {
  STATE_NONE: "none",
  STATE_CONNECTOR_PICK_FIRST: "connectorPickFirst",
  STATE_CONNECTOR_MOVE_POINT: "connectorMovePoint"
};
DIAGRAMO.COMMANDS = {};
DIAGRAMO.init = function(){
    DIAGRAMO.Handle.init();
    DIAGRAMO.FigureHandle.init();
    DIAGRAMO.ConnectorHandle.init();
    DIAGRAMO.Figure.init();
    DIAGRAMO.Group.init();
    DIAGRAMO.Connector.init();
    DIAGRAMO.ConnectorConnectionPoint.init();
    DIAGRAMO.FigureConnectionPoint.init();
    DIAGRAMO.SETS.init();
    DIAGRAMO.CONNECTORS.init();
    DIAGRAMO.Grid.init();
};

_.extend(DiagramoEditor.prototype, DIAGRAMO.Diagramo.prototype,{
    clone: function(object){
      return JSON.parse(JSON.stringify(this.toJSONObject()));
    },
    load: function(jsonObject){
        var self = this;
        DIAGRAMO.Diagramo.prototype.load.call(this, jsonObject);
        self.connectors.splice(0, self.connectors.length);
        self.glues.splice(0, self.glues.length);
        self.paintables.splice(0, self.paintables.length);
        self.getConnectionPoints().splice(0, self.getConnectionPoints().length);
        this.selectedObject = null;
        self.background = null;

        self.canvas.width = jsonObject.width;
        self.canvas.height = jsonObject.height;
        if(jsonObject.background){
            self.background = DIAGRAMO.Image.load(jsonObject.background);
            self.background.image.onerror = function(){
              self.background.image.src = DIAGRAMO.PROPERTIES.defaultBackground;
            }
        }
        else{
          self.background = new DIAGRAMO.Image(new DIAGRAMO.Point(0, 0), DIAGRAMO.PROPERTIES.defaultBackground);
        }
        self.background.isHidden = true;
        self.background.visible = true;
        self.background.zIndex = 1000;
        this.addPaintable(self.background);
        this.handleManager = new DIAGRAMO.HandleManager(this);
        //connectors
        if(typeof jsonObject.connectors == "function"){
          jsonObject.connectors = jsonObject.connectors();
        }
        jsonObject.connectors.forEach(function(connector){
          //HACK: to deal with previous issue where duplicate connectors were saved
          if(_.findWhere(self.connectors, {id: connector.id})){
            return;
          }
            var con = DIAGRAMO.load(connector);
            self.addConnector(con);
        });

        var hasGrid = false;
        //figures
        jsonObject.paintables.forEach(function(paintable){
            var figure = DIAGRAMO.load(paintable);
            var inBounds =
              DIAGRAMO.Util.areBoundsInBounds(figure.getBounds(), [0, 0, self.canvas.width, self.canvas.height]) ||
              DIAGRAMO.Util.doBoundsIntersect(figure.getBounds(), [0, 0, self.canvas.width, self.canvas.height]);
            if(figure.isFigure && inBounds){
                self.addFigure(figure);
            }
            else{
                self.addPaintable(figure);
            }
            if(figure instanceof DIAGRAMO.Grid){
              hasGrid = true;
            }
        });

        if(!hasGrid){
          this.grid = new DIAGRAMO.Grid(0, 0, this.canvas.width, this.canvas.height);
          this.grid.visible = false;
          this.grid.hidden = true; //make it non selectable
          this.grid.zIndex = 999;
          self.addPaintable(this.grid);
        }

        //glues
        jsonObject.glues.forEach(function(glue){
            var g = DIAGRAMO.load(glue);
            g.figureConPoint = _.findWhere(self.getConnectionPoints(), {id: g.figureConPoint});
            g.connectorConPoint = _.findWhere(self.getConnectionPoints(), {id: g.connectorConPoint});
            if(g.figureConPoint && g.connectorConPoint){
                g.figureConPoint.glues.push(g);
                g.connectorConPoint.glue = g;
                self.glues.push(g);
            }
        });
        this.addPaintable(this.selectionArea);
        this.getConnectionPoints().forEach(function(cp){
          if(!cp.id){
            cp.id = DIAGRAMO.generateId();
          }
        });
        this.repaint();
    },
    showCustomFigures: function(customFigureProvider, DOMObject, title){
        if(!customFigureProvider.rootProvider){
          this.customFigureProvider = customFigureProvider;
        }
        this.customFigureDOMObject = DOMObject;
        var viewer = new DIAGRAMO.CustomFigureViewer(this, customFigureProvider, DOMObject);
    },
    saveCustomFigure: function(figure){
        this.customFigureProvider.saveFigure(figure);
    },
    toJSONObject: function(){
        var ret = {
            version: 1.1,
            background: this.background ? this.background.toJSONObject() : undefined,
            paintables: [],
            handleManager: this.handleManager.toJSONObject(),
            connectors: [],
            connectionPoints: [],
            glues: [],
            width: this.canvas.width,
            height: this.canvas.height
        };

        this.paintables.forEach(function(p){
          if(p.isConnector){
            ret.connectors.push(p.toJSONObject());
          }
        });
        this.getConnectionPoints().forEach(function(connectionPoint){
            ret.connectionPoints.push(connectionPoint.toJSONObject());
        });

        this.glues.forEach(function(glue){
           ret.glues.push(glue.toJSONObject());
        });

        var self = this;
        this.paintables.forEach(function(paintable){
            if(!(paintable instanceof DIAGRAMO.Grid) && !paintable.isConnector && paintable != self.background && !(paintable instanceof DIAGRAMO.Polygon) && !paintable.isFigureHandle && self.handleManager.selectRect != paintable && self.selectRect != paintable){
              ret.paintables.push(paintable.toJSONObject());
            }
        });

        return ret;
    },
    figureConnectionPointsSetVisible: function(visible, conConPoint){
        var self = this;
        this.getConnectionPoints().forEach(function(figConPoint){
            if(figConPoint.connectionPointType == DIAGRAMO.ConnectionPoint.TYPE_FIGURE && (!visible || !conConPoint || !conConPoint.trigger("beforeConnect",[figConPoint]))){
                figConPoint.visible = visible;
                self.redraw = true;
            }
        });
    },
    getConnectionPoints: function(){
      var conPoints = [];
      this.paintables.forEach(function(p){
        if(p.getConnectionPoints){
          conPoints = conPoints.concat(p.getConnectionPoints());
        }
        else if(p.connectionPoints){
          conPoints = conPoints.concat(p.connectionPoints);
        }
      });
      return conPoints;
    },
    addConnectionPoints: function(conPoints){
        if(!conPoints){
            return;
        }
        var self = this;
        conPoints.forEach(function(conPoint){
           self.connectionPoints.push(conPoint);
           conPoint.visible = false;
           conPoint.mouseDown(function(event){
               var editor = self;
               if(editor.state.name == DIAGRAMO.STATES.STATE_CONNECTOR_PICK_FIRST){
                   var end = this.point.clone();
                   var command = new DIAGRAMO.COMMANDS.ConnectorCreateCommand(editor, editor.state.connectorConstructor, this.point.clone(), end);
                   command.execute();
                   editor.makeGlue(this, command.connector.getConnectionPoints()[0]);
               }
           });
        });
    },
    setupConnector: function(connector){
        var self = this;
        connector.setup = true;
        connector.handleMouseDown = function(event){
            self.handleManager.dragging = this;
            self.handleManager.shape.activeHandle = self.handleManager.dragging;
            return false;//we want this to bubble
        };
        connector.connectionPointMouseDownHandler = function(event){
            var editor = self;
            editor.handleManager.dragging = this;

            if(this.isConnectionPoint){
                editor.figureConnectionPointsSetVisible(true, this);
                if(editor.redraw){
                    editor.repaint();
                }
            }
            return false;
        };
        connector.handles.forEach(function(handle){
            if(!handle.isConnectionPoint){
                handle.mouseDown(connector.handleMouseDown);
            }
        });
        connector.getConnectionPoints().forEach(function(conPoint){
            conPoint.mouseDown(function(event){connector.connectionPointMouseDownHandler.call(this, event)});
        });
        connector.mouseDown(function(event){
          this.trigger("beforeSelect", [{click: true}]);
          var ret = self.selectObject.call(self, event.paintable);
          if(DIAGRAMO.ctrlPressed && self.canvas.ondblclick){
            self.canvas.ondblclick(event.event);
          }
          return ret;
        });
    },
    addPaintable: function(paintable){
        if(!paintable.zIndex){
            paintable.zIndex = this.paintables.length;
        }
        DIAGRAMO.Diagramo.prototype.addPaintable.call(this, paintable);
    },
    addConnector: function(connector){
        var self = this;
        if(_.isUndefined(connector.id)){
            connector.id = this.generateId();
        }
        if(!connector.setup){
          this.setupConnector(connector);
        }
        if(connector.connectionPoints){
          connector.afterTransform(function(matrix, transformConnector, transformText){
            self.transformConnectionPoints(this, matrix, transformConnector);
          });
        }
        this.connectors.push(connector);
        var self = this;
        connector.zIndex = 1;
        connector.handles.forEach(function(handle){
            if(handle.isConnectionPoint){
                self.connectionPoints.push(handle);
            }
        });
        this.addPaintable(connector);
        connector.zIndex = this.paintables.length;
    },
    addFigure: function(figure){
        var self = this;
        if(_.isUndefined(figure.id)){
            figure.id = this.generateId();
        }

        this.addPaintable(figure);
        this.addConnectionPoints(figure.connectionPoints);

        if(!figure.setup){
          figure.mouseDown(function(event){
              if(self.state.name != DIAGRAMO.STATES.STATE_CONNECTOR_PICK_FIRST && self.state.name != DIAGRAMO.STATES.STATE_CONNECTOR_MOVE_POINT){
                  return self.selectObject.call(self, event.paintable);
              }
              this.trigger("beforeSelect", [{click: true}]);
          });
          figure.afterTransform(function(matrix, transformConnector, transformText){
              //cascade transform to the connection point
              self.redraw = true;
              self.transformConnectionPoints(this, matrix, transformConnector);
          });
        }
        figure.setup = true;
        if(figure.rotationCoords.length != 2){
            figure.finalise();
        }
    },
    setupEditor: function(object){
        DIAGRAMO.propertyEditor($("#editor")[0], this, object, DIAGRAMO.PROPERTIES.EXTRA(object, this));
    },
    keyDownHandler: function(event){
        var self = this;
        event.shiftPressed = this.shiftPressed;
        event.cntrlPressed = this.ctrlPressed;
        event.cmdPressed = this.cmdPressed;

        /*if(this.selectedObject && this.selectedObject.trigger("keyDown", [event]) === false){
          this.repaint();
          return;
        }
        else*/ if(this.selectedObject && DIAGRAMO.propertyEditor.keyDownHandler(event) === false){
          this.repaint();
          return;
        }
        if(event.keyCode == DIAGRAMO.KEYS.ESCAPE){
            if(DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler){
              self.canvas.onmousedown = DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler;
            }
            if(this.selectedObject && this.selectedObject.isNotFinished){
                var finished = this.selectedObject.finish();
                if(finished){
                  this.setupConnector(this.selectedObject);
                  this.isMouseDown = false;
                }
                else{
                  self.handleManager.dragging = null;
                  this.removePaintable(this.selectedObject);
                  this.selectFigure(null);
                }
            }
            else{
                this.selectedObject = null;
                this.setupEditor(null);
                this.handleManager.setShape(null);
            }
            this.figureConnectionPointsSetVisible(false);
            this.repaint();
        }
        else if(event.keyCode == DIAGRAMO.KEYS.DELETE || event.keyCode == DIAGRAMO.KEYS.BACKSPACE){
            if(this.selectedObject){
                if(this.selectedObject.isFigure){
                    var cmd = new DIAGRAMO.COMMANDS.FigureDeleteCommand(this, this.selectedObject);
                    cmd.execute();
                }
                else if(this.selectedObject.isConnector){
                    var cmd = new DIAGRAMO.COMMANDS.ConnectorDeleteCommand(this, this.selectedObject);
                    cmd.execute();
                }
                this.selectFigure(null);
            }
            this.repaint();
        }
        else if(event.keyCode == DIAGRAMO.KEYS.SHIFT){
            this.shiftPressed = true;
            DIAGRAMO.shiftPressed = true;
        }
        else if(event.keyCode == DIAGRAMO.KEYS.CTRL || DIAGRAMO.KEYS.isCommand(event.keyCode)){
          this.ctrlPressed = true;
          DIAGRAMO.ctrlPressed = true;
        }
        else if(event.keyCode == DIAGRAMO.KEYS.ALT){
          this.altPressed = true;
          DIAGRAMO.altPressed = true;
        }
        if(this.ctrlPressed && event.keyCode == DIAGRAMO.KEYS.S && this.save){
          this.save();
        }
        if(this.ctrlPressed && (event.keyCode == DIAGRAMO.KEYS.C || event.keyCode == DIAGRAMO.KEYS.c)){
          this.copiedObject = this.selectedObject;
        }
        if(this.ctrlPressed && (event.keyCode == DIAGRAMO.KEYS.V || event.keyCode == DIAGRAMO.KEYS.v)){
          if(this.copiedObject){
            var copy;
            if(this.copiedObject.isGroup){
              var figures = [];
              var glues = [];
              this.copiedObject.paintables.forEach(function(p){
                if(!p.isConnector){
                  var p1 = p.clone();
                  p1.oldId = p.id;
                  figures.push(p1);
                  self.addFigure(p1);
                }
              });
              this.copiedObject.paintables.forEach(function(p){
                if(p.isConnector){
                  var p1 = p.clone();
                  figures.push(p1);
                  self.addConnector(p1);
                  var glue = p.getConnectionPoints()[0].glue;//_.findWhere(self.glues, {connectorConPoint: _.where(p.handles, {isConnectionPoint: true})[0]});
                  var index = 0;
                  if(glue){
                    if(_.where(figures, {oldId: glue.figureConPoint.parent.id})){
                      var fIndex = -1;
                      for(var i = 0; i < glue.figureConPoint.parent.getConnectionPoints().length; i++){
                        if(glue.figureConPoint.parent.connectionPoints[i] == glue.figureConPoint){
                          fIndex = i;
                        }
                      }
                    }
                    if(fIndex != -1){
                      glues.push({
                        connectorConPoint: p1.getConnectionPoints()[0],
                        figureConPoint: _.findWhere(figures, {oldId: glue.figureConPoint.parent.id}).connectionPoints[fIndex]
                      });
                    }
                  }
                  glue = p.getConnectionPoints()[1].glue;//_.findWhere(self.glues, {connectorConPoint: _.where(p.handles, {isConnectionPoint: true})[1]});
                  index = 1;
                  if(glue){
                    if(_.where(figures, {oldId: glue.figureConPoint.parent.id})){
                      var fIndex = -1;
                      for(var i = 0; i < glue.figureConPoint.parent.getConnectionPoints().length; i++){
                        if(glue.figureConPoint.parent.connectionPoints[i] == glue.figureConPoint){
                          fIndex = i;
                        }
                      }
                    }
                    if(fIndex != -1){
                      glues.push({
                        connectorConPoint: p1.getConnectionPoints()[1],
                        figureConPoint: _.findWhere(figures, {oldId: glue.figureConPoint.parent.id}).connectionPoints[fIndex]
                      });
                    }
                  }
                }
              });
              glues.forEach(function(g){
                self.makeGlue(g.figureConPoint, g.connectorConPoint);
              });
              copy = this.createGroup(figures);
            }
            else{
              copy = this.copiedObject.clone();
            }
            copy.zIndex = undefined;
            this.copiedObject = copy;
            if(copy.isConnector){
              this.addConnector(copy);
            }
            else{
              this.addFigure(copy);
            }
            copy.transform(DIAGRAMO.Matrix.translationMatrix(10, 10));
            var cmd = new DIAGRAMO.COMMANDS.BringToFrontCommand(this, copy);
            cmd.execute();
            this.selectObject(copy);
            this.isMouseDown = false;
          }
        }

        if(this.shiftPressed){
          if(this.availableConnectors){
            this.availableConnectors.some(function(con){
              if(con.KeyCodes && con.KeyCodes.indexOf(event.keyCode) != -1){
                self.figureConnectionPointsSetVisible(false);
                var connector = new DIAGRAMO.CONNECTORS[con.key](new DIAGRAMO.Point(0, 0), new DIAGRAMO.Point(0, 0));

                self.selectConnector(connector);
                self.setupConnector(connector);
                DIAGRAMO.propertyEditor($("#editor")[0], null);
                DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler = self.canvas.onmousedown;
                self.canvas.onmousedown = function(e){
                    e.stopPropagation();
                    e.bubbles = false;
                    e.preventDefault = true;
                    self.mouseDownHandler({x: e.offsetX * self.scale, y: e.offsetY * self.scale});
                }
                //self.figureConnectionPointsSetVisible(true, connector.getConnectionPoints()[0]);
                self.repaint();
                self.handleManager.dragging = connector.getConnectionPoints()[0];
                return true;

              }
            });
          }
        }
        this.repaint();
    },
    keyUpHandler: function(event){
        if(event.keyCode == DIAGRAMO.KEYS.SHIFT){
            this.shiftPressed = false;
            DIAGRAMO.shiftPressed = false;
        }
        else if(event.keyCode == DIAGRAMO.KEYS.CTRL || DIAGRAMO.KEYS.isCommand(event.keyCode)){
          this.ctrlPressed = false;
          DIAGRAMO.ctrlPressed = false;
        }
        else if(event.keyCode == DIAGRAMO.KEYS.ALT){
          this.altPressed = false;
          DIAGRAMO.altPressed = false;
        }
        this.repaint();
    },
    mouseUpHandler: function(event){
        var self = this;
        this.redraw = false;
        var stop = this.connectorManagerMouseUpHandler(event);
        if(!stop){
            this.figureConnectionPointsSetVisible(false);
            this.handleManager.mouseUpHandler(event);
            if(this.selectionArea.visible){
                this.redraw = true;
                this.selectionArea.visible = false;
                var bounds = this.selectionArea.getBounds();
                var figures;
                if(bounds[2] - bounds[0] < 5 && bounds[3] - bounds[1] < 5){
                  figures = this.getPaintableByXY(
                    bounds[0] + (bounds[2] - bounds[0]) / 2,
                    bounds[1] + (bounds[3] - bounds[1]) / 2,
                    "isHidden",
                    false
                  );
                  if(figures){
                    figures = [figures];
                  }
                  else{
                    figures = [];
                  }
                }
                else{
                  figures = this.getPaintablesIntersectingBounds(this.selectionArea.getBounds());
                }
                figures.forEach(function(fig){
                  fig.trigger("beforeSelect", [{click: false}]);
                });
                if(figures.length > 1){
                    var group = this.createGroup(figures);
                    this.addFigure(group);
                    this.selectObject(group);
                }
                else if(figures.length == 1){
                    this.selectObject(figures[0]);
                }
            }
            //TODO: Find out what this did? It was removed because it caused problems with selecting component by triggering beforeSelect twice, once on MD once on MU
            /*if(this.selectedObject != null){
              this.selectObject(this.selectedObject);
            }*/
            this.isMouseDown = false;
        }
        //this.setupEditor(this.selectedObject);
        this.repaint();
    },
    mouseDownHandler: function(event){
        var self = this;
        this.isMouseDown = true;
        var stop = this.connectorManagerMouseDownHandler(event);
        if(!stop && this.handleManager.getSelectedHandle() == null){
            this.redraw = false;
            this.handleManager.setShape(null);
            if(this.selectedObject && this.selectedObject.isGroup && this.selectedObject.isTemp){
                this.destroyGroup(this.selectedObject);
            }
            this.selectedObject = null;
            this.canvas.style.cursor = "";
        }
        if(!stop){
            this.selectionArea.visible = true;
            this.selectionArea.points[0].x = event.x;
            this.selectionArea.points[0].y = event.y;
            this.selectionArea.points[1].x = event.x;
            this.selectionArea.points[1].y = event.y;
            this.selectionArea.points[2].x = event.x;
            this.selectionArea.points[2].y = event.y;
            this.selectionArea.points[3].x = event.x;
            this.selectionArea.points[3].y = event.y;
        }
        this.startDragging = {x: event.x, y: event.y};
        this.setupEditor(null);
        this.repaint();
    },
    mouseOverHandler: function(event){
    },
    mouseMoveHandler: function(event){
        this.redraw = false;
        var self = this;
        event.lastMove = this.lastMove;
        var gridSize = DIAGRAMO.PROPERTIES.GRID_SIZE;
        if(this.snapToGrid){
          event.x = Math.round(event.x / gridSize) * gridSize;
          event.y = Math.round(event.y / gridSize) * gridSize;
        }

        if(this.lastMove != null && this.isMouseDown){
            if(this.handleManager.getSelectedHandle() != null){
                this.handleManager.mouseMoveHandler(event);
                this.redraw = true;
            }
            else if(this.selectedObject != null && (this.selectedObject.isFigure || !this.selectedObject.isGlued())){
                var figure = this.selectedObject;
                var cmd = new DIAGRAMO.COMMANDS.FigureTranslateCommand(figure, DIAGRAMO.Matrix.translationMatrix(event.x - this.lastMove[0], event.y - this.lastMove[1]));
                this.history.addUndo(cmd);
                cmd.execute();
                this.handleManager.setShape(figure);
                this.redraw = true;
            }
            else{
                this.selectionArea.points[1].x = event.x;
                this.selectionArea.points[2].x = event.x;
                this.selectionArea.points[2].y = event.y;
                this.selectionArea.points[3].y = event.y;
                this.redraw = true;
            }
        }
        if(this.isMouseDown){
          $("#full-editor-wrapper").hide();
          //this.setupEditor(null);
        }
        if(this.snapToGrid && this.selectedObject && this.isMouseDown){
          var bounds = this.selectedObject.getBounds();
          var adjustX = Math.round(bounds[0] / gridSize) * gridSize - bounds[0];
          var adjustY = Math.round(bounds[1] / gridSize) * gridSize - bounds[1];
          if(adjustX != 0 || adjustY != 0){
            this.selectedObject.transform(DIAGRAMO.Matrix.translationMatrix(adjustX, adjustY));
          }
        }
        else if(this.snapToFigure && this.mouseDown && this.selectedObject && this.selectedObject.isFigure){
          var bounds = this.selectedObject.getBounds();
          var center = DIAGRAMO.Util.getCenter(bounds);
          var snapX = [];
          var snapY = [];
          _.filter(this.paintables, function(p){
            return p.isVisible() && !p.isHidden
          }).forEach(function(p){

            //pLeft
            if(p != self.selectedObject &&
              (
                Math.abs(pBounds[0] - bounds[0]) < DIAGRAMO.SNAP_RADIUS ||
                Math.abs(pBounds[0] - bounds[2]) < DIAGRAMO.SNAP_RADIUS
              )
            ){
                snapX.push(pBounds[0]);
            }
            //pRight
            if(p != self.selectedObject &&
              (
                Math.abs(pBounds[2] - bounds[2]) < DIAGRAMO.SNAP_RADIUS ||
                Math.abs(pBounds[2] - bounds[0]) < DIAGRAMO.SNAP_RADIUS
              )
            ){
                snapX.push(pBounds[2]);
            }
            //pCenterX
            if(p != self.selectedObject && Math.abs(pCenter.x - center.x) < DIAGRAMO.SNAP_RADIUS){
                snapX.push(pCenter.x);
            }
          });
        }
        this.lastMove = [event.x, event.y];
        this.connectorManagerMouseMoveHandler(event);
        if(this.redraw){
            this.repaint();
        }
    },
    connectorManagerMouseDownHandler: function(event){
        if(this.selectedObject && this.selectedObject.isConnector && (this.connectors.indexOf(this.selectedObject) == -1 || this.selectedObject.isNotFinished)){
            var self = this;
            var connector;
            var conConPoint;
            if(this.connectors.indexOf(this.selectedObject) == -1){
                var end = new DIAGRAMO.Point(event.x, event.y);
                var start = new DIAGRAMO.Point(event.x, event.y);
                var func = window;
                var command = new DIAGRAMO.COMMANDS.ConnectorCreateCommand(this, this.selectedObject, start, end);
                command.execute();

                this.figureConnectionPointsSetVisible(false);
                connector = command.connector;
                //this.figureConnectionPointsSetVisible(true, connector.getConnectionPoints()[1]);
                conConPoint = connector.getConnectionPoints()[0];
                this.getConnectionPoints().some(function(figConPoint){
                    if(!conConPoint.trigger("beforeConnect",[figConPoint]) && figConPoint.near(event.x, event.y, DIAGRAMO.PROPERTIES.SNAP_RADIUS)){
                        self.makeGlue(figConPoint, conConPoint);
                        conConPoint.point.x = figConPoint.point.x;
                        conConPoint.point.y = figConPoint.point.y;
                        stop = true;
                        return true;
                    }
                });
            }
            return true;
        }
    },
    connectorManagerMouseMoveHandler: function(event){
        var self = this;
        if(this.selectedObject && self.handleManager.dragging != null && self.handleManager.dragging.isConnectionPoint){
            this.dragging = self.handleManager.dragging;
            if(self.handleManager.dragging.glue){
                self.handleManager.dragging.glue.figureConPoint.glues = _.without(self.handleManager.dragging.glue.figureConPoint.glues, self.handleManager.dragging.glue);
                self.handleManager.dragging.glue = null;
            }
            this.getConnectionPoints().forEach(function(conPoint){
                if(conPoint.connectionPointType == DIAGRAMO.ConnectionPoint.TYPE_CONNECTOR){
                  return;
                }
                if(!self.dragging.trigger("beforeConnect",[conPoint]) && conPoint.near(event.x, event.y, DIAGRAMO.PROPERTIES.SNAP_RADIUS)){
                    self.handleManager.dragging.glue = new DIAGRAMO.Glue(conPoint, self.dragging);
                    self.handleManager.dragging.glue.isTemp = true;
                    //glue must be made before the action
                    self.handleManager.dragging.action([self.handleManager.dragging.point.x, self.handleManager.dragging.point.y], conPoint.point.x, conPoint.point.y);
                }
                else if(!self.dragging.trigger("beforeConnect",[conPoint]) && conPoint.near(event.x, event.y, DIAGRAMO.PROPERTIES.CLOUD_RADIUS)){
                    conPoint.highlight();
                }
                else{
                    conPoint.unhighlight();
                }
            });
        }
        else{
            this.getConnectionPoints().forEach(function(conPoint){
                conPoint.unhighlight();
            });
        }
    },
    connectorManagerMouseUpHandler: function(event){
        var self = this;

        if(self.dragging != null){
            this.getConnectionPoints().forEach(function(conPoint){
                if(!self.dragging.trigger("beforeConnect", [conPoint])){
                    if(conPoint.point.near(self.dragging.point.x, self.dragging.point.y, 1) || self.dragging.point.near(conPoint.point.x, conPoint.point.y, 1)){
                        self.makeGlue(conPoint, self.dragging);
                    }
                }
            });
        }
        if(this.selectedObject == null || (this.selectedObject && !this.selectedObject.isNotFinished)){
            this.getConnectionPoints().forEach(function(conPoint){
            });
            this.dragging = null;
        }
        if(this.selectedObject != null && this.selectedObject.isNotFinished){
            var stop = false;
            var conConPoint = this.selectedObject.getConnectionPoints();
            conConPoint = conConPoint[conConPoint.length - 1];
            this.getConnectionPoints().some(function(figConPoint){
                if(!conConPoint.trigger("beforeConnect",[figConPoint]) && figConPoint.near(event.x, event.y, DIAGRAMO.PROPERTIES.SNAP_RADIUS)){
                    self.makeGlue(figConPoint, conConPoint);
                    /* this breaks MagicFigureConnectionPoint on multi point connectors (specifically blocker).
                    conConPoint.point.x = figConPoint.point.x;
                    conConPoint.point.y = figConPoint.point.y;*/
                    self.selectedObject.isNotFinished = false;
                    self.isMouseDown = false;
                    stop = true;
                    return true;
                }
            });
            if(!stop){
              this.selectedObject.addPoint(new DIAGRAMO.Point(event.x, event.y));
              this.selectedObject.handles = _.without(this.selectedObject.handles, [conConPoint]);
              this.selectedObject.buildFromPoints(
                this.selectedObject.mainItem.points || this.selectedObject.mainItem.getPoints(null),
                this.selectedObject.getConnectionPoints()
              );
              this.setupConnector(this.selectedObject);
              conConPoint = this.selectedObject.getConnectionPoints();
              conConPoint = conConPoint[conConPoint.length - 1];
              this.handleManager.dragging = this.dragging = conConPoint;
              return true;
            }
        }
    },
    destroyGroup: function(group){
        var self = this;
        this.removePaintable(group);
        group.groups.forEach(function(tg){
          paintables = _.filter(group.paintables, function(p){return tg.ids.indexOf(p.id) != -1;});
          group.paintables = _.difference(group.paintables, paintables);
          group.paintables.push(tg.group);
        });
        group.paintables.forEach(function(paintable){
            self.addPaintable(paintable);
        });
    },
    createGroup: function(figures){
        var self = this;
        var group = new DIAGRAMO.Group();
        group.noRecurse = true;
        figures.forEach(function(figure){
            if(figure.isGroup){
                group.groups.push({
                  group: figure,
                  ids: figure.paintables.map(function(p){return p.id;})
                });
                figure.paintables.forEach(function(f2){
                    group.addPrimitive(f2);
                });
            }
            else{
                group.addPrimitive(figure);
            }
            self.removePaintable(figure);
            //NOTE: addFigure(group) removes the paintables, and we need to ensure no double adding occurs through recursion
           //self.removePaintable(figure);
           if(figure.isConnector){
             self.connectors = _.without(self.connectors, figure);
           }
        });
        group.finalise();
        return group;
    },
    selectObject: function(obj){
      var component = !_.contains(this.paintables, obj);
        if(!component && this.shiftPressed && obj != this.selectedObject && this.selectedObject != null){
            obj = this.createGroup([obj, this.selectedObject]);
            this.addFigure(obj);
        }
        else if(!component && obj != this.selectedObject && this.selectedObject && this.selectedObject.isGroup && this.selectedObject.isTemp){
            this.destroyGroup(this.selectedObject);
        }
        if(component){
          return this.selectComponent(obj);
        }
        else if(obj.isFigure){
          return this.selectFigure(obj);
        }
        else{
          return this.selectConnector(obj);
        }
    },
    selectComponent: function(component){
      if(component.parent && component.parent.isFigure){
        this.selectFigure(component.parent);
      }
      else if(component.parent && component.parent.isConnector){
        this.selectConnector(component.parent);
      }
      this.setupEditor(component);
      return true;
    },
    selectFigure: function(figure){
        this.handleManager.setShape(figure);
        this.repaint();
        this.isMouseDown = true;
        this.selectedObject = figure;
        this.setupEditor(figure);
        return true;
    },
    selectConnector: function(connector){
        this.handleManager.setShape(connector);
        this.repaint();
        this.isMouseDown = true;
        this.selectedObject = connector;

        this.setupEditor(connector);
        return true;
    },
    makeGlue: function(figureConPoint, connectorConPoint){
        var add = true;
        var self = this;
        this.glues.some(function(glue){
            if(glue.figureConPoint == figureConPoint && glue.connectorConPoint == connectorConPoint && glue.connectorConPoint.glue == glue){
                add = false;
                return true;
            }
            else if(glue.figureConPoint == figureConPoint && glue.connectorConPoint == connectorConPoint){
                self.glues = _.without(self.glues, glue);
            }
        });
        if(add && !connectorConPoint.trigger("beforeConnect",[figureConPoint]) && !figureConPoint.trigger("beforeConnect",[connectorConPoint])){
            var glue = new DIAGRAMO.Glue(figureConPoint, connectorConPoint, false);
            glue.dateAdded = new Date().getTime();
            this.glues.push(glue);
            connectorConPoint.glue = glue;
            figureConPoint.glues.push(glue);
            connectorConPoint.parent.activeHandle = connectorConPoint;
            connectorConPoint.parent.trigger("afterTransform", [DIAGRAMO.Matrix.IDENTITY]);
            connectorConPoint.parent.activeHandle = null;
            connectorConPoint.parent.trigger("afterConnect");
            figureConPoint.parent.trigger("afterConnect");
        }
    },
    removeGlue: function(glue){
        var self = this;
        glue.figureConPoint.glues = _.without(glue.figureConPoint.parent.glues,glue);
        glue.connectorConPoint.glue = null;
        this.glues.some(function(aGlue, index){
            if(aGlue == glue){
                self.glues.splice(index, 1);
                return true;
            }
        });
    },
    transformConnectionPoints: function(parent, matrix, transformConnector){
        var self = this;
        var modified = [];
        if(transformConnector){
            parent.getGlues().forEach(function(glue){
               if(glue.figureConPoint.parent == parent && glue.connectorConPoint.glue == glue){
                   glue.transform(matrix);
                   if(modified.indexOf(glue.connectorConPoint.parent) == -1){
                       modified.push(glue.connectorConPoint);
                   }
               }
            });
        }
        var use = parent.connectionPoints ? parent.connectionPoints : this.connectionPoints;
        use.forEach(function(conPoint){
           if(conPoint.parent == parent){
               conPoint.transform(matrix);
           }
        });
        modified.forEach(function(connectorConPoint){
            var connector = connectorConPoint.parent;
            connector.activeHandle = connectorConPoint;
            connector.trigger("afterTransform", [matrix]);
            //this should really be a different trigger
            connector.trigger("afterConnect");
            connector.activeHandle = null;
        })
    },

    connectorMouseDown: function(e){
      var self = this;
        e.stopPropagation();
        e.bubbles = false;
        e.preventDefault = true;
        self.figureConnectionPointsSetVisible(false);
        _.keys(DIAGRAMO.editors).forEach(function(name){
            var connector = new DIAGRAMO.CONNECTORS[e.target.id](new DIAGRAMO.Point(0, 0), new DIAGRAMO.Point(0, 0));
            self.setupConnector(connector);
            self.selectConnector(connector);
            DIAGRAMO.propertyEditor($("#editor")[0], null);
            if(!DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler){
              DIAGRAMO.COMMANDS.ConnectorCreateCommand.MouseDownHandler = self.canvas.onmousedown;
            }
            self.canvas.onmousedown = function(e){
                e.stopPropagation();
                e.bubbles = false;
                e.preventDefault = true;
                self.mouseDownHandler({x: e.offsetX / self.scale, y: e.offsetY / self.scale});
            }
            //self.figureConnectionPointsSetVisible(true, connector.getConnectionPoints()[0]);
            self.repaint();
            self.handleManager.dragging = connector.getConnectionPoints()[0];
        })
    },
    showConnectors: function(connectors, connectorsDiv){
      var i = 0;
      var self = this;
      this.availableConnectors = connectors;
      connectors.forEach(function(connector){
          if(connector.key == "SpaceConnector"){
            var dummy = $("<div>");
            dummy.css("display","inline-block");
            dummy.css("width", "20px");
            dummy.css("height", "20px");
            dummy.addClass("line-btn");
            $(connectorsDiv).append(dummy);
            return;
          }
          var a = $("<a>");
          i++;
          a.attr("href", "javascript: nothing();");
          a.attr("class","line-btn");
          var title = connector.key.replace("Connector","").replace("Arrow","").replace("Blocker","").replace("GapFill","");
          title = title.replace("MultiPoint", "Multi-point").replace("Organic","Curve");
          if(connector.KeyCodes){
            title = title + " (Shift + " + _.invert(DIAGRAMO.KEYS)[connector.KeyCodes[0]] + ")"
          }
          a.attr("title",title);
          var img = $("<img>");
          img.attr("src",DIAGRAMO.PROPERTIES.ImagePath + connector.img);
          img.attr("id", connector.key);
          img.click(function(e){
              e.stopPropagation();
              e.bubbles = false;
              e.preventDefault = true;
          });
          img.mousedown(
            function(e){
              self.connectorMouseDown.call(self, e)
            }
          );
          a.append(img);
          $(connectorsDiv).append(a);
      });
    },
    shapeDrop: function(shape, e, preCheck){
      var self = this;
      DIAGRAMO.dragging = new DIAGRAMO.draggableImage(e.target.src, e, function(event){
        if(preCheck && !preCheck()){
          DIAGRAMO.dragging.destroy();
          DIAGRAMO.dragging = null;
          return;
        }
        var obj = self.canvasGetXY(event.pageX, event.pageY);
        if(obj.x < 0 || obj.y < 0 || obj.x > self.canvas.width || obj.y > self.canvas.height){
            DIAGRAMO.dragging.destroy();
            DIAGRAMO.dragging = null;
            return;
        }
        if(shape instanceof Function){
          shape = new shape(obj.x, obj.y);
        }
        var center = shape.getMiddle().clone();
        //we need the afterblah triggers that addFigure, etc setup.
        if(shape.isGroup){
          var idMap = {};
          shape.paintables.forEach(function(p){
            idMap[p.id] = p;
            p.id = null;
            if(p.isConnector){
              p.getConnectionPoints().forEach(function(c){
                idMap[c.id] = c;
                c.id = null;
              });
            }
            else if(p.isFigure){
              p.connectionPoints.forEach(function(c){
                idMap[c.id] = c;
                c.id = null;
              });
            }
          });
          shape.paintables.forEach(function(p){
            if(p.isConnector){
              self.addConnector(p);
            }
            else if(p.isFigure){
              self.addFigure(p);
            }
            self.removePaintable(p);
          });
          if(shape.glues){
            shape.glues.forEach(function(g){
              self.makeGlue(idMap[g.figureConPointId], idMap[g.connectorConPointId]);
            });
          }
        }
        var skipSelect = self.addFigure(shape);
        if(!skipSelect){
          self.selectFigure(shape);
        }
        shape.transform(DIAGRAMO.Matrix.translationMatrix(obj.x - center.x, obj.y - center.y));

        var cmd = new DIAGRAMO.COMMANDS.BringToFrontCommand(self, shape);
        cmd.execute();
        self.isMouseDown = false;
        self.repaint();
        DIAGRAMO.dragging.destroy();
        DIAGRAMO.dragging = null;
      });
    },
    showShapes: function(shapes, figuresDiv, preCheck){
      var self = this;
      shapes.forEach(function(shape){
          var img = $("<img>");
          img.attr("src", DIAGRAMO.PROPERTIES.ImagePath + "/shapes/" + shape.img);
          img.attr("id",shape.id);
          img.mousedown(function(e){
              var parts = e.target.id.split("_");
              var shape = DIAGRAMO.SETS[parts[0]][parts[1]];
              self.shapeDrop(shape, e, preCheck);
          });

          var span = $("<span>");
          span.append(img);
          $(figuresDiv).append(span);
      });
    }
});
