DIAGRAMO.CustomFigureViewer = function(editor, customFigureProvider, DOMObject){
    var header = $(".h2", $(DOMObject));
    header.css("position","relative");

    var figuresElement = $("#custom-figures", $(DOMObject));
    var plusmin = $("<i>");
    plusmin.addClass("fa fa-plus");
    if(figuresElement.size() == 0){
        var figuresElement = document.createElement("div");
        figuresElement.id = "custom-figures";
        DOMObject.appendChild(figuresElement);
        figuresElement = $(figuresElement);
    }
    figuresElement.css("display","none");

    var self = this;
    customFigureProvider.getCustomFigures(function(figures){
        figuresElement.html("");
        header.html("Favorites (" + figures.length + ")");
        header.append(plusmin);
        plusmin.click(function(){
            if(plusmin.hasClass("fa-plus")){
                figuresElement.css("display","block");
                plusmin.removeClass("fa-plus");
                plusmin.addClass("fa-minus");
            }
            else{
                figuresElement.css("display","none");
                plusmin.addClass("fa-plus");
                plusmin.removeClass("fa-minus");
            }
        })
        figures.forEach(function(figure){
            var div = figure.getElement(editor, customFigureProvider);
            figure.element = div;
            figuresElement.append(div);
        });
    });
}
