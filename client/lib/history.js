"use strict";

/**
 * An facade to add Commands, undo and redo them.
 * It keeps a STACK of commands and can trigger undo actions in the system.
 * @this {History} 
 * @constructor
 * @author Zack Newsham zack_newsham@yahoo.co.uk
 * @author Alex <alex@scriptoid.com>
 * 
 * Interesting implementation in Java
 * @see http://download.oracle.com/javase/6/docs/api/javax/swing/undo/UndoManager.html
 */
function History(editor){
    this.editor = editor;
    
    /**Object is a figure*/
    this.OBJECT_FIGURE = 0; 

    /**Object is a connector*/
    this.OBJECT_CONNECTOR = 1;

    /**Object is a connection point*/
    this.OBJECT_CONNECTION_POINT = 2;

    /**Object is a generic object*/
    this.OBJECT_STATIC = 3;
    

    /**Object is a group
     *@deprecated
     **/
    this.OBJECT_GROUP = 4;

    /**Object is a glue*/
    this.OBJECT_GLUE = 5;

    /**Where the {Array} or commands is stored*/
    this.COMMANDS = [];

    /**The current command inde within the vector of undoable objects. At that position there will be a Command*/
    this.CURRENT_POINTER = -1;
}
DIAGRAMO.History = History;


History.prototype = {
    /* Add an action to the STACK of undoable actions.
     * We position at current pointer, remove everything after it and then add the new
     * action
     * @param {Command} command -  the command History must store
     */
    addUndo: function(command){
        if(true){
            /**As we are now positioned on CURRENT_POINTER(where current Command is stored) we will
             *delete anything after it, add new Command and increase CURRENT_POINTER
             **/

            //remove commands after current command 
            this.COMMANDS.splice(this.CURRENT_POINTER +1, this.COMMANDS.length);

            //add new command 
            this.COMMANDS.push(command);

            //increase the current pointer
            this.CURRENT_POINTER++;
        }
    },

    /**Undo current command
     *TODO: nice to compress/merge some actions like many Translate in a row
     **/
    undo: function(){
        if(this.CURRENT_POINTER >= 0){
            Log.info('undo()->Type of action: ' + this.COMMANDS[this.CURRENT_POINTER].oType);
            this.COMMANDS[this.CURRENT_POINTER].undo();

            this.CURRENT_POINTER --;
        }
    },

    /**Redo a command
     *TODO: nice to compress/merge some actions like many Translate in a row
     **/
    redo: function(){
        if(this.CURRENT_POINTER + 1 < this.COMMANDS.length){
            this.COMMANDS[this.CURRENT_POINTER + 1].execute();

            this.CURRENT_POINTER++;
        }
    },

    /**Pack identical commands into a single, equivalend command.
     *It will pack only consecutive and same type commands (until a TurningPointCommand is founded)
     **/
    pack: function(){
        //TODO: implement
    }
};


