DIAGRAMO.propertyEditor = function(DOMObject, editor, object, extra){
    if(!DOMObject || (object && object.isFigureHandle)){
      return;
    }
    DIAGRAMO.propertyEditor.object = object;
    DOMObject.innerHTML = "";
    if(object && object.properties){
        object.properties.forEach(function(property){
            property.generateCode.call(property, editor, DOMObject);
        });
        if(extra){
            extra.forEach(function(property){
                property.figure = object;
                property.generateCode.call(property, editor, DOMObject);
            });
        }
    }

    setTimeout(function() {
     $("#full-editor-wrapper").find('textarea').focus();
    }, 0);
}

DIAGRAMO.propertyEditor.keyDownHandler = function(event){
  var ret = true;
  if(!DIAGRAMO.propertyEditor.object){
    return;
  }
  DIAGRAMO.propertyEditor.object.properties.some(function(p){
    if(p.keyDownHandler && p.keyDownHandler(event) === false){
      ret = false;
      return false;
    }
  });
  return ret;
}

DIAGRAMO.propertyEditor.positionPropertiesPanel = function(DOMObject, shape){
    var scale = $("#a").width() / origWidth;
    var bounds = shape.getBounds();
    bounds[0] *= scale;
    bounds[1] *= scale;
    bounds[2] *= scale;
    bounds[3] *= scale;
    var canvas = document.getElementById("a");
    var curDisplay = DOMObject.parentElement.style.display;
    DOMObject.parentElement.style.display = "block";
    DOMObject.parentElement.style.left = ((canvas.offsetLeft + bounds[0] + ((bounds[2] - bounds[0]) / 2) - DOMObject.offsetWidth / 2) )+ "px";
    var below = (bounds[1] + ((bounds[3] - bounds[1])) + 3) + DOMObject.parentElement.offsetHeight < canvas.offsetHeight;
    if(below) {
        DOMObject.parentElement.children[DOMObject.parentElement.children.length - 1].style.display="none";
        DOMObject.parentElement.children[0].style.display="block";
        DOMObject.parentElement.style.top = ((canvas.offsetTop + bounds[1] + ((bounds[3] - bounds[1])) + 3))+ "px";
    }
    else{
        DOMObject.parentElement.children[0].style.display="none";
        DOMObject.parentElement.children[DOMObject.parentElement.children.length - 1].style.display="block";
        DOMObject.parentElement.style.top = (((canvas.offsetTop + bounds[1]) - DOMObject.parentElement.offsetHeight - 3))+ "px";
    }
    DOMObject.parentElement.style.display = curDisplay;
}
