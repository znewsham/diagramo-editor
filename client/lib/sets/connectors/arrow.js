DIAGRAMO.SETS.connectors.Arrow = function(x, y, style, angle){
    DIAGRAMO.Figure.call(this, DIAGRAMO.PROPERTIES.STYLE_ARROW);
    if(_.isUndefined(style)){
        style = this.style;
    }
    if(_.isUndefined(angle)){
        angle = 0;
    }
    this.style = style;
    var startPoint = new DIAGRAMO.Point(x,y);
    var line = new DIAGRAMO.Line(startPoint.clone(),DIAGRAMO.Util.getEndPoint(startPoint,DIAGRAMO.PROPERTIES.ARROW_SIZE, Math.PI/180*DIAGRAMO.PROPERTIES.ARROW_ANGLE));
    var line1 = new DIAGRAMO.Line(startPoint.clone(),DIAGRAMO.Util.getEndPoint(startPoint,DIAGRAMO.PROPERTIES.ARROW_SIZE, Math.PI/180*-DIAGRAMO.PROPERTIES.ARROW_ANGLE));

    var path = new DIAGRAMO.Path();

    path.addPrimitive(line);
    path.addPrimitive(line1);


    this.addPrimitive(path);

    this.finalise();
    this.transform(DIAGRAMO.Matrix.translationMatrix(-x, -y));
    this.transform(DIAGRAMO.Matrix.rotationMatrix(angle));
    this.transform(DIAGRAMO.Matrix.translationMatrix(x, y));
}
DIAGRAMO.SETS.connectors.Arrow.img="square.png"

DIAGRAMO.SETS.connectors.Arrow.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.connectors.Arrow(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.connectors.Arrow.init = function(){
    _.extend(DIAGRAMO.SETS.connectors.Arrow.prototype, DIAGRAMO.Figure.prototype, {

    });
}
