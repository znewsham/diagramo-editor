if(DIAGRAMO.SETS.connectors == undefined){DIAGRAMO.SETS.connectors = {
    init: function(){
        _.keys(DIAGRAMO.SETS.connectors).forEach(function(shape){
            if(shape != "init" && shape != "properties"){
                DIAGRAMO.SETS.connectors[shape].init();
            }
        });
    },
    properties: {
        hidden: true,
        name: "Connector Styles"
    }
};}
