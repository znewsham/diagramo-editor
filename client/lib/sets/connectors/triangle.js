DIAGRAMO.SETS.connectors.Triangle = function(x, y, style, angle){
    DIAGRAMO.Figure.call(this, DIAGRAMO.PROPERTIES.STYLE_FILLED_TRIANGLE);
    if(_.isUndefined(style)){
        style = this.style;
    }
    if(_.isUndefined(angle)){
        angle = 0;
    }
    this.style = style;

    var startPoint = new DIAGRAMO.Point(x,y + DIAGRAMO.PROPERTIES.ARROW_SIZE);
    var point2 = DIAGRAMO.Util.getEndPoint(startPoint,DIAGRAMO.PROPERTIES.ARROW_SIZE, Math.PI/180*DIAGRAMO.PROPERTIES.ARROW_ANGLE);
    var point3 = DIAGRAMO.Util.getEndPoint(startPoint, DIAGRAMO.PROPERTIES.ARROW_SIZE, - Math.PI/180*DIAGRAMO.PROPERTIES.ARROW_ANGLE);

    var tri = new DIAGRAMO.Polygon();
    tri.addPoint(startPoint);
    tri.addPoint(point2);
    tri.addPoint(point3);

    this.addPrimitive(tri);

    this.finalise();
    this.transform(DIAGRAMO.Matrix.translationMatrix(-x, -y));
    this.transform(DIAGRAMO.Matrix.rotationMatrix(angle));
    this.transform(DIAGRAMO.Matrix.translationMatrix(x, y));
}
DIAGRAMO.SETS.connectors.Triangle.img="square.png"

DIAGRAMO.SETS.connectors.Triangle.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.connectors.Triangle(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.connectors.Triangle.init = function(){
    _.extend(DIAGRAMO.SETS.connectors.Triangle.prototype, DIAGRAMO.Figure.prototype, {
        paint: function(context){
            if(this.trigger("beforePaint")){
                return;
            }

            if(this.paintDelegate != null){
                this.paintDelegate.style = this.style;
                this.paintDelegate.paint(context);
            }
            else{
                if(this.style){
                    this.style.setupContext(context);
                }

                var oldStyle = this.paintables[0].style.clone();
                this.paintables[0].style.merge(this.style);
                this.paintables[0].style.fillStyle = this.style.strokeStyle;
                this.paintables[0].paint(context);
                this.paintables[0].style = oldStyle;
            }
        }
    });
}
