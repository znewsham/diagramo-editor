DIAGRAMO.SETS.basic.Hexagon = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Hexagon");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var r = new DIAGRAMO.Polygon();
    var l = DIAGRAMO.FigureDefaults.segmentShortSize / 4 * 3 + 2;
    r.addPoint(new DIAGRAMO.Point(x, y + l));
    r.addPoint(new DIAGRAMO.Point(x + l, y + l / 2));//(l * (Math.sqrt(3 / 2)))
    r.addPoint(new DIAGRAMO.Point(x + l, y - l / 2));//(l * (Math.sqrt(3 / 2)))
    r.addPoint(new DIAGRAMO.Point(x, y - l));
    r.addPoint(new DIAGRAMO.Point((x - l), y - l / 2));//(l * (Math.sqrt(3 / 2)))
    r.addPoint(new DIAGRAMO.Point((x - l), y + l / 2));//(l * (Math.sqrt(3 / 2)))

    this.addPrimitive(r);

    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x,y), DIAGRAMO.generateId()));

    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x,y + l), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + l, y + l / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + l, y - l / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y - l), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - l, y - l / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - l, y + l / 2), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}



DIAGRAMO.SETS.basic.Hexagon.img="hexagon.png"

DIAGRAMO.SETS.basic.Hexagon.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Hexagon(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Hexagon.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Hexagon.prototype, DIAGRAMO.Figure.prototype, {

    });
}
