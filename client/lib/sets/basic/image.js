DIAGRAMO.SETS.basic.Image = function(x, y, skipTranslate){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Image");
    var self = this;

    this.backgroundImage = new DIAGRAMO.Image(new DIAGRAMO.Point(x, y), "");
    var onload = this.backgroundImage.image.onload;
    this.backgroundImage.image.onload = function(){
      onload();
      if(self.rotationCoords.length == 2 && self.rotationCoords[0].equals(self.rotationCoords[1])){
        self.finalise();
      }
      if(!skipTranslate){
        var bounds = self.getBounds();
        var center = DIAGRAMO.Util.getCenter(bounds);
        self.transform(DIAGRAMO.Matrix.translationMatrix(DIAGRAMO.PROPERTIES.MAX_IMAGE_WIDTH / 2 - center.x, DIAGRAMO.PROPERTIES.MAX_IMAGE_HEIGHT / 2 - center.y));
      }
    }
    this.properties = [];
    this.properties.push(new DIAGRAMO.PROPERTIES.ImageUploadProperty(this, "Image", "image"));
    this.addPrimitive(this.backgroundImage);
}
DIAGRAMO.SETS.basic.Image.img="square.png"

DIAGRAMO.SETS.basic.Image.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Image(0, 0, true);

    square = DIAGRAMO.Figure.load(jsonObject, square);
    square.backgroundImage = square.paintables[0];

    if(square.rotationCoords.length == 2 && square.rotationCoords[0].equals(square.rotationCoords[1])){
      square.finalise();
    }
    return square;
}

DIAGRAMO.SETS.basic.Image.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Image.prototype, DIAGRAMO.Figure.prototype, {
        setImage: function(src){
          this.backgroundImage.ready = false;
          this.backgroundImage.image.src = DIAGRAMO.PROPERTIES.ImagePath + src;
        },
        getImage: function(){
          return this.backgroundImage.image.src;
        }
    });
}
