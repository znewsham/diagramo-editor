if(DIAGRAMO.SETS.basic == undefined){
  DIAGRAMO.SETS.basic = {
    init: function(){
        _.keys(DIAGRAMO.SETS.basic).forEach(function(shape){
            if(shape != "init" && shape != "properties"){
                DIAGRAMO.SETS.basic[shape].init();
            }
        });
    },
    properties: {
        hidden: true,
        name: "Basic"
    }
};}
