DIAGRAMO.SETS.basic.Square = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Square");

    var r = new DIAGRAMO.Polygon();
    r.addPoint(new DIAGRAMO.Point(x, y));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize, y));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize, y + DIAGRAMO.FigureDefaults.segmentSize));
    r.addPoint(new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize));
    this.addPrimitive(r);

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;


    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2, y + DIAGRAMO.FigureDefaults.segmentSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize, y + DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}
DIAGRAMO.SETS.basic.Square.img="square.png"

DIAGRAMO.SETS.basic.Square.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Square(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Square.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Square.prototype, DIAGRAMO.Figure.prototype, {

    });
}
