DIAGRAMO.SETS.basic.Octogon = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Octogon");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var r = new DIAGRAMO.Polygon();

    var l = DIAGRAMO.FigureDefaults.segmentShortSize / 3 * 2;
    var a = l / Math.sqrt(2);
    r.addPoint(new DIAGRAMO.Point(x, y));
    r.addPoint(new DIAGRAMO.Point(x + l, y));
    r.addPoint(new DIAGRAMO.Point(x + l + a, y + a));
    r.addPoint(new DIAGRAMO.Point(x + l + a, y + a + l));
    r.addPoint(new DIAGRAMO.Point(x + l, y + a + l + a));
    r.addPoint(new DIAGRAMO.Point(x, y + a + l + a));
    r.addPoint(new DIAGRAMO.Point(x - a, y + a + l));
    r.addPoint(new DIAGRAMO.Point(x - a, y + a));

    this.addPrimitive(r);

    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x,y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + l, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + l + a, y + a), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + l + a, y + a + l), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + l, y + a + l + a), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + a + l + a), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - a, y + a + l), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - a, y + a), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + l / 2, y + (l + a + a) / 2), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}



DIAGRAMO.SETS.basic.Octogon.img="octogon.png"

DIAGRAMO.SETS.basic.Octogon.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Octogon(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Octogon.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Octogon.prototype, DIAGRAMO.Figure.prototype, {

    });
}
