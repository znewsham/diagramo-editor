
DIAGRAMO.SETS.basic.Text = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Text");
    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    this.isText = true;

    var t2 = new DIAGRAMO.Text(DIAGRAMO.FigureDefaults.textStr, x, y + DIAGRAMO.FigureDefaults.radiusSize / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, DIAGRAMO.Text.ALIGN_CENTER);
    t2.style.fillStyle = DIAGRAMO.FigureDefaults.textColor;
    this.addPrimitive(t2);
    this.finalise();

    //lets set up our connection points to be right on the bounds
    var bounds = this.getBounds();
    var width = bounds[2] - bounds[0];
    var height = bounds[3] - bounds[1];

    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(bounds[0] + width / 2, bounds[1] - 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(bounds[0] + width / 2, bounds[3] + 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(bounds[0] - 2, bounds[1] + height / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(bounds[0] + width + 2, bounds[1] + height / 2), DIAGRAMO.generateId()));

    //this.properties.push(new DIAGRAMO.PROPERTIES.TextProperty(this, "_text.str", "Text"));

    this._text = this.paintables[0];

    var self = this;

    function resize(property, value){
      var x = self.rotationCoords[0].x;
      var y = self.rotationCoords[0].y;
      var angle = DIAGRAMO.Util.getAngle(self.rotationCoords[0], self.rotationCoords[1]);
      //rotate it to 0
      self.transform(DIAGRAMO.Matrix.translationMatrix(-x, -y));
      self.transform(DIAGRAMO.Matrix.rotationMatrix(-angle));
      self.transform(DIAGRAMO.Matrix.translationMatrix(x, y));
      var oldBounds = self.getBounds();
      self._text[property] = value;
      var newBounds = self.getBounds();
      var matrix;
      if(this.align == DIAGRAMO.Text.ALIGN_LEFT){//scale from left
          matrix = DIAGRAMO.Matrix.mergeTransformations(
              DIAGRAMO.Matrix.translationMatrix(-oldBounds[0], 0),
              DIAGRAMO.Matrix.scaleMatrix(((newBounds[2] - newBounds[0]) + 4) / ((oldBounds[2] - oldBounds[0]) + 4), 1),
              DIAGRAMO.Matrix.translationMatrix(oldBounds[0], 0)
          )
      }
      else if(this.align == DIAGRAMO.Text.ALIGN_RIGHT){//scale from right
          matrix = DIAGRAMO.Matrix.mergeTransformations(
              DIAGRAMO.Matrix.translationMatrix(-newBounds[2], 0),
              DIAGRAMO.Matrix.scaleMatrix(((newBounds[2] - newBounds[0]) / 2 + 2) / ((oldBounds[2] - oldBounds[0]) / 2 + 2), 1),
              DIAGRAMO.Matrix.translationMatrix(newBounds[2], 0)
          )
      }
      else{//scale from middle
          matrix = DIAGRAMO.Matrix.mergeTransformations(
              DIAGRAMO.Matrix.translationMatrix(-(newBounds[0] + (newBounds[2] - newBounds[0]) / 2), 0),
              DIAGRAMO.Matrix.scaleMatrix(((newBounds[2] - newBounds[0])  + 4) / ((oldBounds[2] - oldBounds[0])  + 4), 1),
              DIAGRAMO.Matrix.translationMatrix((newBounds[0] + (newBounds[2] - newBounds[0]) / 2), 0)
          )
      }
      self.trigger("afterTransform",[matrix, true]);
      self.transform(DIAGRAMO.Matrix.translationMatrix(-x, -y));
      self.transform(DIAGRAMO.Matrix.rotationMatrix(angle));
      self.transform(DIAGRAMO.Matrix.translationMatrix(x, y));
    }
    this._text.getStr = function(){
        return this.str;
    }
    this._text.getSize = function(){
        return this.size;
    }
    this._text.setStr = function(str){
      resize("str", str);
    }
    this._text.setSize = function(size){
      resize("size", size);
    }
}

DIAGRAMO.SETS.basic.Text.img="text.png"

DIAGRAMO.SETS.basic.Text.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Text(0, 0);

    var tmp = square._text;
    square = DIAGRAMO.Figure.load(jsonObject, square);
    square._text.getStr = tmp.getStr;
    square._text.setStr = tmp.setStr;
    square._text.getSize = tmp.getSize;
    square._text.setSize = tmp.setSize;
    return square;
}

DIAGRAMO.SETS.basic.Text.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Text.prototype, DIAGRAMO.Figure.prototype, {
        clone: function(){
          var ret = DIAGRAMO.Figure.prototype.clone.call(this);
          ret._text = ret.paintables[0];
          return ret;
        },
        getBounds: function(){
          if(this._text){
            return this._text.getBounds();
          }
          else{
            return [0,0,0,0];
          }
        }
    });
}
