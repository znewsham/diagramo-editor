DIAGRAMO.SETS.basic.Ellipse = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Ellipse");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var c = new DIAGRAMO.Ellipse(new DIAGRAMO.Point(x, y), DIAGRAMO.FigureDefaults.segmentShortSize, DIAGRAMO.FigureDefaults.segmentShortSize / 2);

    this.addPrimitive(c);


    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentShortSize, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentShortSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - DIAGRAMO.FigureDefaults.segmentShortSize, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y - DIAGRAMO.FigureDefaults.segmentShortSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y), DIAGRAMO.generateId()));

        var bounds = this.getBounds();
    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);;
    this.finalise();
}


DIAGRAMO.SETS.basic.Ellipse.img="ellipse.png"

DIAGRAMO.SETS.basic.Ellipse.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Ellipse(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Ellipse.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Ellipse.prototype, DIAGRAMO.Figure.prototype, {

    });
}
