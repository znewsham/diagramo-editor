DIAGRAMO.SETS.basic.Rectangle = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Rectangle");

    var r = new DIAGRAMO.Polygon();
    r.addPoint(new DIAGRAMO.Point(x, y));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize * 1.5, y));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize * 1.5, y + DIAGRAMO.FigureDefaults.segmentSize));
    r.addPoint(new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize));
    this.addPrimitive(r);

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;


    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + (DIAGRAMO.FigureDefaults.segmentSize * 1.5) / 2, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + (DIAGRAMO.FigureDefaults.segmentSize * 1.5) / 2, y + DIAGRAMO.FigureDefaults.segmentSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + (DIAGRAMO.FigureDefaults.segmentSize * 1.5), y + DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));


    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    text.style.fillStyle = DIAGRAMO.FigureDefaults.textColor;
    this.addPrimitive(text);
    this.finalise();
    var bounds = this.getBounds();
}
DIAGRAMO.SETS.basic.Rectangle.img="rectangle.png"

DIAGRAMO.SETS.basic.Rectangle.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Rectangle(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Rectangle.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Rectangle.prototype, DIAGRAMO.Figure.prototype, {

    });
}
