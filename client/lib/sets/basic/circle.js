DIAGRAMO.SETS.basic.Circle = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Circle");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;

    var c = new DIAGRAMO.Arc(x, y, DIAGRAMO.FigureDefaults.radiusSize, 0, 360, false, 0);

    this.addPrimitive(c);

    var text = new DIAGRAMO.Text("", x, y, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);

    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + (DIAGRAMO.FigureDefaults.radiusSize), y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - (DIAGRAMO.FigureDefaults.radiusSize), y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.radiusSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x , y - DIAGRAMO.FigureDefaults.radiusSize), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}

DIAGRAMO.SETS.basic.Circle.img="circle.png"

DIAGRAMO.SETS.basic.Circle.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Circle(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Circle.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Circle.prototype, DIAGRAMO.Figure.prototype, {
      toJSONObject: function(){
        var obj = DIAGRAMO.Figure.prototype.toJSONObject.call(this);
        obj.isBall = this.isBall;
        return obj;
      }
    });
}
