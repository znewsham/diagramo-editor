DIAGRAMO.SETS.basic.Pentagon = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Pentagon");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var r = new DIAGRAMO.Polygon();
    var l = DIAGRAMO.FigureDefaults.radiusSize;
    for (var i = 0; i < 5; i++) {
        r.addPoint(new DIAGRAMO.Point(x - l * Math.sin(2 * Math.PI * i / 5), y - l * Math.cos(2 * Math.PI * i / 5)));
    }
    this.addPrimitive(r);
    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x,y), DIAGRAMO.generateId()));

    for (var i = 0; i < 5; i++) {
        this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - l * Math.sin(2 * Math.PI * i / 5), y - l * Math.cos(2 * Math.PI * i / 5)), DIAGRAMO.generateId()));
    }

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}


DIAGRAMO.SETS.basic.Pentagon.img="pentagon.png"

DIAGRAMO.SETS.basic.Pentagon.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Pentagon(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Pentagon.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Pentagon.prototype, DIAGRAMO.Figure.prototype, {

    });
}
