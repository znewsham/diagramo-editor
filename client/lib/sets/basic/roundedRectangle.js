DIAGRAMO.SETS.basic.RoundedRectangle = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.RoundedRectangle");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var p = new DIAGRAMO.Path();
    var hShrinker = 10;
    var vShrinker = 6;
    var l1 = new DIAGRAMO.Line(new DIAGRAMO.Point(x + hShrinker, y + vShrinker),
            new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker, y + vShrinker));

    var c1 = new DIAGRAMO.QuadCurve(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker, y + vShrinker),
            new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker + DIAGRAMO.FigureDefaults.corner * (DIAGRAMO.FigureDefaults.cornerRoundness / 10), y + DIAGRAMO.FigureDefaults.corner / DIAGRAMO.FigureDefaults.cornerRoundness + vShrinker),
            new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker + DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + vShrinker))

    var l2 = new DIAGRAMO.Line(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker + DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + vShrinker),
            new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker + DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker));

    var c2 = new DIAGRAMO.QuadCurve(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker + DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker),
            new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker + DIAGRAMO.FigureDefaults.corner * (DIAGRAMO.FigureDefaults.cornerRoundness / 10), y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker + DIAGRAMO.FigureDefaults.corner * (DIAGRAMO.FigureDefaults.cornerRoundness / 10)),
            new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker + DIAGRAMO.FigureDefaults.corner))

    var l3 = new DIAGRAMO.Line(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize - hShrinker, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker + DIAGRAMO.FigureDefaults.corner),
            new DIAGRAMO.Point(x + hShrinker, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker + DIAGRAMO.FigureDefaults.corner));

    var c3 = new DIAGRAMO.QuadCurve(
            new DIAGRAMO.Point(x + hShrinker, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker + DIAGRAMO.FigureDefaults.corner),
            new DIAGRAMO.Point(x + hShrinker - DIAGRAMO.FigureDefaults.corner * (DIAGRAMO.FigureDefaults.cornerRoundness / 10), y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker + DIAGRAMO.FigureDefaults.corner * (DIAGRAMO.FigureDefaults.cornerRoundness / 10)),
            new DIAGRAMO.Point(x + hShrinker - DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker))

    var l4 = new DIAGRAMO.Line(new DIAGRAMO.Point(x + hShrinker - DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker),
            new DIAGRAMO.Point(x + hShrinker - DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + vShrinker));

    var c4 = new DIAGRAMO.QuadCurve(
            new DIAGRAMO.Point(x + hShrinker - DIAGRAMO.FigureDefaults.corner, y + DIAGRAMO.FigureDefaults.corner + vShrinker),
            new DIAGRAMO.Point(x + hShrinker - DIAGRAMO.FigureDefaults.corner * (DIAGRAMO.FigureDefaults.cornerRoundness / 10), y + vShrinker),
            new DIAGRAMO.Point(x + hShrinker, y + vShrinker))

    p.addPrimitive(l1);
    p.addPrimitive(c1);
    p.addPrimitive(l2);
    p.addPrimitive(c2);
    p.addPrimitive(l3);
    p.addPrimitive(c3);
    p.addPrimitive(l4);
    p.addPrimitive(c4);

    this.addPrimitive(p);

    var wid = DIAGRAMO.FigureDefaults.segmentSize - hShrinker + DIAGRAMO.FigureDefaults.corner;
    var height = DIAGRAMO.FigureDefaults.corner + DIAGRAMO.FigureDefaults.segmentShortSize - vShrinker + DIAGRAMO.FigureDefaults.corner;

    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid / 2 - 10, y + vShrinker), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid / 2, y + vShrinker), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid / 2 + 10, y + vShrinker), DIAGRAMO.generateId()));

    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid, y + height / 2 - 10), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid, y + height / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid, y + height / 2 + 10), DIAGRAMO.generateId()));

    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid / 2 - 10, y + height), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid / 2, y + height), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + wid / 2 + 10, y + height), DIAGRAMO.generateId()));

    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + height / 2 - 10), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + height / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + height / 2 + 10), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();

}
DIAGRAMO.SETS.basic.RoundedRectangle.img="rounded_rectangle.png"

DIAGRAMO.SETS.basic.RoundedRectangle.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.RoundedRectangle(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}


DIAGRAMO.SETS.basic.RoundedRectangle.init = function(){
    _.extend(DIAGRAMO.SETS.basic.RoundedRectangle.prototype, DIAGRAMO.Figure.prototype, {

    });
}
