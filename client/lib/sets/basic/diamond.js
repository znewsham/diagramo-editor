DIAGRAMO.SETS.basic.Diamond = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Diamond");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var r = new DIAGRAMO.Polygon();
    r.addPoint(new DIAGRAMO.Point(x, y - DIAGRAMO.FigureDefaults.segmentSize / 2));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentShortSize / 3 * 2, y));
    r.addPoint(new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize / 2));
    r.addPoint(new DIAGRAMO.Point(x - DIAGRAMO.FigureDefaults.segmentShortSize / 3 * 2, y));
    this.addPrimitive(r);


    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y - DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentShortSize / 3 * 2, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x - DIAGRAMO.FigureDefaults.segmentShortSize / 3 * 2, y), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}


DIAGRAMO.SETS.basic.Diamond.img="diamond.png"

DIAGRAMO.SETS.basic.Diamond.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Diamond(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Diamond.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Diamond.prototype, DIAGRAMO.Figure.prototype, {

    });
}
