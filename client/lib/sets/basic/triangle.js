DIAGRAMO.SETS.basic.Triangle = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Triangle");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var t = new DIAGRAMO.Polygon();
    t.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2, y));
    t.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize, y + DIAGRAMO.FigureDefaults.segmentSize));
    t.addPoint(new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize));
    this.addPrimitive(t);

    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize, y + DIAGRAMO.FigureDefaults.segmentSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x, y + DIAGRAMO.FigureDefaults.segmentSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2, y + DIAGRAMO.FigureDefaults.segmentSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2, y + DIAGRAMO.FigureDefaults.segmentSize / 2 + 5), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2 + DIAGRAMO.FigureDefaults.segmentSize / 4, y + DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 4, y + DIAGRAMO.FigureDefaults.segmentSize / 2), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}



DIAGRAMO.SETS.basic.Triangle.img="triangle.png"

DIAGRAMO.SETS.basic.Triangle.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Triangle(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}


DIAGRAMO.SETS.basic.Triangle.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Triangle.prototype, DIAGRAMO.Figure.prototype, {

    });
}
