DIAGRAMO.SETS.basic.Parallelogram = function(x, y){
    DIAGRAMO.Figure.call(this, "DIAGRAMO.SETS.basic.Parallelogram");

    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    var r = new DIAGRAMO.Polygon();
    r.addPoint(new DIAGRAMO.Point(x + 10, y));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize + 10, y));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize + DIAGRAMO.FigureDefaults.parallelsOffsetSize, y + DIAGRAMO.FigureDefaults.segmentShortSize));
    r.addPoint(new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.parallelsOffsetSize, y + DIAGRAMO.FigureDefaults.segmentShortSize));

    this.addPrimitive(r);

    this.connectionPoints = [];
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + 10, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize + 10, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize + DIAGRAMO.FigureDefaults.parallelsOffsetSize / 2 + 5, y + DIAGRAMO.FigureDefaults.segmentShortSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.parallelsOffsetSize, y + DIAGRAMO.FigureDefaults.segmentShortSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize + DIAGRAMO.FigureDefaults.parallelsOffsetSize / 2 + 5, y + DIAGRAMO.FigureDefaults.segmentShortSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.parallelsOffsetSize / 2 + 5, y + DIAGRAMO.FigureDefaults.segmentShortSize / 2), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2 + DIAGRAMO.FigureDefaults.parallelsOffsetSize, y + DIAGRAMO.FigureDefaults.segmentShortSize), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2 + 10, y), DIAGRAMO.generateId()));
    this.connectionPoints.push(new DIAGRAMO.FigureConnectionPoint(this, new DIAGRAMO.Point(x + DIAGRAMO.FigureDefaults.segmentSize / 2 + DIAGRAMO.FigureDefaults.parallelsOffsetSize / 2 + 2, y + DIAGRAMO.FigureDefaults.segmentShortSize / 2), DIAGRAMO.generateId()));

    var bounds = this.getBounds();
    var text = new DIAGRAMO.Text("", bounds[0] + (bounds[2] - bounds[0]) / 2, bounds[1] + (bounds[3] - bounds[1]) / 2, DIAGRAMO.FigureDefaults.textFont, DIAGRAMO.FigureDefaults.textSize, false, "center");
    this.addPrimitive(text);
    this.finalise();
}


DIAGRAMO.SETS.basic.Parallelogram.img="parallelogram.png"

DIAGRAMO.SETS.basic.Parallelogram.load = function(jsonObject){
    var square = new DIAGRAMO.SETS.basic.Parallelogram(0, 0);

    square = DIAGRAMO.Figure.load(jsonObject, square);

    return square;
}

DIAGRAMO.SETS.basic.Parallelogram.init = function(){
    _.extend(DIAGRAMO.SETS.basic.Parallelogram.prototype, DIAGRAMO.Figure.prototype, {

    });
}
