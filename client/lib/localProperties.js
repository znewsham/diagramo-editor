

/**Object with default values for figures*/
DIAGRAMO.FigureDefaults = {
    /**Size of figure's segment*/
    segmentSize : 33,

    /**Size of figure's short segment*/
    segmentShortSize : 22,

    /**Size of radius*/
    radiusSize : 12,

    /**Size of offset for parallels
    * For example: for parallelogram it's projection of inclined line on X axis*/
    parallelsOffsetSize : 20,

    /**Corner radius
    * For example: for rounded rectangle*/
    corner : 7,

    /**Corner roundness
    * Value from 0 to 10, where 10 - it's circle radius.*/
    cornerRoundness : 5,

    /**Color of lines*/
    strokeStyle : "#000000",

    /**Color of fill*/
    fillStyle : "#ffffff",

    /**Text size*/
    textSize : 16,

    /**Text label*/
    textStr : "",

    /**Text font*/
    textFont : "Arial",

    /**Color of text*/
    textColor : "#000000",

    lineWidth: "1"
};

DIAGRAMO.PROPERTIES.GRID_SIZE = 20;
DIAGRAMO.PROPERTIES.CLOUD_RADIUS = 15;
DIAGRAMO.PROPERTIES.SNAP_RADIUS = 10;
DIAGRAMO.PROPERTIES.CLOUD_LINEWIDTH = 3;
DIAGRAMO.PROPERTIES.CLOUD_STROKE_STYLE = "rgba(255, 153, 0, 0.8)"; //orange

DIAGRAMO.PROPERTIES.STYLE_ARROW = "DIAGRAMO.SETS.connectors.Arrow";
DIAGRAMO.PROPERTIES.STYLE_NORMAL = "Normal";
DIAGRAMO.PROPERTIES.STYLE_FILLED_TRIANGLE = "DIAGRAMO.SETS.connectors.Triangle";

DIAGRAMO.PROPERTIES.ARROW_SIZE = 12;
DIAGRAMO.PROPERTIES.ARROW_ANGLE = 15;

DIAGRAMO.PROPERTIES.EXTRA = function(object, editor){
    if(object == null){
        return [];
    }
    if(object.isFigure){
        return [
            new DIAGRAMO.PROPERTIES.MultiProperty(null, null, [
            new DIAGRAMO.PROPERTIES.CommandProperty(null, null, "FlipH", "/assets/flip-horiz-white.png",
                new DIAGRAMO.COMMANDS.FigureFlipCommand(object, DIAGRAMO.Matrix.flipHorizontalMatrix(), DIAGRAMO.Matrix.flipHorizontalMatrix())
            ),
            new DIAGRAMO.PROPERTIES.CommandProperty(null, null, "FlipV", "/assets/flip-vert-white.png",
                new DIAGRAMO.COMMANDS.FigureFlipCommand(object, DIAGRAMO.Matrix.flipVerticalMatrix(), DIAGRAMO.Matrix.flipVerticalMatrix())
            ),
            new DIAGRAMO.PROPERTIES.CommandProperty(null, null, "back", "/assets/send-back.png",
              new DIAGRAMO.COMMANDS.SendToBackCommand(editor, object, DIAGRAMO.Matrix.flipVerticalMatrix(), DIAGRAMO.Matrix.flipVerticalMatrix())
            ),
            new DIAGRAMO.PROPERTIES.CommandProperty(null, null, "front", "/assets/bring-front.png",
              new DIAGRAMO.COMMANDS.BringToFrontCommand(editor, object, DIAGRAMO.Matrix.flipVerticalMatrix(), DIAGRAMO.Matrix.flipVerticalMatrix())
            )], {width: "100%"})
        ];
    }
    else{
      return [];
    }
}

DIAGRAMO.PROPERTIES.FILL_THIRDS = [
    {Text: "None", Value: -1, Image: "/assets/fillThirds-none.png"},
    {Text: "Left", Value: 0, Image: "/assets/fillThirds-left.png"},
    {Text: "Straight-up", Value: 1, Image: "/assets/fillThirds-center.png"},
    {Text: "Right", Value: 2, Image: "/assets/fillThirds-right.png"}
];
DIAGRAMO.PROPERTIES.LINE_WIDTHS = [
    {Text: '1px', Value: '1'},{Text: '2px',Value: '2'},{Text: '3px',Value: '3'},
    {Text: '4px',Value: '4'},{Text: '5px',Value: '5'},{Text: '6px',Value: '6'},
    {Text: '7px',Value: '7'},{Text: '8px',Value: '8'},{Text: '9px',Value: '9'},
    {Text: '10px',Value: '10'},
    {Text: '11',Value: '11'},
    {Text: '12',Value: '12'},
    {Text: '13',Value: '13'},
    {Text: '14',Value: '14'},
    {Text: '15',Value: '15'},
    {Text: '16',Value: '16'},
    {Text: '17',Value: '17'},
    {Text: '18',Value: '18'},
    {Text: '19',Value: '19'},
    {Text: '20',Value: '20'}
]
DIAGRAMO.PROPERTIES.TEXT_SIZES = [
    {Text: '1', Value: '1'},{Text: '2',Value: '2'},{Text: '3',Value: '3'},
    {Text: '4',Value: '4'},{Text: '5',Value: '5'},{Text: '6',Value: '6'},
    {Text: '7',Value: '7'},{Text: '8',Value: '8'},{Text: '9',Value: '9'},
    {Text: '10',Value: '10'},
    {Text: '11',Value: '11'},
    {Text: '12',Value: '12'},
    {Text: '13',Value: '13'},
    {Text: '14',Value: '14'},
    {Text: '15',Value: '15'},
    {Text: '16',Value: '16'},
    {Text: '17',Value: '17'},
    {Text: '18',Value: '18'},
    {Text: '19',Value: '19'},
    {Text: '20',Value: '20'}
]

DIAGRAMO.PROPERTIES.CONNECTOR_STYLES = [
    {Text: "None", Value: DIAGRAMO.PROPERTIES.STYLE_NORMAL, FigureFunction: null},
    {Text: "Arrow", Value: DIAGRAMO.PROPERTIES.STYLE_ARROW, FigureFunction: "DIAGRAMO.SETS.connectors.Arrow"},
    {Text: "Triangle", Value: DIAGRAMO.PROPERTIES.STYLE_FILLED_TRIANGLE, FigureFunction: "DIAGRAMO.SETS.connectors.Triangle"},
];



/**Line styles*/
DIAGRAMO.PROPERTIES.CONNECTOR_LINE_STYLES = [
    {Text: 'Solid', Value: 'continuous'},
    {Text: 'Dotted', Value: 'dotted'},
    {Text: 'Dashed',Value: 'dashed'},
    {Text: 'Squiggly',Value: 'squiggly', delegate: "SquigglyLineDelegate"}
];
DIAGRAMO.PROPERTIES.FIGURE_LINE_STYLES = [
    {Text: 'Solid', Value: 'continuous'},
    {Text: 'Dotted', Value: 'dotted'},
    {Text: 'Dashed',Value: 'dashed'}
];

DIAGRAMO.FIGURE_ESCAPE_DISTANCE = 30;
