/**
 * Created by zacknewsham on 11/21/15.
 */


function SquigglyLineDelegate(item, startIndex, pointIndexes){
    this.style = null;
    this.squiggleLength = 10;
    this.squiggleWidth = 10;
    this.startIndex = startIndex;
    this.pointIndexes = pointIndexes;
    this.item = item;
}

SquigglyLineDelegate.MIN_SQUIGGLE_LENGTH = 8;
SquigglyLineDelegate.MAX_SQUIGGLE_LENGTH = 12;

DIAGRAMO.SquigglyLineDelegate = SquigglyLineDelegate;

DIAGRAMO.SquigglyLineDelegate.load = function(obj){
  return new SquigglyLineDelegate(obj.item, obj.startIndex, obj.pointIndexes);
}

SquigglyLineDelegate.prototype = {
    constructor: SquigglyLineDelegate,
    toJSONObject: function(){
      var ret = {
        oType: "DIAGRAMO.SquigglyLineDelegate",
        startIndex: this.startIndex,
        pointIndexes: null,
        item: (this.item.paintDelegate && (this.item.paintDelegate.paintables[0] == this)) ? null : this.item.toJSONObject()
      };

      return ret;
    },
    paint: function(context){
        var points = this.item.getPoints();
        var origPoints = null;
        if(this.item instanceof DIAGRAMO.Polyline){
          origPoints = points;
          points = DIAGRAMO.Point.cloneArray(points);
          var distance = DIAGRAMO.Util.distance(points[points.length - 1], points[points.length - 2]);
          var pNew = DIAGRAMO.Util.getEndPoint(points[points.length - 2], distance - 10, DIAGRAMO.Util.getAngle(points[points.length - 2], points[points.length - 1]));
          points[points.length - 1] = pNew;
        }
        var fragments = [];
        if(this.item instanceof DIAGRAMO.NURBS){
            var pointsLength = points.length / this.item.fragments.length;
            var points = [];
            this.item.fragments.forEach(function(curve){
                fragments.push(DIAGRAMO.Util.equidistancePoints(curve, 200));
            });
        }
        else if(this.item instanceof DIAGRAMO.Polyline){
            for(var i = 1; i < points.length; i++){
                fragments.push([points[i - 1], points[i]]);
            }
        }
        else if(this.item instanceof DIAGRAMO.Line){
            fragments.push(this.item.getPoints());
        }
        for(var f = 0; f < fragments.length; f++){
            var points = fragments[f];
            var squiggles = 0;
            var STEP = 0.1;
            var useStart = false;
            var bestSquiggles = -1;
            var bestA = 0;
            var startPoint = points[0];
            var distance = DIAGRAMO.Util.distance(points[0], points[1]);
            var inc = 1;
            while(distance < SquigglyLineDelegate.MIN_SQUIGGLE_LENGTH && inc < points.length){
                distance = DIAGRAMO.Util.distance(points[0], points[inc]);
                inc++;
            }
            for(var a = SquigglyLineDelegate.MIN_SQUIGGLE_LENGTH; a < SquigglyLineDelegate.MAX_SQUIGGLE_LENGTH; a += STEP){
                squiggles = distance / a;
                var diff = Math.abs(Math.floor(squiggles) - squiggles);
                if(diff < bestSquiggles || bestSquiggles == -1){
                    bestA = a;
                    bestSquiggles = diff;
                }
            }
            squiggles = distance / bestA;
            var doit = Math.max(0, Math.min(inc, (points.length - 0) - 1));
            if(doit <= 0){
              return;
            }
            for(var i = 0; i < points.length; i += doit){
                context.beginPath();
                var point = points[i];
                if(DIAGRAMO.Util.distance(point, startPoint) < SquigglyLineDelegate.MIN_SQUIGGLE_LENGTH){

                    //continue;
                }
                for(var s = 0; s < squiggles-1; s++){
                    var start = DIAGRAMO.Util.point_on_segment(startPoint, point, s * bestA);
                    var end = DIAGRAMO.Util.point_on_segment(startPoint, point, (s + 1) * bestA);
                    var mid = DIAGRAMO.Util.point_on_segment(startPoint, point, (s + 0.5) * bestA);
                    end.style.lineWidth = 1;
                    var line1 = new DIAGRAMO.Line(DIAGRAMO.Util.point_on_segment(mid, end, -this.squiggleWidth), DIAGRAMO.Util.point_on_segment(mid, end, this.squiggleWidth));
                    var mid1 = DIAGRAMO.Util.getMiddle(line1.startPoint, line1.endPoint);
                    line1.transform(DIAGRAMO.Matrix.translationMatrix(-mid1.x, -mid1.y));
                    line1.transform(DIAGRAMO.Matrix.rotationMatrix(90 * (Math.PI/180)));
                    line1.transform(DIAGRAMO.Matrix.translationMatrix(mid1.x, mid1.y));
                    var control1 = useStart ? line1.startPoint : line1.endPoint;

                    var curve = new DIAGRAMO.QuadCurve(start, control1, end);
                    curve.style = null;
                    curve.paint(context);
                    useStart = !useStart;
                    if(i >= points.length - 1){
                        i = points.length;
                    }
                }
                startPoint = point;
                context.lineTo(point.x, point.y);
                context.stroke();
                doit = Math.max(0, Math.min(inc, (points.length - 0) - 1));
                if(doit <= 0){
                  return;
                }
            }
            if(origPoints && f == fragments.length - 1){
              context.beginPath();
              context.moveTo(points[points.length - 1].x, points[points.length - 1].y)
              context.lineTo(origPoints[origPoints.length - 1].x, origPoints[origPoints.length - 1].y)
              context.stroke();
            }
        }
    }
}
