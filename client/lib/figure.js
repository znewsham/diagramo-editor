/**A figure is simply a collection of basic paintables: DIAGRAMO.Points, DIAGRAMO.Lines, DIAGRAMO.Arcs, Curves and Paths
 * A figure should not care about grouping paintables into Paths, each shape should draw itself.
 * The Figure only delegate the painting to the composing shape.
 *
 * @constructor
 * @this {Figure}
 * @param {String} name - the name of the figure
 *
 **/
function Figure(name) {
    DIAGRAMO.Primitive.call(this);
    /**Figure's name*/
    this.name = name;
    this.activeHandle = null;

    /**the Group'id to which this figure belongs to*/
    this.groupId = -1;

    /*Keeps track of all the handles for a figure*/
    //this.handles = [];
    //this.handleSelectedIndex=-1;
    //this.builder = new Builder(this.id);

    /**An {Array} of {@link BuilderProperty} objects*/
    this.properties = [];

    /**The {@link DIAGRAMO.Style} use to draw this figure*/
    this.style = new DIAGRAMO.Style();
    this.style.fillStyle = DIAGRAMO.FigureDefaults.fillStyle;
    this.style.strokeStyle = DIAGRAMO.FigureDefaults.strokeStyle;
    this.style.lineWidth = DIAGRAMO.FigureDefaults.lineWidth;
    this.style.gradientBounds = this.getBounds();

    /**We keep the figure position by having different points
     *[central point of the figure, the middle of upper edge]
     * An {Array} or {@link DIAGRAMO.Point}s
     **/
    this.rotationCoords = [];


    /**A {String} that point to a location*/
    this.url = '';

    /**Object type used for JSON deserialization*/
    this.oType = 'Figure';
    this.isFigure = true;
    this.properties = [];
    this.properties.push(new DIAGRAMO.PROPERTIES.TextProperty(this, "_text.str", "Mid Text", {multiline: true}));
    this.properties.push(new DIAGRAMO.PROPERTIES.MultiProperty(this, "Text Style", [
        new DIAGRAMO.PROPERTIES.SelectProperty(this, "_text.size", "", DIAGRAMO.PROPERTIES.TEXT_SIZES, {width: "75px", marginRight: "5px"}),
        new DIAGRAMO.PROPERTIES.ColorProperty(this, "_text.style.fillStyle", "")
    ]));
    this.properties.push(new DIAGRAMO.PROPERTIES.MultiProperty(this, "Border & Fill", [
        new DIAGRAMO.PROPERTIES.SelectProperty(this, "style.lineWidth", "Line Width", DIAGRAMO.PROPERTIES.LINE_WIDTHS, {width: "75px", marginRight: "5px"}),
        new DIAGRAMO.PROPERTIES.ColorProperty(this, "style.fillStyle", "Fill Color")
    ]));
    this.properties.push(new DIAGRAMO.PROPERTIES.MultiProperty(this, "Border Style", [
        new DIAGRAMO.PROPERTIES.SelectProperty(this, "style.lineStyle", "Line Width", DIAGRAMO.PROPERTIES.FIGURE_LINE_STYLES, {width: "75px", marginRight: "5px"}),
        new DIAGRAMO.PROPERTIES.ColorProperty(this, "style.strokeStyle", "Line Color")
    ]));


    this.keyDown(function(event){
      if(event.keyCode == DIAGRAMO.KEYS.UP){
        this.transform(DIAGRAMO.Matrix.translationMatrix(0, -1));
      }
      else if(event.keyCode == DIAGRAMO.KEYS.DOWN){
        this.transform(DIAGRAMO.Matrix.translationMatrix(0, 1));
      }
      else if(event.keyCode == DIAGRAMO.KEYS.LEFT){
        this.transform(DIAGRAMO.Matrix.translationMatrix(-1, 0));
      }
      else if(event.keyCode == DIAGRAMO.KEYS.RIGHT){
        this.transform(DIAGRAMO.Matrix.translationMatrix(1, 0));
      }
      else{
        return false;
      }
      return true;
    })
    this.mouseDown(this.mouseDownHandler);
    this.connectionPoints = [];
}
DIAGRAMO.Figure = Figure;

/**Creates a new {Figure} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {Figure} a newly constructed Figure
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
Figure.load = function(jsonObject, figure){
    _.keys(jsonObject).forEach(function(key){
      if(key.substring(0, 2) == 'is'){
        figure[key] = jsonObject[key];
      }
    });
    figure.visible = jsonObject.visible;
    figure.id = jsonObject.id;
    figure.paintables = [];
    figure.style = DIAGRAMO.Style.load(jsonObject.style);
    figure.zIndex = jsonObject.zIndex;
    jsonObject.paintables.forEach(function(paintable){
        figure.paintables.push(DIAGRAMO.load(paintable));
    });
    figure.connectionPoints = [];
    jsonObject.connectionPoints.forEach(function(conPoint){
        var cp = DIAGRAMO.load(conPoint);
        cp.parent = figure;
        figure.connectionPoints.push(cp);
    });

    figure.rotationCoords = [];
    figure.rotationCoords.push(DIAGRAMO.Point.load(jsonObject.rotationCoords[0]));
    figure.rotationCoords.push(DIAGRAMO.Point.load(jsonObject.rotationCoords[1]));
    figure._text = _.findWhere(figure.paintables, {oType: "Text"});
    if(!figure._text){
        figure.properties = _.reject(figure.properties, function(prop){
            if(!prop.accessors){
                var ret = false;
                prop.properties.forEach(function(prop2){
                    if(prop2.accessors.indexOf("_text") == 0){
                        ret = true;
                    }
                });
                return ret;
            }
            return  prop.accessors.indexOf("_text") == 0;
        })
    }
    return figure;
}
/**Creates a new {Array} of {Figure}s out of JSON parsed object
 *@param {JSONObject} v - the JSON parsed object
 *@return {Array} of newly constructed {Figure}s
 *@author Alex Gheorghiu <alex@scriptoid.com>
 *@author Janis Sejans <janis.sejans@towntech.lv>
 **/
Figure.loadArray = function(v){
    var newFigures = [];

    for(var i=0; i<v.length; i++){
        newFigures.push(Figure.load(v[i]));
    }

    return newFigures;
}

Figure.init = function(){
    _.extend(Figure.prototype, DIAGRAMO.Primitive.prototype, {
      getCursor: function(x, y){
        return "crosshair";
      },
        toPNG: function(){
          var canvas = document.createElement("canvas");
          var bounds = this.getBounds();
          canvas.width = (bounds[2] - bounds[0]) + 2;
          canvas.height = (bounds[3] - bounds[1]) + 2;

          this.transform(DIAGRAMO.Matrix.translationMatrix((-bounds[0]) + 1, (-bounds[1]) +1));
          this.paint(canvas.getContext("2d"));
          this.transform(DIAGRAMO.Matrix.translationMatrix(bounds[0] - 1, bounds[1] - 1));
          return canvas.toDataURL();
        },
        getConnectionPoints: function(){
            return this.connectionPoints;
        },
        getGlues: function(){
            var ret = [];
            this.connectionPoints.forEach(function(conPoint){
                ret = ret.concat(conPoint.glues);
            });
            return ret;
        },
        mouseDownHandler: function(event){
            var ret = undefined;
            this.connectionPoints.some(function(conPoint){
                if(conPoint.contains(event.x, event.y)){
                    ret = conPoint.trigger("mouseDown", event);
                    return ret;
                }
            });
            return ret;
        },
        mouseOutHandler: function(event){

        },
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Primitive.prototype.toJSONObject.call(this), {
                oType: this.name,
                rotationCoords: DIAGRAMO.Point.toJSONArray(this.rotationCoords),
                connectionPoints: [],
                version: DIAGRAMO.VERSION
            });
            this.connectionPoints.forEach(function(connectionPoint){
                ret.connectionPoints.push(connectionPoint.toJSONObject());
            });
            return ret;
        },

        //@param{bool} transformConnector - should we transform the connector? Used when we transform a figure,
        //without redrawing it.
        transform:function(matrix, transformConnector, transformText){
            this.trigger("beforeTransform", [matrix, transformConnector, transformText]);
            if(transformConnector == "undefined" || transformConnector == undefined){
                transformConnector = true;
            }
            if(transformText == "undefined" || transformText == undefined){
                transformText = 3;
            }
            //transform all composing paintables
            for(var i = 0; i<this.paintables.length; i++ ){
                if(this.paintables[i] instanceof DIAGRAMO.Text && (transformText & 1) == 1){
                    this.paintables[i].transform(matrix);
                }
                if(!(this.paintables[i] instanceof DIAGRAMO.Text) && (transformText & 2) == 2){
                    this.paintables[i].transform(matrix);
                }
            }

            //transform the style
            this.style.transform(matrix);

            //some figures don't have rotation coords, i.e. those that aren't "real" figures, such as the highlight rectangle
            if(this.rotationCoords.length!=0){
                this.rotationCoords[0].transform(matrix);
                this.rotationCoords[1].transform(matrix);
            }

            this.trigger("afterTransform", [matrix, transformConnector, transformText]);
        },

        getPoints:function(){
            var points = [];
            for (var i=0; i<this.paintables.length; i++){
                points = points.concat(this.paintables[i].getPoints()); //add all primitive's points in a single pass
            }
            return points;
        },

        paint: function(context){
            DIAGRAMO.Primitive.prototype.paint.call(this, context);
            this.connectionPoints.forEach(function(conPoint){
                if(conPoint.isVisible() && context.paintLater){
                  context.paintLater.push(conPoint);
                    //conPoint.paint(context);
                }
            });
        },

        addPrimitive:function(primitive){
            // add id property to primitive equal its index
            //primitive.id = this.paintables.length;

            this.paintables.push(primitive);

            // update bound coordinates for gradient
            this.style.gradientBounds = this.getBounds();
        },

        //no more points to add, so create the handles and selectRect
        finalise:function(){
            this._text = _.findWhere(this.paintables, {oType: "Text"});
            if(!this._text){
                this.properties = _.reject(this.properties, function(prop){
                    if(!prop.accessors){
                        var ret = false;
                        prop.properties.forEach(function(prop2){
                            if(prop2.accessors.indexOf("_text") == 0){
                                ret = true;
                            }
                        });
                        return ret;
                    }
                    return  prop.accessors.indexOf("_text") == 0;
                })
            }
            var bounds = this.getBounds();

            if(bounds == null){
                throw 'Figure bounds are null !!!';
                return;
            }
            this.rotationCoords = [0,0];
            //central point of the figure
            this.rotationCoords[0] = new DIAGRAMO.Point(
                bounds[0] + (bounds[2] - bounds[0]) / 2,
                bounds[1] + (bounds[3] - bounds[1]) / 2
            );

            //the middle of upper edge
            this.rotationCoords[1] = new DIAGRAMO.Point(this.rotationCoords[0].x, bounds[1]);
        },

        getAngle: function(){
            return DIAGRAMO.Util.getAngle(this.rotationCoords[0], this.rotationCoords[1]);
        },

        clone:function(){
            var obj = window;
            var parts = this.name.split(".");
            for(var i = 0; i < parts.length - 1; i++){
              obj = obj[parts[i]];
            }
            var ret = new obj[parts[parts.length - 1]]();

            ret.paintables = [];
            for (var i=0; i<this.paintables.length; i++){
                ret.addPrimitive(this.paintables[i].clone());
            }
            ret._text = _.find(ret.paintables, function(p){return p instanceof DIAGRAMO.Text});
            ret.style = this.style.clone();
            ret.rotationCoords[0]=this.rotationCoords[0].clone();
            ret.rotationCoords[1]=this.rotationCoords[1].clone();
            ret.url = this.url;
            ret.connectionPoints = !this.connectionPoints ? undefined : this.connectionPoints.map(function(conPoint){
               var con = conPoint.clone();
               con.id = DIAGRAMO.generateId();
               con.parent = ret;
               return con;
            });
            return ret;
        },

        /*
         *TODO: this is based on getText which is a WRONG (my or Zack's fault I think)
         *
         *apply/clone another figure style onto this figure
         *@param{Figure} anotherFigure - another figure
         *@author Janis Sejans <janis.sejans@towntech.lv>
         *TODO: From Janis: we don`t have Undo for this operation
         *@deprecated
         */
        applyAnotherFigureStyle:function(anotherFigure){
            this.style = anotherFigure.style.clone();

            var newText = this.getText(); //will contain new text object
            //TODO: From Janis: there is some problem if applying text twice, the getText returns empty string, this means it is not properly cloned
            if(newText instanceof DIAGRAMO.Text){
                var currTextStr = newText.getTextStr(); //remember text str
                var currTextVector = newText.vector; //remember text vector

                newText = anotherFigure.getText().clone();
                newText.setTextStr(currTextStr); //restore text str
                newText.vector = currTextVector; //restore text vector
                this.setText(newText);
            }
        },

        contains:function(x,y){
            var bounds = this.getBounds();
            bounds[0] -= 10;
            bounds[1] -= 10;
            bounds[2] += 10;
            bounds[3] += 10;
            if(!DIAGRAMO.Util.isPointInsideBounds(new DIAGRAMO.Point(x, y), bounds)){
                return false;
            }
            for(var i=0; i<this.paintables.length; i++){
                if(this.paintables[i].contains(x,y)){
                    return true;
                }
            }
            for(var i = 0; i < this.connectionPoints.length; i++){
              if(this.connectionPoints[i].contains(x, y, true)){
                return true;
              }
            }
            return false;
        },


        /**
         * @return {Array<Number>} - returns [minX, minY, maxX, maxY] - bounds, where
         *  all points are in the bounds.
         */
        getBounds: function(){
            var points = [];
            for (var i = 0; i < this.paintables.length; i++) {
                if(this.paintables[i].oType != "Text"){
                    var bounds = this.paintables[i].getBounds();
                    points.push(new DIAGRAMO.Point(bounds[0], bounds[1]));
                    points.push(new DIAGRAMO.Point(bounds[2], bounds[3]));
                }
            }
            return DIAGRAMO.Util.getBounds(points);
        },

        equals:function(anotherFigure){
            if(!anotherFigure instanceof Figure){
                Log.info("Figure:equals() 0");
                return false;
            }


            if(this.paintables.length == anotherFigure.paintables.length){
                for(var i=0; i<this.paintables.length; i++){
                    if(!this.paintables[i].equals(anotherFigure.paintables[i])){
                        Log.info("Figure:equals() 1");
                        return false;
                    }
                }
            }
            else{
                Log.info("Figure:equals() 2");
                return false;
            }
            //test group
            if(this.groupId != anotherFigure.groupId){
                return false;
            }

            //test rotation coords
            if(this.rotationCoords.length == anotherFigure.rotationCoords.length){
                for(var i in this.rotationCoords){
                    if(!this.rotationCoords[i].equals(anotherFigure.rotationCoords[i])){
                        return false;
                    }
                }
            }
            else{
                return false;
            }

            //test style
            if(!this.style.equals(anotherFigure.style)){
                return false;
            }

            //test url
            if(!this.url == anotherFigure.url){
                return false;
            }

            return true;
        },

        near:function(x,y,radius){
            var bounds = this.getBounds();
            bounds[0] -= radius;
            bounds[1] -= radius;
            bounds[2] += radius;
            bounds[3] += radius;
            if(!DIAGRAMO.Util.isPointInsideBounds(new DIAGRAMO.Point(x, y), bounds)){
                return false;
            }
            for(var i=0; i<this.paintables.length; i++){
                if(this.paintables[i].near(x,y,radius)){
                    return true;
                }
            }
            for(var i = 0; i < this.connectionPoints.length; i++){
              if(this.connectionPoints[i].near(x, y, radius, true)){
                return true;
              }
            }
            return false;
        },

        toString:function(){
            var result = this.name + ' [id: ' + this.id + '] (';
            for(var i = 0; i<this.paintables.length; i++ ){
                result += this.paintables[i].toString();
            }
            result += ')';
            return result;
        },
    });
}
