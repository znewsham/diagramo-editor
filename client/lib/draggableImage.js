DIAGRAMO.draggableImage = function(imgUrl, evt, onDropFunction){
    this.draggable = document.createElement('img');
    this.draggable.setAttribute('id', 'draggingThumb');
    this.draggable.style.position = 'absolute';
    this.draggable.style.zIndex = 3;  // set it in front of editor


    //Log.info("editor.php>documentOnMouseMove>STATE_FIGURE_CREATE: selectedFigureThumb=" + selectedFigureThumb);
    this.draggable.setAttribute('src', imgUrl);                        
    this.draggable.style.width = '100px';
    this.draggable.style.height = '100px';
    this.draggable.style.left = (evt.pageX - 50) + 'px';
    this.draggable.style.top = (evt.pageY - 50) + 'px';
    //draggingFigure.style.backgroundColor  = 'red';
    this.draggable.style.display  = 'block';
    document.body.appendChild(this.draggable);

    this.draggable.addEventListener('mouseup', onDropFunction);

    this.mouseMoveHandler = function (event){
        this.draggable.style.left = (event.pageX - 50) + 'px';
        this.draggable.style.top = (event.pageY - 50) + 'px';
    };
    this.destroy = function(){
        $("#draggingThumb").remove();
    }
}