function ConnectorHandle(handleType, point, modifiablePoints, connector){
    DIAGRAMO.Handle.call(this, handleType, connector);
    this.modifiablePoints = modifiablePoints;
    this.point = point;
    this.visible = true;
}
DIAGRAMO.ConnectorHandle = ConnectorHandle;
ConnectorHandle.load = function(jsonObject, connectorHandle){
    var modPoints = [];
    jsonObject.modifiablePoints.forEach(function(mp){
        modPoints.push({
            type: mp.type,
            point: DIAGRAMO.load(mp.point)
        });
    });
    if(connectorHandle == undefined){
        connectorHandle = new DIAGRAMO.ConnectorHandle(jsonObject.handleType,jsonObject.point, modPoints, null);
    }
    else{
        connectorHandle.modifiablePoints = modPoints;
    }
    connectorHandle = DIAGRAMO.Handle.load(jsonObject, connectorHandle);

    return connectorHandle;
}
ConnectorHandle.init = function(){
    _.extend(ConnectorHandle.prototype, DIAGRAMO.Handle.prototype, {
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Handle.prototype.toJSONObject.call(this), {
                modifiablePoints: [],
                point: this.point,
                oType: "DIAGRAMO.ConnectorHandle"
            });
            this.modifiablePoints.forEach(function(obj){
                ret.modifiablePoints.push({
                    type: obj.type,
                    point: obj.point.toJSONObject()
                });
            });
            return ret;
        },
        clone: function(){
            var ret = new ConnectorHandle(this.handleType, this.point.clone(), this.modifiablePoints.map(function(obj){return {type: obj.type, point: obj.point.clone()};}), this.parent);

            return ret;
        },
        getCursor:function(event){
            if(this.visible == false || this.parent.selected != true){
                return "";
            }
            if(this.handleType == 'v'){
                return 'ns-resize';
            }
            if(this.handleType == 'f'){
                return 'move';
            }
            else{
                return 'ew-resize';
            }
        },
        beforeActionHandler: function(){
          if(this.isConnectionPoint){
            return true;
          }
          else{
              var self = this;
              var conPoints = self.parent.getConnectionPoints();
              var ret = true;
              this.modifiablePoints.forEach(function(obj){
                  var conPoint = _.find(conPoints, function(conPoint){
                      return conPoint.point.x == obj.point.x && conPoint.point.y == obj.point.y;
                  });
                  if(conPoint && conPoint.glue != null){
                      ret = false;
                  }
              });
              return ret;
          }
        },
        /**
         *Handle actions for Connector
         *
         *@param {Array } lastMove - an array that will hold [x,y] of last x & y coordinates
         *@param {Number} newX - new X coordinate
         *@param {Number} newY - new Y coordinate
         **/
        action: function(lastMove, newX, newY){
            var self = this;
            this.parent.activeHandle = this;
            var ret = false;
            var pointTransformed = false;
            if(!this.trigger("beforeAction")){
                return;
            }
            this.modifiablePoints.forEach(function(point){
                self.parent.getHandles().forEach(function(handle){
                    if(handle.point.equals(point) && handle.glue != null && handle != self){
                        ret = true;
                    }
                });
            });
            switch(this.handleType){
                case 'v':
                    var deltaY = newY - lastMove[1];    //Take changes on Oy
                    var translationMatrix = DIAGRAMO.Matrix.translationMatrix(0, deltaY);    //Generate translation matrix
                    this.modifiablePoints.forEach(function(obj){
                        obj.point.transform(translationMatrix);
                        if(self.point == obj.point){
                            pointTransformed = true;
                        }
                    });
                    break;

                case 'h':
                    var deltaX = newX-lastMove[0];    //Take changes on Ox
                    var translationMatrix = DIAGRAMO.Matrix.translationMatrix(deltaX, 0);    //Generate translation matrix

                    this.modifiablePoints.forEach(function(obj){
                        obj.point.transform(translationMatrix);
                        if(self.point == obj.point){
                            pointTransformed = true;
                        }
                    });
                    break;

                case 'f':
                    var item = this.parent.mainItem;
                    if(item instanceof DIAGRAMO.Path){
                      var index = _.indexOf(this.parent.getConnectionPoints(), this);
                      if(index == this.parent.getConnectionPoints().length - 1 || index === 0){
                        item = item.paintables[index === 0 ? 0 : item.paintables.length - 1];
                      }
                    }
                    if(this.isConnectionPoint && ((item instanceof DIAGRAMO.Polyline) || (item instanceof DIAGRAMO.Line))){
                      var points = item.points;
                      //ignore complex curves
                      if(points.length < 100 && DIAGRAMO.isAltPressed()){
                        var against = null;
                        if(points[0].equals(this.point)){
                          against = points[1];
                        }
                        else{
                          against = points[points.length - 2];
                        }
                        var newPoint = new DIAGRAMO.Point(newX, newY);
                        var angle = DIAGRAMO.Util.getAngle(against, newPoint);

                        angle = angle * 180 /  Math.PI;
                        var snapAngle = (DIAGRAMO.PROPERTIES.SNAP_ANGLE || 5)
                        angle = Math.round(angle / snapAngle) * snapAngle;
                        angle = angle * Math.PI / 180;

                        var end = DIAGRAMO.Util.getEndPoint(against, DIAGRAMO.Util.getLength(against, newPoint), angle);
                        newX = end.x;
                        newY = end.y;
                      }
                    }
                    var deltaY = newY - this.point.y;    //Take changes on Oy
                    var deltaX = newX - this.point.x;    //Take changes on Oy
                    lastMove[1] = newY;
                    lastMove[0] = newX;
                    var translationMatrix = DIAGRAMO.Matrix.translationMatrix(deltaX, deltaY);    //Generate translation matrix
                    this.modifiablePoints.forEach(function(obj){
                        if(obj.type == 'f'){
                            obj.point.transform(translationMatrix);
                        }
                        else if(obj.type == 'h'){
                            obj.point.transform(DIAGRAMO.Matrix.translationMatrix(deltaX, 0));
                        }
                        else if(obj.type == 'v'){
                            obj.point.transform(DIAGRAMO.Matrix.translationMatrix(0, deltaY));
                        }
                        if(self.point == obj.point){
                            pointTransformed = true;
                        }
                    });
                    break;
            }
            if(!pointTransformed){
                this.point.transform(translationMatrix);
            }
            this.trigger("afterAction");
            this.parent.trigger("afterTransform", [translationMatrix]);
            //this.handleManager.shape.updateMiddleText();
        },
    });
};
