
/**
 *A connection point that is attached to a figure and can accept connectors
 *
 *@constructor
 *@this {ConnectionPoint}
 *@param {Connector | Figure} parent - the parent to which this ConnectionPoint is attached. It can be either a {Figure} or a {Connector}
 *@param {Point} point - coordinate of this connection point, better than using x and y, because when we move "snap to" this
 * connectionPoint the line will follow
 *@param {Number} id - unique id to the parent figure
 *@param {String} connectionPointType - the connectionPointType of the parent. It can be either 'figure' or 'connector'
 *
 *@author Zack Newsham <zack_newsham@yahoo.co.uk>
 *@author Alex Gheorghiu <alex@scriptoid.com>
 */
function FigureConnectionPoint(figure,point,id){
    DIAGRAMO.Primitive.call(this);
    DIAGRAMO.ConnectionPoint.call(this);
    /**Connection point id*/
    this.id = id;

    /**The {Point} that is behind this ConnectionPoint*/
    this.point = point.clone(); //we will create a clone so that no side effect will appear

    /**Parent id (id of the Figure or Connector)*/
    this.parent = figure;
    this.connectionPointType = DIAGRAMO.ConnectionPoint.TYPE_FIGURE;
    /**Current connection point color*/
    this.color = DIAGRAMO.ConnectionPoint.FIGURE_COLOR;

    /**Radius of the connection point*/
    this.radius = 3;

    /**Serialization connectionPointType*/
    this.oType = 'ConnectionPoint'; //object connectionPointType used for JSON deserialization

    this.isConnectionPoint = true;
    var self = this;
    this.glues = [];
    this.beforeConnect(function(other){
      return self.parent == other.parent;
    })

}
DIAGRAMO.FigureConnectionPoint = FigureConnectionPoint;


/**Creates a {ConnectionPoint} out of JSON parsed object
 *@param {JSONObject} o - the JSON parsed object
 *@return {ConnectionPoint} a newly constructed ConnectionPoint
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
FigureConnectionPoint.load = function(o, connectionPoint){
    if(connectionPoint == undefined){
        var connectionPoint = new DIAGRAMO.FigureConnectionPoint(null, DIAGRAMO.Point.load(o.point), o.id); //fake constructor
    }

    return connectionPoint;
}

/**Creates a an {Array} of {ConnectionPoint} out of JSON parsed array
 *@param {JSONObject} v - the JSON parsed {Array}
 *@return {Array} of newly loaded {ConnectionPoint}s
 *@author Alex Gheorghiu <alex@scriptoid.com>
 **/
FigureConnectionPoint.loadArray = function(v){
    var newConnectionPoints = [];

    for(var i=0; i<v.length; i++){
        newConnectionPoints.push(ConnectionPoint.load(v[i]));
    }

    return newConnectionPoints;
}

/**Clones an array of {ConnectionPoint}s
 *@param {Array} v - the array of {ConnectionPoint}s
 *@return an {Array} of {ConnectionPoint}s
 **/
FigureConnectionPoint.cloneArray = function(v){
    var newConnectionPoints = [];
    for(var i=0; i< v.length; i++){
        newConnectionPoints.push(v[i].clone());
    }
    return newConnectionPoints;
}

FigureConnectionPoint.init = function(){
    _.extend(FigureConnectionPoint.prototype, DIAGRAMO.Primitive.prototype,DIAGRAMO.ConnectionPoint.prototype,{
        toJSONObject: function(){
            var ret = _.extend({}, DIAGRAMO.Handle.prototype.toJSONObject.call(this), {
                oType: "DIAGRAMO.FigureConnectionPoint"
            });

            return ret;
        },
        clone: function(){
            var ret = new FigureConnectionPoint(this.parent, this.point.clone(), this.id);

            return ret;
        },
        //should we block the connection
        beforeConnectHandler: function(conPoint){
            return false;
        },

        /**Clone current {ConnectionPoint}
         **/
        clone: function(){
            //parentId,point,id, connectionPointType
            return new FigureConnectionPoint(this.parent, this.point.clone(), this.id, this.connectionPointType );
        },

        /**Compares to another ConnectionPoint
         *@param {ConnectionPoint} anotherConnectionPoint - the other connection point
         *@return {Boolean} - true if equals, false otherwise
         **/
        equals:function(anotherConnectionPoint){

            return this.id == anotherConnectionPoint.id
            && this.point.equals(anotherConnectionPoint.point)
            && this.parentId == anotherConnectionPoint.parentId
            && this.connectionPointType == anotherConnectionPoint.connectionPointType
            && this.color == anotherConnectionPoint.color
            && this.radius == anotherConnectionPoint.radius;
        },

        /**
         *Paints the ConnectionPoint into a Context
         *@param {Context} context - the 2D context
         **/
        paint:function(context){
            context.save();
            context.fillStyle = this.color;
            context.strokeStyle = '#000000';
            context.lineWidth = 1;
            context.beginPath();
            context.arc(this.point.x, this.point.y, DIAGRAMO.ConnectionPoint.RADIUS, 0, (Math.PI/180)*360, false);
            context.stroke();
            context.fill();
            context.restore();
        },


        /**Highlight the connection point*/
        highlight:function(){
            this.color = DIAGRAMO.ConnectionPoint.OVER_COLOR;
        },

        /**Un-highlight the connection point*/
        unhighlight:function(){
            this.color = DIAGRAMO.ConnectionPoint.FIGURE_COLOR;
        },


        /**Tests to see if a point (x, y) is within a range of current ConnectionPoint
         *@param {Numeric} x - the x coordinate of tested point
         *@param {Numeric} y - the x coordinate of tested point
         *@return {Boolean} - true if inside, false otherwise
         *@author Alex Gheorghiu <alex@scriptoid.com>
         **/
        contains:function(x, y){
            return this.near(x, y, DIAGRAMO.ConnectionPoint.RADIUS);
        },

        /**Tests to see if a point (x, y) is within a specified range of current ConnectionPoint
         *@param {Numeric} x - the x coordinate of tested point
         *@param {Numeric} y - the x coordinate of tested point
         *@param {Numeric} radius - the radius around this point
         *@return {Boolean} - true if inside, false otherwise
         *@author Alex Gheorghiu <alex@scriptoid.com>
         **/
        near:function(x, y, radius){
            return this.point.near(x,y,radius);
        },


        /**A String representation of the point*/
        toString:function(){
            return "ConnectionPoint id = " + this.id  + ' point = ['+ this.point + '] ,connectionPointType = ' + this.connectionPointType + ", parentId = " + this.parentId + ")";
        },
        transform: function(matrix){
            this.trigger("beforeTransform", [matrix]);
            this.point.transform(matrix);
        }
    });
}
