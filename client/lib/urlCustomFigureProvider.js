DIAGRAMO.URLCustomFigureProvider = function(getUrl, setUrl, editUrl, deleteUrl, setMethod, perGotFigure, perSavedFigure){
    this.getUrl = getUrl;
    this.setUrl = setUrl;
    this.editUrl = editUrl;
    this.deleteUrl = deleteUrl;
    this.setMethod = setMethod || "post";
    this.figures = [];
    this.perGotFigure = perGotFigure;
    this.perSaveFigure = perSavedFigure;
    this.figureMap = {};
}
DIAGRAMO.URLCustomFigureProvider.prototype = {
    getCustomFigures: function(callback){
        var self = this;
        self.callback = callback;
        $.ajax(this.getUrl, {
            dataType: "json",
            success: function(data){
                data.forEach(function(fig){

                    if(self.perGotFigure){
                        self.perGotFigure(fig);
                    }
                    if(!fig.is_folder && !fig.figure){
                      return;
                    }
                    if(!fig.is_folder){
                      try{
                        fig.figure = JSON.parse(fig.figure);
                      }
                      catch(e){
                        console.log(e);
                        return;
                      }
                    }
                    if(fig.is_folder){
                        self.figureMap[fig.id] = new DIAGRAMO.CustomFigureFolder(fig.id, fig.image, fig.name)
                        self.figures.push(self.figureMap[fig.id]);
                    }
                    else if(fig.parent_id == null){
                        self.figureMap[fig.id] = new DIAGRAMO.CustomFigure(fig.id, fig.figure, fig.image, fig.name);
                        self.figures.push(self.figureMap[fig.id]);
                    }
                    else if(self.figureMap[fig.parent_id]){

                        var figure = new DIAGRAMO.CustomFigure(fig.id, fig.figure, fig.image, fig.name);
                        figure.parent_id = fig.parent_id;
                        self.figureMap[fig.parent_id].figures.push(figure);
                    }
                });
                callback(self.figures);
            }
        });
    },
    editFigure: function(figure){
        var self = this;
        figure.name = prompt("Enter the new name", figure.name);
        if(figure.name != null){
          $.ajax(this.editUrl, {
              data: {
                  favouriteId: figure.id,
                  name: figure.name
              },
              success: function(data){
                  self.callback(self.figures);
              }
          });
        }
    },
    deleteFigure: function(figure){
        var ret = confirm("are you sure you want to delete: " + figure.name);
        if(!ret){
          return;
        }
        var self = this;
        $.ajax(this.deleteUrl, {
            data: {
                favouriteId:  figure.id
            },
            success: function(data){
                if(self.figureMap[figure.id]){
                    self.figures = _.without(self.figures, figure);
                    delete self.figureMap[figure.id];
                }
                else{
                    var parent = self.figureMap[figure.parent_id];
                    parent.figures = _.without(parent.figures, figure);
                }
                self.callback(self.figures);
            }
        });
    },
    saveFigure: function(figure){
        var self = this;


        var dummy = figure;

        this.perSaveFigure(dummy, function(){
            if(!figure.is_folder){
                var bounds = figure.getBounds();
                var tmpCanvas = document.createElement("canvas");
                tmpCanvas.width = bounds[2] - bounds[0];
                tmpCanvas.height = bounds[3] - bounds[1];
                tmpCanvas.getContext("2d").fillStyle="#FFffff";
                tmpCanvas.getContext("2d").fillRect(0,00,tmpCanvas.width,tmpCanvas.height);
                tmpCanvas.getContext("2d").translate(-bounds[0], -bounds[1]);
                figure.paint(tmpCanvas.getContext("2d"));
            }
            var figureJson = figure.is_folder ? "" : figure.toJSONObject();
            if(figure.isGroup){
                figureJson.glues = [];
                figure.glues.forEach(function(glue){
                    figureJson.glues.push(glue.toJSONObject());
                })
            }
            var figureImage = figure.is_folder ? "" : tmpCanvas.toDataURL("image/png");
            $.ajax(self.setUrl, {
                method: "post",
                data: {
                    favourite: {
                        figure:  JSON.stringify(figureJson),
                        image: figureImage,
                        name: dummy.saveName,
                        parent_id: dummy.parent_id,
                        is_folder: figure.is_folder ? 1 : 0
                    }
                },
                success: function(data){
                    if(figure.is_folder){
                        var customFigure = new DIAGRAMO.CustomFigureFolder(data, figure.image, dummy.saveName);
                        customFigure.parent_id = null;
                        customFigure.id = data;
                        self.figures.push(customFigure);
                        self.figureMap[data] = customFigure;
                        $("#favourite-folder").html("");
                        var opt = $("<option>");
                        opt.html("None");
                        opt.val("");
                        $("#favourite-folder").append(opt);
                        self.figures.forEach(function(fig){
                            if(fig.is_folder){
                                var opt = $("<option>");
                                opt.html(fig.name);
                                opt.val(fig.id);
                                $("#favourite-folder").append(opt);
                            }
                        });
                        $("#favourite-folder").val(data);
                    }
                    else{
                        var customFigure = new DIAGRAMO.CustomFigure(data, figureJson, figureImage, dummy.saveName);
                        customFigure.parent_id = dummy.parent_id;
                        customFigure.id = data;
                        if(customFigure.parent_id == null){
                            self.figures.push(customFigure);
                            self.figureMap[data] = customFigure;
                        }
                        else if(self.figureMap[customFigure.parent_id]){
                            self.figureMap[customFigure.parent_id].figures.push(customFigure);
                        }
                    }
                    self.callback(self.figures);
                }
            });
        });

    }
}
