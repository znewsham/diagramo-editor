DIAGRAMO.CustomFigure = function(id, jsonObject, imageSrc, name){
    this.id = id;
    this.jsonObject = jsonObject;
    this.imageSrc = imageSrc;
    this.name = name;
}

DIAGRAMO.CustomFigure.setupShape= function(shape,diagramo, group){
    shape.id = DIAGRAMO.generateId();
    if(shape.isGroup){
      shape.noRecurse = true;
        shape.paintables.forEach(function(subShape){
            DIAGRAMO.CustomFigure.setupShape(subShape,diagramo, shape);
            diagramo.removePaintable(subShape);
        });
    }

    if(shape.isFigure){
        diagramo.addFigure(shape);
        if(shape.glues){
            shape.glues.forEach(function(glue){
                var fcp,ccp;
                shape.paintables.forEach(function(fig){
                    if(fig.isFigure && !fcp){
                        fcp = _.findWhere(fig.connectionPoints, {id: glue.figureConPointId});
                    }
                    else if(fig.isConnector && !ccp){
                        ccp = _.findWhere(fig.getConnectionPoints(), {id: glue.connectorConPointId});
                    }
                });
                if(fcp && ccp){
                  var tmpAT = ccp.parent.afterTransformHandlers;
                  var tmpAC = ccp.parent.afterConnectHandlers;
                  ccp.parent.afterTransformHandlers = [];
                  ccp.parent.afterConnectHandlers = [];
                  diagramo.makeGlue(fcp, ccp);
                  ccp.parent.afterTransformHandlers = tmpAT;
                  ccp.parent.afterConnectHandlers = tmpAC
                }
            });
        }
    }
    else if(shape.isConnector){
        diagramo.addConnector(shape);
    }
    shape.getConnectionPoints().forEach(function(conPoint){
        var newId = DIAGRAMO.generateId();
        if(group && group.glues){
          var glue = _.findWhere(group.glues, {figureConPointId: conPoint.id});
          if(glue){
            glue.figureConPointId = newId;
          }
          glue = _.findWhere(group.glues, {connectorConPointId: conPoint.id});
          if(glue){
            glue.connectorConPointId = newId;
          }
        }
        conPoint.id = newId;
    });
},
DIAGRAMO.CustomFigure.prototype = {

    getElement: function(diagramo, customFigureProvider){
        var self = this;
        var div = $("<div>");
        div.addClass('custom-figure');
        var buttons = $("<div>");
        buttons.addClass('buttons');
        var edit = $("<i>");
        edit.click(function(){
            customFigureProvider.editFigure(self);
        });
        edit.addClass("fa fa-pencil-square-o");
        var del = $("<i>");
        del.addClass("fa fa-trash-o");
        del.css("cursor","pointer").css("marginLeft","5px");
        del.click(function(){
            customFigureProvider.deleteFigure(self);
        });
        buttons.append(edit);
        buttons.append(del);

        div.append(this.getImageElement(diagramo));
        div.append(buttons);

        var label = $("<div>");
        label.addClass('label');
        label.text(self.name);
        div.append(label);
        return div;
    },
    getImageElement: function(diagramo){
        var self = this;
        var img = document.createElement("img");
        img.src = this.imageSrc;
        img.width = "32";
        img.onmousedown = function(e){
            DIAGRAMO.dragging = new DIAGRAMO.draggableImage(self.imageSrc, e, function(event){
                var obj = diagramo.canvasGetXY(event.pageX, event.pageY);
                if(obj.x < 0 || obj.y < 0 || obj.x > diagramo.canvas.width || obj.y > diagramo.canvas.height){
                    DIAGRAMO.dragging.destroy();
                    DIAGRAMO.dragging = null;
                    return;
                }
                var shape = DIAGRAMO.load(self.jsonObject);
                self.jsonObject = _.extend({}, self.jsonObject)
                self.jsonObject.version = DIAGRAMO.VERSION;
                shape.glues = self.jsonObject.glues;
                DIAGRAMO.CustomFigure.setupShape(shape, diagramo);
                var bounds = shape.getBounds();
                var x = bounds[0] + (bounds[2] - bounds[0]) / 2;
                var y = bounds[1] + (bounds[3] - bounds[1]) / 2;


                var cmd = new DIAGRAMO.COMMANDS.BringToFrontCommand(diagramo, shape);
                cmd.execute();
                shape.transform(DIAGRAMO.Matrix.translationMatrix(obj.x - x, obj.y - y));
                diagramo.selectFigure(shape);
                diagramo.isMouseDown = false;
                diagramo.repaint();
                DIAGRAMO.dragging.destroy();
                DIAGRAMO.dragging = null;
            });
        }
        return img;
    }
}
