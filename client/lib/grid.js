DIAGRAMO.Grid = function(x, y, width, height){
  DIAGRAMO.Polygon.call(this, x, y);
  this.width = width;
  this.height = height;
  this.style.strokeStyle = "#ddd";
  this.style.lineWidth = '1px';
   this.style.lineStyle = 'dotted';
  for(var i = x; i < width; i += DIAGRAMO.PROPERTIES.GRID_SIZE){
    this.paintables.push(new DIAGRAMO.Line(
      new DIAGRAMO.Point(i, y),
      new DIAGRAMO.Point(i, y + height)
    ));
  }
  for(var i = y; i < height; i += DIAGRAMO.PROPERTIES.GRID_SIZE){
    this.paintables.push(new DIAGRAMO.Line(
      new DIAGRAMO.Point(x, i),
      new DIAGRAMO.Point(x + width, i)
    ));
  }
}


DIAGRAMO.Grid.init = function(){
  _.extend(DIAGRAMO.Grid.prototype, DIAGRAMO.Polygon.prototype, {
    paint: function(context){
      //skip over the polygon paint
      DIAGRAMO.Primitive.prototype.paint.call(this, context);
    }
  });
}
